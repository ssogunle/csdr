package com.inted.csdr.jsf.beans;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.inted.csdr.domain.TriggerPoint;
import com.inted.csdr.domain.embeddables.Spt;
import com.inted.csdr.domain.models.enumerated.SptType;
import com.inted.csdr.providers.domain.TriggerPointProvider;

@ManagedBean
@ViewScoped
public class TriggerPointBean {

	private TriggerPoint tp;
	private List<TriggerPoint> tpList;
	private TriggerPointProvider worker;
	
	private List<Spt> mainSptList;
	private List<Spt> sptList;
	private List<SptType> sptTypeList;
	
	private int grpCount;
	
	public TriggerPointBean() {
		grpCount =0;
		populateTypeList();
		mainSptList = new ArrayList<Spt>();
		sptList = new ArrayList<Spt>();
		worker = new TriggerPointProvider();
	}

	public void populateTypeList() {
		sptTypeList = new ArrayList<SptType>();
		sptTypeList.add(SptType.SIP_METHOD);
		sptTypeList.add(SptType.REQUEST_URI);
		sptTypeList.add(SptType.SIP_HEADER);
		sptTypeList.add(SptType.SESSION_DESCRIPTION);
		sptTypeList.add(SptType.SESSION_CASE);
	}

	public TriggerPoint getTp() {
		if (tp == null)
			tp = new TriggerPoint();
		return tp;
	}

	public List<SptType> getSptTypeList() {
		return sptTypeList;
	}

	public void setSptTypeList(List<SptType> sptTypeList) {
		this.sptTypeList = sptTypeList;
	}

	public List<Spt> getSptList() {
		return sptList;
	}

	public void setSptList(List<Spt> sptList) {
		this.sptList = sptList;
	}


	public List<Spt> getMainSptList() {
		return mainSptList;
	}

	public void setMainSptList(List<Spt> mainSptList) {
		this.mainSptList = mainSptList;
	}

	
	public int getGrpCount() {
		return grpCount;
	}

	/**
	 * Add SPT
	 */
	public void addSpt() {
		sptList.add(new Spt());
	}

	/**
	 * Remove SPT
	 */
	public void removeSpt(Spt spt) {
		sptList.remove(spt);
	}

	/**
	 * Save SPT Group
	 */
	public void saveSptGroup() {

		if (sptList.size() < 1)
			return;

		for (Spt spt : sptList) {
			if (spt.getType() == null || spt.getData() == null || spt.getData().trim().length() < 1) {
				return;
			}
		}

		appendSptsToMain();
		grpCount++;
		sptList.clear();
	}

	/**
	 * Reset SPT Group List
	 */
	public void clearSptGroupList() {
		mainSptList.clear();
		grpCount =0;
	}
	
	/**
	 * Append SptList to mainList
	 */
	public void appendSptsToMain(){
		
		for(Spt spt: sptList){
			spt.setGrpId(grpCount+1);
			mainSptList.add(spt);
		}
	}
	/**
	 * Save the Trigger Point to Database
	 */
	public void save(){
		
		if(mainSptList.size()<1)
			return;
		
		tp.setSptList(mainSptList);
		worker.add(tp);
		tp = null;
		clearSptGroupList();
	}
	
	/**
	 * Fetch all Trigger Points
	 */
	public List<TriggerPoint> getTpList(){
		
		if(tpList==null){
			tpList = worker.getPTriggerPoints();
		}
		
		int i=0;
		for(TriggerPoint tp: tpList){
			tp.setId(""+(i+1));
			i++;
		}
		return tpList;
	}
}
