package com.inted.csdr.jsf.beans;

import java.util.List;


import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import com.inted.csdr.domain.VisitedNetwork;
import com.inted.csdr.providers.domain.VisitedNetworkProvider;

@ManagedBean
@RequestScoped
public class VisitedNetworkBean {

	private VisitedNetwork vn;
	private List<VisitedNetwork> vnList;
	private VisitedNetworkProvider worker;

	public VisitedNetworkBean() {
		worker = new VisitedNetworkProvider();
	}

	public VisitedNetwork getVn() {
		if (vn == null)
			vn = new VisitedNetwork();
		return vn;
	}

	public void setVn(VisitedNetwork vn) {
		this.vn = vn;
	}

	/**
	 * Fetch all visited networks
	 */
	public List<VisitedNetwork> getVnList() {
		vnList = worker.getPVisitedNetworks();
		int i=0;
		for(VisitedNetwork vn: vnList){
			vn.setId(i+1);
			i++;
		}
		return vnList;
	}

	/**
	 * Store Visited Network Data in Database
	 */
	public void save() {
		worker.add(vn);
		vn = null;
	}
}
