package com.inted.csdr.jsf.beans;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import com.inted.csdr.domain.ClientSessionData;
import com.inted.csdr.providers.domain.ClientSessionDataProvider;

@ManagedBean
@RequestScoped
public class ClientSessionDataBean {

	private List<ClientSessionData> dataList;
	private ClientSessionDataProvider p;
	
	public ClientSessionDataBean(){
		p = new ClientSessionDataProvider();
	}
	
	/**
	 * Fetch all Session Data
	 */
	public List<ClientSessionData> getDataList() {
		dataList = p.getPClientSessionDataSet();
		return dataList;
	}
	
	
}
