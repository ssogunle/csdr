package com.inted.csdr.jsf.beans;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import com.inted.csdr.domain.RegisteredIdentity;
import com.inted.csdr.providers.domain.RegisteredIdentityProvider;

@ManagedBean
@RequestScoped
public class RegisteredIdentityBean {

	private List<RegisteredIdentity> riList;
	private RegisteredIdentityProvider worker; 
	
	public RegisteredIdentityBean(){
		worker = new RegisteredIdentityProvider();
	}
	

	/**
	 * Fetch all Registered Identity
	 */
	public List<RegisteredIdentity> getRiList() {
		riList = worker.getPRegisteredIdentities();
		
		int i=0;
		for(RegisteredIdentity ri: riList){
			ri.setId(i+1);
			i++;
		}
		return riList;
	}
}
