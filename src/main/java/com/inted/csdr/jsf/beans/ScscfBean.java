package com.inted.csdr.jsf.beans;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import com.inted.csdr.domain.Scscf;
import com.inted.csdr.providers.domain.ScscfProvider;

@ManagedBean
@RequestScoped
public class ScscfBean {

	private Scscf scscf;
	private List<Scscf> scscfList;
	private ScscfProvider worker;

	public ScscfBean() {
		worker = new ScscfProvider();
	}

	public Scscf getScscf() {
		if (scscf == null)
			scscf = new Scscf();
		return scscf;
	}

	public void setScscf(Scscf scscf) {
		this.scscf = scscf;
	}

	/**
	 * Fetch all Scscfs
	 */
	public List<Scscf> getScscfList() {
		scscfList = worker.getPScscfs();
		
		int i=0;
		for(Scscf s: scscfList){
			s.setId(""+(i+1));
			i++;
		}
		return scscfList;
	}

	/**
	 * Store S-CSCF Data in Database
	 */
	public void save() {
		worker.add(scscf);
		scscf=null;
	}
}
