package com.inted.csdr.jsf.beans;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.inted.csdr.domain.FeData;
import com.inted.csdr.domain.TriggerPoint;
import com.inted.csdr.providers.domain.FeDataProvider;

@ManagedBean
@ViewScoped
public class FeBean {

	private FeData feData;
	private List<FeData> feDataSet;
	private FeDataProvider provider;
	
	public FeBean(){
		provider = new FeDataProvider();
	}
	
	public FeData getFeData(){
		
		if(feData ==null)
		feData = new FeData();
		
		return feData;
	}
	
	public void save(){
		
		provider.add(feData);
		feData = null;
	}
	
	/**
	 * Fetch all FE Data Sets
	 */
	public List<FeData> getFeDataSet(){
		
		if(feDataSet==null){
			feDataSet = provider.getPFeDataSet();
		}
		
		int i=0;
		for(FeData feData: feDataSet){
			feData.setId(i+1);
			i++;
		}
		return feDataSet;
	}
}
