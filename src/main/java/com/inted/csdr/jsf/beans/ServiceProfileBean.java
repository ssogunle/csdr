package com.inted.csdr.jsf.beans;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.inted.csdr.domain.AppServer;
import com.inted.csdr.domain.Ifc;
import com.inted.csdr.domain.ServiceProfile;
import com.inted.csdr.domain.TriggerPoint;
import com.inted.csdr.domain.models.enumerated.ProfilePartIndicator;
import com.inted.csdr.providers.domain.AppServerProvider;
import com.inted.csdr.providers.domain.ServiceProfileProvider;
import com.inted.csdr.providers.domain.TriggerPointProvider;

@ManagedBean
@ViewScoped
public class ServiceProfileBean {

	private ServiceProfile sp;
	private List<ServiceProfile> spList;

	private Ifc ifc;
	private List<Ifc> ifcList;

	private String tpId;
	private String asId;

	private Map<String, TriggerPoint> triggerPoints;
	private Map<String, AppServer> appServers;
	
	private Map<String, String> asList;
	private Map<String, String> tpList;
	private List<ProfilePartIndicator> ppiList;

	private ServiceProfileProvider worker;

	public ServiceProfileBean() {
		worker = new ServiceProfileProvider();

		populateProfilePartIndicator();
		populateAppServers();
		populateTriggerPoints();
		ifcList = new ArrayList<Ifc>();
	}

	public ServiceProfile getSp() {
		if (sp == null)
			sp = new ServiceProfile();
		return sp;
	}

	public Ifc getIfc() {
		if (ifc == null)
			ifc = new Ifc();
		return ifc;
	}

	public void populateProfilePartIndicator() {
		ppiList = new ArrayList<ProfilePartIndicator>();
		ppiList.add(ProfilePartIndicator.ANY);
		ppiList.add(ProfilePartIndicator.REGISTERED);
		ppiList.add(ProfilePartIndicator.UNREGISTERED);
	}

	public void populateAppServers() {
		List<AppServer> list= new AppServerProvider().getPAppServers();
		appServers = new LinkedHashMap<String, AppServer>();
		asList = new LinkedHashMap<String, String>();

		for (AppServer as : list) {
			asList.put(as.getName(), as.getId());
			appServers.put(as.getId(), as);
		}
	}

	public void populateTriggerPoints() {
		List<TriggerPoint> list = new TriggerPointProvider().getPTriggerPoints();
		triggerPoints = new LinkedHashMap<String, TriggerPoint>();
		tpList = new LinkedHashMap<String, String>();

		for (TriggerPoint tp : list) {
			tpList.put(tp.getName(), tp.getId());
			triggerPoints.put(tp.getId(), tp);
		}
	}

	public String getTpId() {
		return tpId;
	}

	public String getAsId() {
		return asId;
	}

	public void setTpId(String tpId) {
		this.tpId = tpId;
	}

	public void setAsId(String asId) {
		this.asId = asId;
	}

	public Map<String, String> getAsList() {
		return asList;
	}

	public Map<String, String> getTpList() {
		return tpList;
	}

	public List<Ifc> getIfcList() {
		return ifcList;
	}

	public List<ProfilePartIndicator> getPpiList() {
		return ppiList;
	}

	/**
	 * Add Ifc
	 */
	public void addIfc() {
		AppServer as = appServers.get(asId);
		TriggerPoint tp = triggerPoints.get(tpId);
		
		if(as == null || tp ==null)
			return;
		
		ifc.setAppServer(as);
		ifc.setTriggerPoint(tp);
		ifc.setPriority(ifcList.size());
		ifcList.add(ifc);
		ifc = null;
		tpId=asId=null;
	}

	/**
	 * Remove Ifc
	 */
	public void removeIfc(Ifc ifc) {
		ifcList.remove(ifc);

		// Reorder priorities
		for (int i = 0; i < ifcList.size(); i++) {
			ifcList.get(i).setPriority(i);
		}
	}

	/**
	 * Fetch all Service Profiles
	 */
	public List<ServiceProfile> getSpList() {
		spList = worker.getPServiceProfiles();
		
		int i=0;
		for(ServiceProfile sp: spList){
			sp.setId(""+(i+1));
			i++;
		}
		return spList;
	}

	/**
	 * Store Service Profile in Database
	 */
	public void save() {

		if (ifcList.size() > 0)
			sp.setIfcList(ifcList);

		worker.add(sp);
		sp = null;
		ifcList.clear();
	}

	/**
	 * Get Key from value
	 */
	public static Object getKeyFromValue(Map hm, Object value) {
		System.out.println("Obj Value: "+value.toString());
		for (Object o : hm.keySet()) {
			if (hm.get(o).equals(value)) {
				return o;
			}
		}
		return null;
	}
}
