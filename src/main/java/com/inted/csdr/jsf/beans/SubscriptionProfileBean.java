package com.inted.csdr.jsf.beans;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.inted.csdr.domain.Impi;
import com.inted.csdr.domain.Impu;
import com.inted.csdr.domain.RegisteredIdentity;
import com.inted.csdr.domain.Schemes;
import com.inted.csdr.domain.Scscf;
import com.inted.csdr.domain.ServiceProfile;
import com.inted.csdr.domain.SubscriptionProfile;
import com.inted.csdr.domain.VisitedNetwork;
import com.inted.csdr.domain.embeddables.AuthenticationData;
import com.inted.csdr.domain.embeddables.AuthenticationScheme;
import com.inted.csdr.domain.models.enumerated.DsaiValue;
import com.inted.csdr.domain.models.enumerated.IdentityType;
import com.inted.csdr.domain.models.enumerated.ImsUserState;
import com.inted.csdr.domain.models.enumerated.PsiActivation;
import com.inted.csdr.domain.models.enumerated.SchemeEnum;
import com.inted.csdr.providers.domain.RegisteredIdentityProvider;
import com.inted.csdr.providers.domain.ScscfProvider;
import com.inted.csdr.providers.domain.ServiceProfileProvider;
import com.inted.csdr.providers.domain.SubscriptionProfileProvider;
import com.inted.csdr.providers.domain.VisitedNetworkProvider;

@ManagedBean
@ViewScoped
public class SubscriptionProfileBean {

	private SubscriptionProfile subscriptionProfile;
	private List<SubscriptionProfile> subscriptionProfileList;

	private String serviceProfileId;
	private String scscfUri;
	private String password;

	private Impi impi;
	private Impu impu;
	private List<Impu> impuList;

	private List<DsaiValue> dsaiValueList;
	private List<PsiActivation> psiActivationList;
	private List<IdentityType> identityTypeList;

	private Map<String, ServiceProfile> serviceProfiles;
	private Map<String, String> serviceProfileList;

	private Map<String, String> scscfList;

	private List<String> visitedNetworkList;

	private SubscriptionProfileProvider worker;

	public SubscriptionProfileBean() {
		populateDsai();
		populatePsiActivation();
		populateServiceProfiles();
		populateScscfs();
		populateIdentityTypes();
		populateVisitedNetworks();
		worker = new SubscriptionProfileProvider();
		impuList = new ArrayList<Impu>();
	}

	public void populateDsai() {
		dsaiValueList = new ArrayList<DsaiValue>();
		dsaiValueList.add(DsaiValue.INACTIVE);
		dsaiValueList.add(DsaiValue.ACTIVE);
	}

	public void populatePsiActivation() {
		psiActivationList = new ArrayList<PsiActivation>();
		psiActivationList.add(PsiActivation.INACTIVE);
		psiActivationList.add(PsiActivation.ACTIVE);
	}

	public void populateIdentityTypes() {
		identityTypeList = new ArrayList<IdentityType>();
		identityTypeList.add(IdentityType.PUBLIC_USER_IDENTITY);
		identityTypeList.add(IdentityType.DISTINCT_PSI);
		identityTypeList.add(IdentityType.WILDCARDED_PSI);
	}

	public void populateServiceProfiles() {
		List<ServiceProfile> list = new ServiceProfileProvider().getPServiceProfiles();
		serviceProfiles = new LinkedHashMap<String, ServiceProfile>();
		serviceProfileList = new LinkedHashMap<String, String>();

		for (ServiceProfile sp : list) {
			serviceProfileList.put(sp.getName(), sp.getId());
			serviceProfiles.put(sp.getId(), sp);
		}
	}

	public void populateScscfs() {
		List<Scscf> list = new ScscfProvider().getPScscfs();

		scscfList = new LinkedHashMap<String, String>();

		for (Scscf s : list) {
			scscfList.put(s.getName(), s.getSipUri());
		}
	}

	public void populateVisitedNetworks() {
		List<VisitedNetwork> list = new VisitedNetworkProvider().getPVisitedNetworks();
		visitedNetworkList = new ArrayList<String>();

		for (VisitedNetwork vn : list) {
			visitedNetworkList.add(vn.getName());
		}
	}

	public SubscriptionProfile getSubscriptionProfile() {
		if (subscriptionProfile == null)
			subscriptionProfile = new SubscriptionProfile();
		return subscriptionProfile;
	}

	public void setSubscriptionProfile(SubscriptionProfile subscriptionProfile) {
		this.subscriptionProfile = subscriptionProfile;
	}

	public List<PsiActivation> getPsiActivationList() {
		return psiActivationList;
	}

	public List<DsaiValue> getDsaiValueList() {
		return dsaiValueList;
	}

	public List<IdentityType> getIdentityTypeList() {
		return identityTypeList;
	}

	public List<String> getVisitedNetworkList() {
		return visitedNetworkList;
	}

	public Impi getImpi() {
		if (impi == null)
			impi = new Impi();
		return impi;
	}

	public void setImpi(Impi impi) {
		this.impi = impi;
	}

	public String getServiceProfileId() {
		return serviceProfileId;
	}

	public void setServiceProfileId(String serviceProfileId) {
		this.serviceProfileId = serviceProfileId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getScscfUri() {
		return scscfUri;
	}

	public void setScscfUri(String scscfUri) {
		this.scscfUri = scscfUri;
	}

	public List<Impu> getImpuList() {
		return impuList;
	}

	public void setImpuList(List<Impu> impuList) {
		this.impuList = impuList;
	}

	public Map<String, String> getServiceProfileList() {
		return serviceProfileList;
	}

	public Map<String, String> getScscfList() {
		return scscfList;
	}

	public Impu getImpu() {
		if (impu == null)
			impu = new Impu();
		return impu;
	}

	public void setImpu(Impu impu) {
		this.impu = impu;
	}

	/**
	 * Attach IMPU
	 */
	public void addImpu() {

		if (impu.getSipUri() == null || impu.getSipUri().trim().length() < 1)
			return;

		if (impu.getIdentityType() == IdentityType.PUBLIC_USER_IDENTITY
				&& impu.getPsiActivation() == PsiActivation.ACTIVE)
			return;

		impuList.add(impu);
		impu = null;
	}

	/**
	 * Detach IMPU
	 */
	public void removeImpu(Impu impu) {
		impuList.remove(impu);
	}

	/**
	 * Fetch all subscription profiles
	 */
	public List<SubscriptionProfile> getSubscriptionProfileList() {
		subscriptionProfileList = worker.getPSubscriptionProfiles();

		int i = 0;
		for (SubscriptionProfile s : subscriptionProfileList) {
			s.setId("" + (i + 1));
			i++;
		}
		return subscriptionProfileList;
	}

	/**
	 * Store Subscription Profile in Database
	 */
	public void save() {

		// Get Service Profile
		ServiceProfile sp = serviceProfiles.get(serviceProfileId);

		System.out.println("IMPU LIST SIZE: " + impuList.size());
		System.out.println("IMPI IS NULL: " + (impi == null));
		System.out.println("Service Profile is NULL: " + (sp == null));
		System.out.println("S-CSCF URI : " + scscfUri);
		System.out.println("S-CSCF LIST SIZE: " + scscfList.size());

		if (impuList.size() < 1 || impi == null || sp == null || scscfUri == null)
			return;

		System.out.println("Here before SERVICE PROFILE!!!!!!!!!!!");

		// Add Service Profile
		subscriptionProfile.setServiceProfile(sp);

		// Add Scscf
		subscriptionProfile.setScscfName(scscfUri);

		// Add IMPI
		impi = completeImpi(impi);
		subscriptionProfile.setImpi(impi);

		// Add IMPU list
		List<Impu> newImpuList = new ArrayList<Impu>();
		for (Impu oldImpu : impuList) {
			newImpuList.add(completeImpu(oldImpu));
		}
		impuList = newImpuList;
		subscriptionProfile.setImpuList(impuList);

		// Store Subscription Profile
		worker.add(subscriptionProfile);
		System.out.println("Subscription Profile Saved! && was null? "+(subscriptionProfile==null));
		
		// Store Condensed AAA Data
		saveCondensedAAAData();
		System.out.println("AAA Data Saved!");

		impi = null;
		impuList.clear();
		subscriptionProfile = null;
		password = scscfUri = serviceProfileId = null;
	}

	/**
	 * Add necessary Info for IMPU
	 */
	public Impu completeImpu(Impu impu) {

		// Set user State
		impu.setImsUserState(ImsUserState.AUTHENTICATION_PENDING);

		// Set Visited Network

		return impu;
	}

	/**
	 * Add necessary Info for IMPI
	 */
	public Impi completeImpi(Impi impi) {

		// Authentication Data
		AuthenticationData authData = new AuthenticationData();

		String amf = "0000";
		authData.setAmf(amf.getBytes());

		String op = "00000000000000000000000000000000";
		authData.setOp(op.getBytes());

		authData.setSecretKey(password.getBytes());

		authData.setSqn("000000000000");

		// authData.setLineIdentifier(lineIdentifier);
		// authData.setIpAddress(ipAddress);

		impi.setAuthData(authData);

		// Authentication Scheme
		AuthenticationScheme authScheme = new AuthenticationScheme();

		authScheme.setDefaultScheme(Schemes.Auth_Scheme_MD5);

		List<Integer> supportedSchemes = new ArrayList<Integer>();
		supportedSchemes.add(Schemes.All);
		authScheme.setSupportedSchemes(supportedSchemes);

		impi.setAuthScheme(authScheme);

		return impi;
	}

	/**
	 * Store Registered Identity Data
	 */
	public void saveCondensedAAAData() {

		RegisteredIdentity ri = new RegisteredIdentity();
		ri.setAuthAmf(impi.getAuthData().getAmf());
		ri.setAuthIpAddress(impi.getAuthData().getIpAddress());
		ri.setAuthKey(impi.getAuthData().getSecretKey());
		ri.setAuthLineId(impi.getAuthData().getLineIdentifier());
		ri.setAuthOp(impi.getAuthData().getOp());
		ri.setAuthScheme(impi.getAuthScheme().getDefaultScheme());
		ri.setAuthSqn(impi.getAuthData().getSqn());
		ri.setImpi(impi.getUri());
		ri.setScscfName(subscriptionProfile.getScscfName());

		for (Impu tempImpu : impuList) {
			ri.setBarring(tempImpu.getBarringIndication());
			ri.setCanRegister(tempImpu.getCanRegister());
			ri.setImpu(tempImpu.getSipUri());
			ri.setPsiActivation(tempImpu.getPsiActivation());
			ri.setImpuType(tempImpu.getIdentityType());
			ri.setUserState(tempImpu.getImsUserState());
			ri.setVisitedNetwork(tempImpu.getVisitedNetworkId());
			new RegisteredIdentityProvider().add(ri);
		}

	}
}
