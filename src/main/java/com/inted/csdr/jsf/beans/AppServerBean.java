package com.inted.csdr.jsf.beans;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.inted.csdr.domain.AppServer;
import com.inted.csdr.domain.embeddables.ShPurPermission;
import com.inted.csdr.domain.embeddables.ShUdrPermission;
import com.inted.csdr.providers.domain.AppServerProvider;

@ManagedBean
@ViewScoped
public class AppServerBean {

	private AppServer as;
	private ShUdrPermission udr;
	private ShPurPermission pur;

	private List<AppServer> asList;
	private AppServerProvider worker;

	public AppServerBean() {
		worker = new AppServerProvider();
	}

	public AppServer getAs() {
		if (as == null)
			as = new AppServer();
		return as;
	}

	public ShUdrPermission getUdr() {
		if (udr == null)
			udr = new ShUdrPermission();
		return udr;
	}

	public ShPurPermission getPur() {
		if (pur == null)
			pur = new ShPurPermission();
		return pur;
	}

	/**
	 * Store Application Server to Database
	 */
	public void save() {
		
		if(pur.getIsAllowed())
		as.setShPurPermission(pur);
		
		if(udr.getIsAllowed())
		as.setShUdrPermission(udr);
		
		worker.add(as);
		as = null;
		pur =null;
		udr =null;
	}

	/**
	 * Fetch all Application Servers
	 */
	public List<AppServer> getAsList() {
		asList = worker.getPAppServers();
		int i=0;
		for(AppServer as: asList){
			as.setId(""+(i+1));
			i++;
		}
		return asList;
	}
}
