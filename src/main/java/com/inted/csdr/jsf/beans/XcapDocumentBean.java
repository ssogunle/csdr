package com.inted.csdr.jsf.beans;

import java.util.List;

import com.inted.csdr.domain.XcapDocument;
import com.inted.csdr.providers.domain.XcapDocumentProvider;



public class XcapDocumentBean {

	private List<XcapDocument> docList;
	private XcapDocumentProvider p;
	
	public XcapDocumentBean(){
		p = new XcapDocumentProvider();
	}
	
	/**
	 * Fetch all Xcap Document
	 */
	public List<XcapDocument> getDocList() {
		docList = p.getPXcapDocuments();
		return docList;
	}
	
}
