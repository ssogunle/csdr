package com.inted.csdr.util;

import java.io.InputStream;
import java.io.StringReader;
import java.io.FileInputStream;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import javax.xml.transform.Transformer;

import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.apache.commons.lang.StringEscapeUtils;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class XMLUtil {

	
	public static String xmlToHtml(String data){
		
		return StringEscapeUtils.escapeXml(data);
	}
	
	/*
	
	public static void stringToDom(String xmlSource) 
	        throws SAXException, ParserConfigurationException, IOException {
	    // Parse the given input
	    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	    DocumentBuilder builder = factory.newDocumentBuilder();
	    Document doc = builder.parse(new InputSource(new StringReader(xmlSource)));

	    // Write the parsed document to an xml file
	    TransformerFactory transformerFactory = TransformerFactory.newInstance();
	    Transformer transformer = transformerFactory.newTransformer();
	    DOMSource source = new DOMSource(doc);

	    StreamResult result =  new StreamResult(new File("my-file.xml"));
	    transformer.transform(source, result);
	}
	*/
	private static String convertDocumentToString(Document doc) {
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer transformer;
		try {
			transformer = tf.newTransformer();
			// below code to remove XML declaration
			// transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION,
			// "yes");
			StringWriter writer = new StringWriter();
			transformer.transform(new DOMSource(doc), new StreamResult(writer));
			String output = writer.getBuffer().toString();
			return output;
		} catch (TransformerException e) {
			e.printStackTrace();
		}

		return null;
	}

	public static Document convertStringToDocument(String xmlStr) {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder;
		try {
			builder = factory.newDocumentBuilder();
			Document doc = builder.parse(new InputSource(new StringReader(xmlStr)));
			return doc;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static byte[] documentToByte(Document document) throws TransformerException {
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(document);
		
		ByteArrayOutputStream bos=new ByteArrayOutputStream();
		 StreamResult result=new StreamResult(bos);
		 transformer.transform(source, result);
		 byte []array=bos.toByteArray();
		 
		 return array;
	}
	
	public static String byteToString(byte [] data){
	
		try{
			
		Document d =  byteToDocument(data);
		String sd = convertDocumentToString(d);
		
		return sd;
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return null;
	}
	
	private static Document byteToDocument(byte[] data) throws Exception {
	    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	    factory.setNamespaceAware(true);
	    DocumentBuilder builder = factory.newDocumentBuilder();
	    return builder.parse(new ByteArrayInputStream(data));
	}

	public String convertXMLFileToString(String fileName) {
		try {
			DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
			InputStream inputStream = new FileInputStream(new File(fileName));
			org.w3c.dom.Document doc = documentBuilderFactory.newDocumentBuilder().parse(inputStream);
			StringWriter stw = new StringWriter();
			Transformer serializer = TransformerFactory.newInstance().newTransformer();
			serializer.transform(new DOMSource(doc), new StreamResult(stw));
			return stw.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public boolean doValidate(String xsd) {

		try {
			// define the type of schema - we use W3C:
			String schemaLang = "http://www.w3.org/2001/XMLSchema";

			// get validation driver:
			SchemaFactory factory = SchemaFactory.newInstance(schemaLang);

			// create schema by reading it from an XSD file:
			Schema schema = factory.newSchema(new StreamSource("sample.xsd"));
			/*
			 * use string instead of actual file: new StreamSource(new
			 * StringReader(str)) OR String S = ...; InputStream source = new
			 * ByteArrayInputStream(S.getBytes(encoding));
			 */
			Validator validator = schema.newValidator();

			// at last perform validation:
			validator.validate(new StreamSource("sample.xml"));

		} catch (SAXException ex) {
			// we are here if the document is not valid:
			// ... process validation error...
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return false;
	}
}
