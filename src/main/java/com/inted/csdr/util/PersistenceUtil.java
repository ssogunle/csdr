package com.inted.csdr.util;

import java.math.BigInteger;

public class PersistenceUtil {

	private static final String EMF_MONGO = "csdr-mongo"; // EntityManagerFactory
	private static final String EMF_MYSQL = "csdr-mysql"; // EntityManagerFactory
	private static final String EMF_REDIS = "csdr-redis"; // EntityManagerFactory

	public static String getEmfMongo() {

		return EMF_MONGO;
	}

	public static String getEmfMysql() {

		return EMF_MYSQL;
	}
	
	public static String getEmfRedis() {
		return EMF_REDIS;
	}

	public static BigInteger getId(String key) {

		BigInteger bigInt = new BigInteger(key.getBytes());
		return bigInt;
	}
}
