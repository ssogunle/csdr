package com.inted.csdr.util;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Locale;



import org.apache.olingo.commons.api.data.Entity;
import org.apache.olingo.commons.api.data.EntityCollection;

import org.apache.olingo.commons.api.edm.EdmBindingTarget;
import org.apache.olingo.commons.api.edm.EdmEntitySet;
import org.apache.olingo.commons.api.edm.EdmEntityType;
import org.apache.olingo.commons.api.edm.EdmNavigationProperty;
import org.apache.olingo.commons.api.edm.EdmPrimitiveType;
import org.apache.olingo.commons.api.edm.EdmPrimitiveTypeException;
import org.apache.olingo.commons.api.edm.EdmProperty;
import org.apache.olingo.commons.api.edm.EdmType;
import org.apache.olingo.commons.api.ex.ODataRuntimeException;
import org.apache.olingo.commons.api.http.HttpStatusCode;
import org.apache.olingo.server.api.ODataApplicationException;
import org.apache.olingo.server.api.uri.UriInfoResource;
import org.apache.olingo.server.api.uri.UriParameter;
import org.apache.olingo.server.api.uri.UriResource;
import org.apache.olingo.server.api.uri.UriResourceEntitySet;
import org.apache.olingo.server.api.uri.UriResourceNavigation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ODataUtil {

	private static final Logger LOG = LoggerFactory.getLogger(ODataUtil.class);
	
	
	// EDM entity set for a request
	public static EdmEntitySet getEdmEntitySet(UriInfoResource uriInfo) throws ODataApplicationException {
		List<UriResource> resourcePaths = uriInfo.getUriResourceParts();
		UriResourceEntitySet uriResource = (UriResourceEntitySet) resourcePaths.get(0);
		return uriResource.getEntitySet();
	}
	

	public static URI createId(String entitySetName, Object id) {
		try {
			return new URI(entitySetName + "(" + String.valueOf(id) + ")");
		} catch (URISyntaxException e) {
			throw new ODataRuntimeException("Unable to create id for entitySet: " + entitySetName, e);
		}

	}
	
	public static URI createIdSet(String entitySetName, Object[] keyName, Object[] keyValue) {
		
		try {
			String val="";
		
			for(int i=0; i<keyName.length; i++){
				
				if(i==0)
					val  = String.valueOf(keyName[i]+"="+keyValue[i]);
				else
				val = val+","+ String.valueOf(keyName[i]+"="+keyValue[i]);;
			}
			
			//Hopefully it catches NULL value!
			return new URI(entitySetName + "(" + val + ")");
			
		} catch (URISyntaxException e) {
			throw new ODataRuntimeException("Unable to create id for entitySet: " + entitySetName, e);
		}

	}

	public static Entity findEntity(EdmEntityType edmEntityType, EntityCollection entitySet,
			List<UriParameter> keyParams) throws ODataApplicationException {

		List<Entity> entityList = entitySet.getEntities();

		// loop over all entities in order to find that one that matches all
		// keys in request
		// an example could be e.g. contacts(ContactID=1, CompanyID=1)
		for (Entity rt_entity : entityList) {
			boolean foundEntity = entityMatchesAllKeys(edmEntityType, rt_entity, keyParams);
			if (foundEntity) {
				return rt_entity;
			}
		}

		return null;
	}

	public static boolean entityMatchesAllKeys(EdmEntityType edmEntityType, Entity rt_entity,
			List<UriParameter> keyParams) throws ODataApplicationException {
		LOG.info("now finding key match");
		// loop over all keys
		for (final UriParameter key : keyParams) {
			// key
			String keyName = key.getName();
			String keyText = key.getText();

			// Edm: we need this info for the comparison below
			EdmProperty edmKeyProperty = (EdmProperty) edmEntityType.getProperty(keyName);
			Boolean isNullable = edmKeyProperty.isNullable();
			Integer maxLength = edmKeyProperty.getMaxLength();
			Integer precision = edmKeyProperty.getPrecision();
			Boolean isUnicode = edmKeyProperty.isUnicode();
			Integer scale = edmKeyProperty.getScale();
			// get the EdmType in order to compare
			EdmType edmType = edmKeyProperty.getType();
			// Key properties must be instance of primitive type
			EdmPrimitiveType edmPrimitiveType = (EdmPrimitiveType) edmType;
			LOG.info( "KeyName : "+rt_entity.getProperty(keyName).getValue());
			// Runtime data: the value of the current entity
			Object valueObject = rt_entity.getProperty(keyName).getValue(); // null-check
																			// is
																			// done
																			// in
																			// FWK

			// now need to compare the valueObject with the keyText String
			// this is done using the type.valueToString //
			String valueAsString = null;
			try {
				valueAsString = edmPrimitiveType.valueToString(valueObject, isNullable, maxLength, precision, scale,
						isUnicode);
			} catch (EdmPrimitiveTypeException e) {
				throw new ODataApplicationException("Failed to retrieve String value",
						HttpStatusCode.INTERNAL_SERVER_ERROR.getStatusCode(), Locale.ENGLISH, e);
			}

			if (valueAsString == null) {
				return false;
			}

			boolean matches = valueAsString.equals(keyText);
			if (!matches) {
				// if any of the key properties is not found in the entity, we
				// don't need to search further
				return false;
			}
		}

		return true;
	}
	
	public static EdmEntitySet getNavigationTargetEntitySet(final UriInfoResource uriInfo) throws ODataApplicationException {

	    EdmEntitySet entitySet;
	    final List<UriResource> resourcePaths = uriInfo.getUriResourceParts();

	    // First must be entity set (hence function imports are not supported here).
	    if (resourcePaths.get(0) instanceof UriResourceEntitySet) {
	        entitySet = ((UriResourceEntitySet) resourcePaths.get(0)).getEntitySet();
	    } else {
	        throw new ODataApplicationException("Invalid resource type.",
	                HttpStatusCode.NOT_IMPLEMENTED.getStatusCode(), Locale.ROOT);
	    }

	    int navigationCount = 0;
	    while (entitySet != null
	        && ++navigationCount < resourcePaths.size()
	        && resourcePaths.get(navigationCount) instanceof UriResourceNavigation) {
	        final UriResourceNavigation uriResourceNavigation = (UriResourceNavigation) resourcePaths.get(navigationCount);
	        final EdmBindingTarget target = entitySet.getRelatedBindingTarget(uriResourceNavigation.getProperty().getName());
	        if (target instanceof EdmEntitySet) {
	            entitySet = (EdmEntitySet) target;
	        } else {
	            throw new ODataApplicationException("Singletons not supported", HttpStatusCode.NOT_IMPLEMENTED.getStatusCode(),
	                                                 Locale.ROOT);
	        }
	    }

	    return entitySet;
	}
	
	/**
	   * Example:
	   * For the following navigation: DemoService.svc/Categories(1)/Products
	   * we need the EdmEntitySet for the navigation property "Products"
	   *
	   * This is defined as follows in the metadata:
	   * <code>
	   * 
	   * <EntitySet Name="Categories" EntityType="OData.Demo.Category">
	   * <NavigationPropertyBinding Path="Products" Target="Products"/>
	   * </EntitySet>
	   * </code>
	   * The "Target" attribute specifies the target EntitySet
	   * Therefore we need the startEntitySet "Categories" in order to retrieve the target EntitySet "Products"
	   */
	  public static EdmEntitySet getNavigationTargetEntitySet(EdmEntitySet startEdmEntitySet,
	      EdmNavigationProperty edmNavigationProperty)
	      throws ODataApplicationException {

	    EdmEntitySet navigationTargetEntitySet = null;

	    String navPropName = edmNavigationProperty.getName();
	    EdmBindingTarget edmBindingTarget = startEdmEntitySet.getRelatedBindingTarget(navPropName);
	    if (edmBindingTarget == null) {
	      throw new ODataApplicationException("Not supported.",
	              HttpStatusCode.NOT_IMPLEMENTED.getStatusCode(), Locale.ROOT);
	    }

	    if (edmBindingTarget instanceof EdmEntitySet) {
	      navigationTargetEntitySet = (EdmEntitySet) edmBindingTarget;
	    } else {
	      throw new ODataApplicationException("Not supported.",
	              HttpStatusCode.NOT_IMPLEMENTED.getStatusCode(), Locale.ROOT);
	    }

	    return navigationTargetEntitySet;
	  }
	
	public static UriResourceNavigation getLastNavigation(final UriInfoResource uriInfo) {

	    final List<UriResource> resourcePaths = uriInfo.getUriResourceParts();
	    int navigationCount = 1;
	    while (navigationCount < resourcePaths.size()
	        && resourcePaths.get(navigationCount) instanceof UriResourceNavigation) {
	        navigationCount++;
	    }

	    return (UriResourceNavigation) resourcePaths.get(--navigationCount);
	}
	
}
