package com.inted.csdr.auth;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet Filter implementation class CsdrFilter
 */
@WebFilter("/AuthFilter")
public class AuthFilter implements Filter {

	private ServletContext context;
    /**
     * Default constructor. 
     */
    public AuthFilter() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
	
	
		
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse resp =  (HttpServletResponse) response;
		
		String uri = req.getRequestURI();
		this.context.log("Requested Resource");
		
		
		String clientAddress = req.getRemoteAddr();
		
		HttpSession session = req.getSession(false);
		
		if(uri.endsWith(".xhtml") && session != null ){
			chain.doFilter(request, response);
		}
		else if(clientAddress.isEmpty()){
		resp.sendError(resp.SC_UNAUTHORIZED, "Front End not authorised for this transaction");
		}
		else{
			resp.sendError(resp.SC_SERVICE_UNAVAILABLE, "The CSDR can not this transaction at this time.");
		}
		
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		this.context = fConfig.getServletContext();
		this.context.log("CSDR Auth Filter Initialised");
	}

}
