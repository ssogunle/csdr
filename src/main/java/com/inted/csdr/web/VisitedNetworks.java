package com.inted.csdr.web;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.inted.csdr.domain.VisitedNetwork;
import com.inted.csdr.providers.domain.VisitedNetworkProvider;

/**
 * Servlet implementation class VisitedNetworks
 */
@WebServlet("/VisitedNetworks")
public class VisitedNetworks extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	private VisitedNetworkProvider service;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public VisitedNetworks() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		service = new VisitedNetworkProvider();
		
		List<VisitedNetwork> vnList = (List<VisitedNetwork>) service.getPVisitedNetworks();
		
		request.setAttribute("visited_networks", vnList);

		RequestDispatcher view  = request.getRequestDispatcher("WEB-INF/views/visited_network_list.jsp");
		
		view.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
