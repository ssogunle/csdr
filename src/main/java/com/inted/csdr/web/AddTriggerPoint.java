package com.inted.csdr.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import com.inted.csdr.domain.TriggerPoint;
import com.inted.csdr.domain.embeddables.Spt;
import com.inted.csdr.providers.domain.TriggerPointProvider;

/**
 * Servlet implementation class AddTriggerPoint
 */
@WebServlet("/AddTriggerPoint")
public class AddTriggerPoint extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private TriggerPointProvider worker;  
	final Logger LOG = LoggerFactory.getLogger(AddTriggerPoint.class);
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddTriggerPoint() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		RequestDispatcher view = request
				.getRequestDispatcher("WEB-INF/views/add_trigger_point.jsp");

		view.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		worker = new TriggerPointProvider();
		
		/*
		 * Trigger Point
		 */
		TriggerPoint tp = new TriggerPoint();
		
		String name = request.getParameter("name");
		tp.setName(name);

		String ctcnfVal = request.getParameter("ctcnf")==null? "true": request.getParameter("ctcnf");
		boolean conditionTypeCnf = Boolean.parseBoolean(ctcnfVal);
		tp.setConditionTypeCnf(conditionTypeCnf);
						
		List<Spt> sptList= new ArrayList<Spt>();
		/*
		 * Service Point Triggers
		 */
		
		/*
		 * Needs to be modified to support multiple SPT groups
		 */
			
		
		//List<SptGroup> groups = new ArrayList<SptGroup>();
		//groups.add(new SptGroup(1));
		/*
		SptGroup grp1 = new SptGroup(1);
		String[] negatedSpts = request.getParameterValues("negated");
		
				//SIP METHOD
				String sipMethod = request.getParameter("sip_method");
				
				if (sipMethod!=null){
					
					SipMethod sm = new SipMethod();
					
					sm.setConditionNegated(isNegated(negatedSpts, "method"));
					sm.setMethod(sipMethod);
		
					sm.setGroup(grp1);
			
					List<SipMethod> smList = new ArrayList<SipMethod>();
					smList.add(sm);
					tp.setSipMethodList(smList);
				}
				
				//REQUEST URI
				String reqUri = request.getParameter("request_uri");
				
				if(reqUri!=null){
					
					RequestUri ru = new RequestUri();
					
					ru.setConditionNegated(isNegated(negatedSpts, "request"));
					ru.setUri(reqUri);
					ru.setGroup(grp1);
					
					List<RequestUri> ruList = new ArrayList<RequestUri>(); 
					ruList.add(ru);
					tp.setRequestUriList(ruList);
				}
				
				//SIP HEADER
				String sipHeader = request.getParameter("header_value");
				
				if(sipHeader!=null){
					
					SipHeader sh = new SipHeader();
					
					sh.setConditionNegated(isNegated(negatedSpts, "header"));
					sh.setHeader(sipHeader);
					String sipHeaderContent = request.getParameter("header_content")==null?"":
						request.getParameter("header_content") ;
					sh.setContent(sipHeaderContent);
					sh.setGroup(grp1);
					
					List<SipHeader> shList = new ArrayList<SipHeader>();
					shList.add(sh);
					tp.setSipHeaderList(shList);
				}
						
				//SESSION DESCRIPTION
				String sdLine = request.getParameter("sd_line");
				
				if(sdLine!=null){
					
					SessionDescription sd = new SessionDescription();
					
					sd.setConditionNegated(isNegated(negatedSpts, "desc"));
					sd.setGroup(grp1);
					sd.setLine(sdLine);
					String sdContent = request.getParameter("sd_content");
					sd.setContent(sdContent);
					
					List<SessionDescription> sdList = new ArrayList<SessionDescription>();
					sdList.add(sd);
					tp.setSessionDescriptionList(sdList);
				}
						
				//SESSION CASE
				String sessionCase = request.getParameter("session_case");
				
				if(sessionCase!=null){
					
					SessionCase sc = new SessionCase(); 
					
					sc.setConditionNegated(isNegated(negatedSpts, "case"));
					sc.setGroup(grp1);
					int val = Integer.parseInt(sessionCase);
					sc.setValue(SessionCaseEnum.getEValue(val));
				
					List<SessionCase> scList = new ArrayList<SessionCase>();
					scList.add(sc);
					tp.setSessionCaseList(scList);
				}
				*/
		
		worker.add(tp);
		response.sendRedirect("TriggerPoints");
	}

	private boolean isNegated(String [] negatedSet, String val){
		
		if(negatedSet!=null){
			
			for(int i=0; i<negatedSet.length; i++){
				
				if(negatedSet[i].equals(val))
				return true;
			}
		}
		
		return false;
	}
}
