package com.inted.csdr.web;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.inted.csdr.domain.AppServer;
import com.inted.csdr.domain.VisitedNetwork;
import com.inted.csdr.providers.domain.AppServerProvider;

/**
 * Servlet implementation class AppServers
 */
@WebServlet("/AppServers")
public class AppServers extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private AppServerProvider worker;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AppServers() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		worker = new AppServerProvider();
		
		List<AppServer> asList = (List<AppServer>) worker.getPAppServers();

		request.setAttribute("app_servers", asList);

		RequestDispatcher view = request
				.getRequestDispatcher("WEB-INF/views/app_server_list.jsp");

		view.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
