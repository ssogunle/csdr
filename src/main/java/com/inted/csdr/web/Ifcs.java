package com.inted.csdr.web;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import com.inted.csdr.domain.Ifc;
import com.inted.csdr.providers.domain.AppServerProvider;
import com.inted.csdr.providers.domain.IfcProvider;


/**
 * Servlet implementation class Ifcs
 */
@WebServlet("/Ifcs")
public class Ifcs extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private IfcProvider worker;
	private AppServerProvider asWorker;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Ifcs() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		worker = new IfcProvider();
		asWorker = new AppServerProvider();

		List<Ifc> ifcList = (List<Ifc>) worker.getPIfcSet();
		request.setAttribute("ifcs", ifcList);
		
		//List<AppServer> asList = (List<AppServer>) asWorker.getPAppServers();
		//request.setAttribute("as_list", asList);

		RequestDispatcher view = request
				.getRequestDispatcher("WEB-INF/views/ifc_list.jsp");

		view.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
