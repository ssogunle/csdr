package com.inted.csdr.web;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.inted.csdr.domain.Capability;
import com.inted.csdr.providers.domain.CapabilityProvider;

/**
 * Servlet implementation class Capabilities
 */
@WebServlet("/Capabilities")
public class Capabilities extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Capabilities() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CapabilityProvider worker = new CapabilityProvider();
		
		List<Capability> cList = (List<Capability>) worker.getPCapabilities();

		request.setAttribute("capabilities", cList);

		RequestDispatcher view = request
				.getRequestDispatcher("WEB-INF/views/capabilty_list.jsp");

		view.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
