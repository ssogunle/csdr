package com.inted.csdr.web;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.inted.csdr.domain.ServiceProfile;
import com.inted.csdr.providers.domain.ServiceProfileProvider;
import com.inted.csdr.providers.domain.SubscriptionProfileProvider;

/**
 * Servlet implementation class AddSubscriptionProfile
 */
@WebServlet("/AddSubscriptionProfile")
public class AddSubscriptionProfile extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private SubscriptionProfileProvider worker;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddSubscriptionProfile() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		List<ServiceProfile> spList = new ServiceProfileProvider().getPServiceProfiles();
		request.setAttribute("sp_list", spList);
		
		RequestDispatcher view = request
				.getRequestDispatcher("WEB-INF/views/add_subscription_profile.jsp");

		view.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		worker  = new SubscriptionProfileProvider();
		
		String pName = request.getParameter("profile_name");	
		//String dsai = request.getParameter("dsai_value");
		String serviceProfileId = request.getParameter("service_profile");
		
		//IMPI 
		String impiUri = request.getParameter("impi_uri");
		String impiKey = request.getParameter("impi_key");
		
		//IMPUs
		//id_type, sip_uri, barring_ind, can_register, display_name, psi_activation
		String[] idType = request.getParameterValues("id_type");
		String[] sipUri = request.getParameterValues("sip_uri");
		String[] barring = request.getParameterValues("barring_ind");
		String[] canRegister = request.getParameterValues("can_register");
		String[] displayName = request.getParameterValues("display_name");
		String[] psiActivation = request.getParameterValues("psi_activation");
		

		try {
		//	worker.add(pName,serviceProfileId, impiUri, impiKey, idType, sipUri, barring, canRegister, displayName, psiActivation);
			response.sendRedirect("SubscriptionProfiles");
		} catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("opr", "[Add Subscription Profile]");
			request.setAttribute("error", e);
			RequestDispatcher view  = request.getRequestDispatcher("WEB-INF/views/error.jsp");
			view.forward(request, response);
			
		}
		
	}

}
