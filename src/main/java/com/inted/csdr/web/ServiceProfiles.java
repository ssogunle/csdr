package com.inted.csdr.web;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.inted.csdr.domain.ServiceProfile;
import com.inted.csdr.providers.domain.ServiceProfileProvider;

/**
 * Servlet implementation class ServletProfiles
 */
@WebServlet("/ServiceProfiles")
public class ServiceProfiles extends HttpServlet {
	
	   private static final long serialVersionUID = 1L;
       private ServiceProfileProvider worker;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServiceProfiles() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		worker = new ServiceProfileProvider();
		
		List<ServiceProfile> spList = worker.getPServiceProfiles();
		request.setAttribute("service_profiles", spList);

		RequestDispatcher view  = request.getRequestDispatcher("WEB-INF/views/service_profile_list.jsp");
		
		view.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
