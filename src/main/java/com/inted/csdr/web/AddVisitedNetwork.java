package com.inted.csdr.web;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.inted.csdr.domain.VisitedNetwork;
import com.inted.csdr.providers.domain.VisitedNetworkProvider;

/**
 * Servlet implementation class AddRoamingNetwork
 */
@WebServlet("/AddVisitedNetwork")
public class AddVisitedNetwork extends HttpServlet {
	private static final long serialVersionUID = 1L;
	final Logger LOG = LoggerFactory.getLogger(AddVisitedNetwork.class);
	
	private VisitedNetworkProvider worker;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddVisitedNetwork() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher view = request
				.getRequestDispatcher("WEB-INF/views/add_visited_network.jsp");

		view.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		worker = new VisitedNetworkProvider();
		
		VisitedNetwork vn = new VisitedNetwork();
		
		String name = request.getParameter("name");
		vn.setName(name);
		
		worker.add(vn);
		
		response.sendRedirect("VisitedNetworks");
	}

}
