package com.inted.csdr.web;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import org.apache.olingo.server.api.OData;
import org.apache.olingo.server.api.ODataHttpHandler;
import org.apache.olingo.server.api.ServiceMetadata;

import org.apache.olingo.commons.api.edmx.EdmxReference;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.inted.csdr.processors.CsdrComplexCollectionProcessor;
import com.inted.csdr.processors.CsdrEntityCollectionProcessor;
import com.inted.csdr.processors.CsdrEntityProcessor;
import com.inted.csdr.providers.CsdrDataProducer;
import com.inted.csdr.providers.CsdrEdmProvider;

/**
 * 
 * @author Segun Sogunle
 * Servlet implementation class CsdrApp
 */

public class CsdrApp extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final Logger LOG =  LoggerFactory.getLogger(CsdrApp.class);
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CsdrApp() {
        super();
        // TODO Auto-generated constructor stub
    }
    
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		LOG.info("CSDR web module started....");
     
		HttpSession session = request.getSession(true);
		
		CsdrDataProducer dp = (CsdrDataProducer) session.getAttribute(CsdrDataProducer.class.getName());
	  
		 if (dp == null) 
		   {
		       dp = new CsdrDataProducer();
		      session.setAttribute(CsdrDataProducer.class.getName(), dp);
		      
		      LOG.info("Created new data producer.");
		   }
		 
        // create odata handler and configure it with EdmProvider and Processor
		
        OData odataStack = OData.newInstance();
        
          ServiceMetadata edm = odataStack.createServiceMetadata(new CsdrEdmProvider(), new ArrayList<EdmxReference>());
         
          ODataHttpHandler handler = odataStack.createHandler(edm);
          
          handler.register(new CsdrEntityCollectionProcessor(dp));
          handler.register(new CsdrEntityProcessor(dp));
          handler.register(new CsdrComplexCollectionProcessor(dp));
        //  handler.register(new BaseEntityProcessor(dp));
        //  handler.register(new BasePrimitiveProcessor(dp));
          
         handler.process(request, response);
        
		
	}

}
