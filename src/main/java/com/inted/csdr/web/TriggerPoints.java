package com.inted.csdr.web;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.inted.csdr.domain.TriggerPoint;
import com.inted.csdr.providers.domain.TriggerPointProvider;

/**
 * Servlet implementation class TriggerPoints
 */
@WebServlet("/TriggerPoints")
public class TriggerPoints extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	private TriggerPointProvider worker; 
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TriggerPoints() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		worker = new TriggerPointProvider();
		
		List<TriggerPoint> tpList = worker.getPTriggerPoints(); 
		
		request.setAttribute("trigger_points", tpList);

		RequestDispatcher view  = request.getRequestDispatcher("WEB-INF/views/trigger_point_list.jsp");
		
		view.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
