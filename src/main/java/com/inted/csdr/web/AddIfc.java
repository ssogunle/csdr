package com.inted.csdr.web;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.inted.csdr.domain.AppServer;
import com.inted.csdr.domain.Ifc;
import com.inted.csdr.domain.TriggerPoint;
import com.inted.csdr.domain.embeddables.ShPurPermission;
import com.inted.csdr.domain.embeddables.ShUdrPermission;
import com.inted.csdr.domain.models.enumerated.ProfilePartIndicator;
import com.inted.csdr.providers.domain.AppServerProvider;
import com.inted.csdr.providers.domain.IfcProvider;
import com.inted.csdr.providers.domain.TriggerPointProvider;

/**
 * Servlet implementation class AddIfc
 */
@WebServlet("/AddIfc")
public class AddIfc extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	final private Logger LOG = LoggerFactory.getLogger(AddIfc.class);
	
	private IfcProvider worker;
	private AppServerProvider asWorker;
	private TriggerPointProvider tpWorker;
	

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AddIfc() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		tpWorker = new TriggerPointProvider();
		asWorker = new AppServerProvider();
		
		List<AppServer> asList = asWorker.getPAppServers();
		List<TriggerPoint> tpList = tpWorker.getPTriggerPoints();
		
		RequestDispatcher view = request
				.getRequestDispatcher("WEB-INF/views/add_ifc.jsp");

		request.setAttribute("as_list", asList);
		request.setAttribute("tp_list", tpList);
		
		view.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		worker = new IfcProvider();

		Ifc ifc = new Ifc();
		
		String name = request.getParameter("name");
		ifc.setName(name);
		
		String appServerId = request.getParameter("app_server_id");
		AppServer as = new AppServerProvider().findPAppServer(appServerId);
		
		//if(as!=null)
		//ifc.setAppServer(as);
		
		String tpId = request.getParameter("tp_id");
		TriggerPoint tp = new TriggerPointProvider().findPTriggerPoint(tpId);
		//ifc.setTriggerPoint(tp);
		
		String ppiId = request.getParameter("ppi");
		ifc.setProfilePartIndicator(ProfilePartIndicator.getEValue(Integer.parseInt(ppiId)));
	
		LOG.info("TP is null: "+(tp==null));
		LOG.info("AS is null: "+(as==null));
		
		if(tp!=null && as!=null)
		//worker.addIfc(ifc,as,tp);

		response.sendRedirect("Ifcs");
	}

}
