package com.inted.csdr.web;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Servlet implementation class Subscriptions
 */
@WebServlet("/Subscriptions")
public class Subscriptions extends HttpServlet {
	final Logger LOG = LoggerFactory.getLogger(Subscriptions.class);
	private static final long serialVersionUID = 1L;
      // private ImsSubscriptionWorker service;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Subscriptions() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
//		service = new ImsSubscriptionWorker();
		
//		List<ImsSubscription> sList = (List<ImsSubscription>) service.getPImsSubscriptions(); 
		
	//	LOG.info("SUBS LIST SIZE: "+sList.size());
		
	//	request.setAttribute("subscription_list", sList);

		RequestDispatcher view  = request.getRequestDispatcher("WEB-INF/views/subscription_list.jsp");
		
		view.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
