package com.inted.csdr.web;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//import com.inted.csdr.domain.UserDocument1;
//import com.inted.csdr.services.domain.UserDocumentService1;

/**
 * Servlet implementation class Documents
 */
@WebServlet("/Documents")
public class Documents extends HttpServlet {
	private static final long serialVersionUID = 1L;
	//private UserDocumentService1 service;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Documents() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	
	//		service = new UserDocumentService1();

	//		List<UserDocument1> docs =  service.getDocuments();

	//		request.setAttribute("documents_list", docs);

			RequestDispatcher view = request.getRequestDispatcher("WEB-INF/views/documents_list.jsp");

			view.forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

}
