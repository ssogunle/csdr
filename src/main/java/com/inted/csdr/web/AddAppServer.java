package com.inted.csdr.web;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.inted.csdr.domain.AppServer;
import com.inted.csdr.domain.embeddables.ShPurPermission;
import com.inted.csdr.domain.embeddables.ShUdrPermission;
import com.inted.csdr.providers.domain.AppServerProvider;

/**
 * Servlet implementation class AddAppServer
 */
@WebServlet("/AddAppServer")
public class AddAppServer extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	final Logger LOG = LoggerFactory.getLogger(AddAppServer.class);
	
	private AppServerProvider worker;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddAppServer() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		RequestDispatcher view = request
				.getRequestDispatcher("WEB-INF/views/add_app_server.xhtml");

		view.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		worker = new AppServerProvider();
		
		AppServer as = new AppServer();
		
		String name = request.getParameter("name");
		as.setName(name);
		
		String sipUri = request.getParameter("sip_uri");
		as.setServerName(sipUri);
		
		String diameterFqdn = request.getParameter("diameter_fqdn");
		as.setDiameterUri(diameterFqdn);
		
		ShUdrPermission udr =new ShUdrPermission();
		udr.setAliasesRepositoryData(false); udr.setChargingInfo(false); udr.setDsai(false); udr.setIfc(false); udr.setImpuIdentifier(true); 
		udr.setImsUserState(true); udr.setIsAllowed(true); udr.setPsiActivation(false);
		as.setShUdrPermission(udr);
		
		ShPurPermission pur = new ShPurPermission();
		pur.setDsai(false); pur.setIsAllowed(false); //pur.setPsiActivation(false);;
		as.setShPurPermission(pur);
		
		as.setIncludeRegisterRequest(false);
		as.setIncludeRegisterResponse(false);
		
		worker.add(as);
		
		response.sendRedirect("AppServers");
	}

}
