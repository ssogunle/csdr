package com.inted.csdr.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.inted.csdr.domain.AppServer;
import com.inted.csdr.domain.Ifc;
import com.inted.csdr.domain.ServiceProfile;
import com.inted.csdr.domain.TriggerPoint;
import com.inted.csdr.providers.domain.AppServerProvider;
import com.inted.csdr.providers.domain.IfcProvider;
import com.inted.csdr.providers.domain.ServiceProfileProvider;
import com.inted.csdr.providers.domain.TriggerPointProvider;

/**
 * Servlet implementation class AddServiceProfile
 */
@WebServlet("/AddServiceProfile")
public class AddServiceProfile extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	final Logger LOG = LoggerFactory.getLogger(AddServiceProfile.class);
	private ServiceProfileProvider worker;
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddServiceProfile() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		List<AppServer> asList =  new AppServerProvider().getPAppServers();
		List<TriggerPoint> tpList =  new TriggerPointProvider().getPTriggerPoints();

		request.setAttribute("as_list", asList);
		request.setAttribute("tp_list", tpList);
		
		RequestDispatcher view = request
				.getRequestDispatcher("WEB-INF/views/add_service_profile.jsp");

		view.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		worker = new ServiceProfileProvider();
		String spName = request.getParameter("name");	
		//String serviceAuth = request.getParameter("service_auth");
		String[] ifcPriority = request.getParameterValues("ifc_priority");
		String[] ifcName = request.getParameterValues("ifc_name");
		String[] asId = request.getParameterValues("as_id");
		String[] tpId = request.getParameterValues("tp_id");
		String[] ppi = request.getParameterValues("ppi");
		
		try {
		//	worker.add(spName,ifcPriority , ifcName, asId, tpId, ppi);
			response.sendRedirect("ServiceProfiles");
		} catch (Exception e) {
			e.printStackTrace();
			request.setAttribute("opr", "[Add Service Profile]");
			request.setAttribute("error", e);
			RequestDispatcher view  = request.getRequestDispatcher("WEB-INF/views/error.jsp");
			view.forward(request, response);
			
		}
	
	}

}
