package com.inted.csdr.web;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.inted.csdr.domain.SubscriptionProfile;
import com.inted.csdr.providers.domain.SubscriptionProfileProvider;

/**
 * Servlet implementation class SubscriptionProfiles
 */
@WebServlet("/SubscriptionProfiles")
public class SubscriptionProfiles extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SubscriptionProfiles() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		SubscriptionProfileProvider worker = new SubscriptionProfileProvider();
		List<SubscriptionProfile> spList = worker.getPSubscriptionProfiles();
		request.setAttribute("subscription_profiles", spList);
		RequestDispatcher view  = request.getRequestDispatcher("WEB-INF/views/subscription_profile_list.jsp");
		view.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
