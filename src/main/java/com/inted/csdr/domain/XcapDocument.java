package com.inted.csdr.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * Entity implementation class for Entity: XcapDocument
 *
 */
@Entity
public class XcapDocument implements Serializable {
	@Transient
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer Id;
	private String appUsage;
	private String xui;  //User Identifier : Can be SIP-URI
	private String name;
	private String data;
	private String eTag;
	private Boolean isUpdated;
	
	public XcapDocument() {
		super();
	}

	public Integer getId() {
		return Id;
	}

	public void setId(Integer id) {
		Id = id;
	}

	public String getAppUsage() {
		return appUsage;
	}

	public void setAppUsage(String appUsage) {
		this.appUsage = appUsage;
	}

	public String getXui() {
		return xui;
	}

	public void setXui(String xui) {
		this.xui = xui;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String geteTag() {
		return eTag;
	}

	public void seteTag(String eTag) {
		this.eTag = eTag;
	}

	public Boolean getIsUpdated() {
		return isUpdated;
	}

	public void setIsUpdated(Boolean isUpdated) {
		this.isUpdated = isUpdated;
	}
   
}
