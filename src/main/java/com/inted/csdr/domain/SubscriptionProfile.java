package com.inted.csdr.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

import com.inted.csdr.domain.embeddables.ChargingInfo;
import com.inted.csdr.domain.models.enumerated.DsaiValue;


/**
 * Entity implementation class for Entity: SubscriptionProfile
 *
 */
@Entity
public class SubscriptionProfile implements Serializable {

	@Transient
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private String Id;
	
	private String name;
	
	//@OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY)
	//private List<Impi> impiList;
	//IMPI used as Address-of-Record (AOR)
	@OneToOne(cascade = {CascadeType.ALL})
	private Impi impi;
	
	@OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
	private List<Impu> impuList;
	
	@Embedded
	private ChargingInfo chargingInfo;
	
	@Enumerated(EnumType.STRING)
	private DsaiValue dsaiValue;
	
	@OneToOne
	private ServiceProfile serviceProfile;
	
	
	private String scscfName;
	
	//private List<RefLocInfo> refLocInfo;
	
	public SubscriptionProfile() {
		super();
	}

	public String getId() {
		return Id;
	}

	public void setId(String id) {
		Id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Impu> getImpuList() {
		return impuList;
	}

	public void setImpuList(List<Impu> impuList) {
		this.impuList = impuList;
	}

	public Impi getImpi() {
		return impi;
	}

	public void setImpi(Impi impi) {
		this.impi = impi;
	}

	public ChargingInfo getChargingInfo() {
		return chargingInfo;
	}

	public void setChargingInfo(ChargingInfo chargingInfo) {
		this.chargingInfo = chargingInfo;
	}

	public ServiceProfile getServiceProfile() {
		return serviceProfile;
	}

	public void setServiceProfile(ServiceProfile serviceProfile) {
		this.serviceProfile = serviceProfile;
	}

	public DsaiValue getDsaiValue() {
		return dsaiValue;
	}

	public void setDsaiValue(DsaiValue dsaiValue) {
		this.dsaiValue = dsaiValue;
	}

	public String getScscfName() {
		return scscfName;
	}

	public void setScscfName(String scscfName) {
		this.scscfName = scscfName;
	}


}
