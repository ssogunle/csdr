package com.inted.csdr.domain;

import java.io.Serializable;
import javax.persistence.*;

import com.inted.csdr.domain.models.enumerated.IdentityType;
import com.inted.csdr.domain.models.enumerated.ImsUserState;
import com.inted.csdr.domain.models.enumerated.PsiActivation;


/**
 * Entity implementation class for Entity: RegisteredIdentity
 *
 */
@Entity
public class RegisteredIdentity implements Serializable {

	@Transient
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer Id;
	private String impi;
	private String impu;
	private byte[] authKey;
	private byte[] authOp;
	private byte[] authAmf;
	private String authSqn;
	private Integer authScheme;
	private String authLineId;
	private String authIpAddress;
	private Boolean barring;
	private Boolean canRegister;
	private String visitedNetwork;
	private String scscfName;
	@Enumerated(EnumType.STRING)
	private IdentityType impuType;
	@Enumerated(EnumType.STRING)
	private PsiActivation psiActivation;
	@Enumerated(EnumType.STRING)
	private ImsUserState userState;

	public RegisteredIdentity() {
		super();
	}

	public Integer getId() {
		return Id;
	}

	public void setId(Integer id) {
		Id = id;
	}

	public String getImpi() {
		return impi;
	}

	public void setImpi(String impi) {
		this.impi = impi;
	}

	public String getImpu() {
		return impu;
	}

	public void setImpu(String impu) {
		this.impu = impu;
	}

	public byte[] getAuthKey() {
		return authKey;
	}

	public void setAuthKey(byte[] authKey) {
		this.authKey = authKey;
	}

	public byte[] getAuthOp() {
		return authOp;
	}

	public void setAuthOp(byte[] authOp) {
		this.authOp = authOp;
	}

	public byte[] getAuthAmf() {
		return authAmf;
	}

	public void setAuthAmf(byte[] authAmf) {
		this.authAmf = authAmf;
	}

	public String getAuthSqn() {
		return authSqn;
	}

	public void setAuthSqn(String authSqn) {
		this.authSqn = authSqn;
	}

	public Integer getAuthScheme() {
		return authScheme;
	}

	public void setAuthScheme(Integer authScheme) {
		this.authScheme = authScheme;
	}

	public String getAuthLineId() {
		return authLineId;
	}

	public void setAuthLineId(String authLineId) {
		this.authLineId = authLineId;
	}

	public String getAuthIpAddress() {
		return authIpAddress;
	}

	public void setAuthIpAddress(String authIpAddress) {
		this.authIpAddress = authIpAddress;
	}

	public Boolean getBarring() {
		return barring;
	}

	public void setBarring(Boolean barring) {
		this.barring = barring;
	}

	public Boolean getCanRegister() {
		return canRegister;
	}

	public void setCanRegister(Boolean canRegister) {
		this.canRegister = canRegister;
	}

	public String getVisitedNetwork() {
		return visitedNetwork;
	}

	public void setVisitedNetwork(String visitedNetwork) {
		this.visitedNetwork = visitedNetwork;
	}

	public String getScscfName() {
		return scscfName;
	}

	public void setScscfName(String scscfName) {
		this.scscfName = scscfName;
	}

	public IdentityType getImpuType() {
		return impuType;
	}

	public void setImpuType(IdentityType impuType) {
		this.impuType = impuType;
	}

	public PsiActivation getPsiActivation() {
		return psiActivation;
	}

	public void setPsiActivation(PsiActivation psiActivation) {
		this.psiActivation = psiActivation;
	}

	public ImsUserState getUserState() {
		return userState;
	}

	public void setUserState(ImsUserState userState) {
		this.userState = userState;
	}

}
