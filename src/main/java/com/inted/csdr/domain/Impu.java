package com.inted.csdr.domain;

import java.io.Serializable;

import javax.persistence.*;

import com.inted.csdr.domain.models.enumerated.IdentityType;
import com.inted.csdr.domain.models.enumerated.ImsUserState;
import com.inted.csdr.domain.models.enumerated.PsiActivation;

/**
 * Entity implementation class for Entity: ImsPublicIdentity
 *
 */
@Entity
public class Impu implements Serializable {

	@Transient
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private String Id;

	private String sipUri;

	@Enumerated(EnumType.STRING)
	private IdentityType identityType;

	private Boolean barringIndication;

	private Boolean canRegister;

	private String displayName;

	private String visitedNetworkId;

	@Enumerated(EnumType.STRING)
	private PsiActivation psiActivation;

	@Enumerated(EnumType.STRING)
	private ImsUserState imsUserState;

	public Impu() {
		super();
	}

	public String getId() {
		return Id;
	}

	public void setId(String id) {
		Id = id;
	}

	public String getSipUri() {
		return sipUri;
	}

	public void setSipUri(String sipUri) {
		this.sipUri = sipUri;
	}

	public IdentityType getIdentityType() {
		return identityType;
	}

	public void setIdentityType(IdentityType identityType) {
		this.identityType = identityType;
	}

	public Boolean getBarringIndication() {
		return barringIndication;
	}

	public void setBarringIndication(Boolean barringIndication) {
		this.barringIndication = barringIndication;
	}

	public Boolean getCanRegister() {
		return canRegister;
	}

	public void setCanRegister(Boolean canRegister) {
		this.canRegister = canRegister;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getVisitedNetworkId() {
		return visitedNetworkId;
	}

	public void setVisitedNetworkId(String visitedNetworkId) {
		this.visitedNetworkId = visitedNetworkId;
	}

	public PsiActivation getPsiActivation() {
		return psiActivation;
	}

	public void setPsiActivation(PsiActivation psiActivation) {
		this.psiActivation = psiActivation;
	}

	public ImsUserState getImsUserState() {
		return imsUserState;
	}

	public void setImsUserState(ImsUserState imsUserState) {
		this.imsUserState = imsUserState;
	}
}
