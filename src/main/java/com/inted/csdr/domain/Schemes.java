package com.inted.csdr.domain;

public class Schemes {

	public static int AKAv1 =1;
	public static int AKAv2 =2;
	public static int Auth_Scheme_MD5 =4;
	public static int Digest =8;
	public static int HTTP_Digest_MD5 =16;
	public static int Early =32;
	public static int NASS_Bundled =64;
	public static int SIP_Digest =128;
	public static int All =255;
}
