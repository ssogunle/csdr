package com.inted.csdr.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: CapabilitySet
 *
 */
//@Entity

public class CapabilitySet implements Serializable {

	@Transient
	private static final long serialVersionUID = 1L;

	@Id
	private String Id;
	
	private String name;
	
	//@OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY)
	private List<Capability> capabilityList;
	
	public CapabilitySet() {
		super();
	}

	public String getId() {
		return Id;
	}

	public void setId(String id) {
		Id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Capability> getCapabilityList() {
		return capabilityList;
	}

	public void setCapabilityList(List<Capability> capabilityList) {
		this.capabilityList = capabilityList;
	}
	
}
