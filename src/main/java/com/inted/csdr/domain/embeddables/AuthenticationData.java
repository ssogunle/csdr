package com.inted.csdr.domain.embeddables;

import java.io.Serializable;

import javax.persistence.*;

/**
 * Entity implementation class for Embeddable: AuthenticationData
 *
 */
@Embeddable
public class AuthenticationData implements Serializable {

	@Transient
	private static final long serialVersionUID = 1L;

	public AuthenticationData() {
		super();
	}

	// Secret Key
	private byte[] secretKey;
	private byte[] op;
	private byte[] amf;
	private String sqn;
	private String ipAddress;
	private String lineIdentifier;

	public byte[] getSecretKey() {
		return secretKey;
	}
	public void setSecretKey(byte[] secretKey) {
		this.secretKey = secretKey;
	}
	public byte[] getOp() {
		return op;
	}
	public void setOp(byte[] op) {
		this.op = op;
	}
	public byte[] getAmf() {
		return amf;
	}
	public void setAmf(byte[] bs) {
		this.amf = bs;
	}
	public String getSqn() {
		return sqn;
	}
	public void setSqn(String sqn) {
		this.sqn = sqn;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public String getLineIdentifier() {
		return lineIdentifier;
	}
	public void setLineIdentifier(String lineIdentifier) {
		this.lineIdentifier = lineIdentifier;
	}
	
	
}
