package com.inted.csdr.domain.embeddables;

import java.io.Serializable;

import javax.persistence.*;

import com.inted.csdr.domain.models.enumerated.SptType;

/**
 * 
 * Entity implementation class for Entity: ServicePointTrigger
 *
 */
@Embeddable
public class Spt implements Serializable {

	@Transient
	private static final long serialVersionUID = 1L;

	private Integer grpId;

	private Boolean conditionNegated;

	@Enumerated(EnumType.STRING)
	private SptType type;

	private String data;

	public Spt() {
		super();
	}

	public Integer getGrpId() {
		return grpId;
	}

	public void setGrpId(Integer grpId) {
		this.grpId = grpId;
	}

	public Boolean getConditionNegated() {
		return conditionNegated;
	}

	public void setConditionNegated(Boolean conditionNegated) {
		this.conditionNegated = conditionNegated;
	}

	public SptType getType() {
		return type;
	}

	public void setType(SptType type) {
		this.type = type;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}
}
