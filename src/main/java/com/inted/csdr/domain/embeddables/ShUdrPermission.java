package com.inted.csdr.domain.embeddables;

import java.io.Serializable;
import javax.persistence.*;


/**
 * Entity implementation class for Entity: ShUdrPermission
 *
 */
@Embeddable
public class ShUdrPermission implements Serializable {

	private static final long serialVersionUID = 1L;

	private Boolean isAllowed;
	private Boolean impuIdentifier;
    private Boolean imsUserState;
	private Boolean ifc;
	private Boolean chargingInfo;
	private Boolean psiActivation;
	private Boolean dsai;
	private Boolean aliasesRepositoryData;
	
	
	public ShUdrPermission() {
		super();
	}

	public Boolean getIsAllowed() {
		return isAllowed;
	}


	public void setIsAllowed(Boolean isAllowed) {
		this.isAllowed = isAllowed;
	}


	public Boolean getImpuIdentifier() {
		return impuIdentifier;
	}


	public void setImpuIdentifier(Boolean impuIdentifier) {
		this.impuIdentifier = impuIdentifier;
	}


	public Boolean getImsUserState() {
		return imsUserState;
	}


	public void setImsUserState(Boolean imsUserState) {
		this.imsUserState = imsUserState;
	}


	public Boolean getIfc() {
		return ifc;
	}


	public void setIfc(Boolean ifc) {
		this.ifc = ifc;
	}


	public Boolean getChargingInfo() {
		return chargingInfo;
	}


	public void setChargingInfo(Boolean chargingInfo) {
		this.chargingInfo = chargingInfo;
	}


	public Boolean getPsiActivation() {
		return psiActivation;
	}


	public void setPsiActivation(Boolean psiActivation) {
		this.psiActivation = psiActivation;
	}


	public Boolean getDsai() {
		return dsai;
	}


	public void setDsai(Boolean dsai) {
		this.dsai = dsai;
	}


	public Boolean getAliasesRepositoryData() {
		return aliasesRepositoryData;
	}


	public void setAliasesRepositoryData(Boolean aliasesRepositoryData) {
		this.aliasesRepositoryData = aliasesRepositoryData;
	}

	@Override
	public String toString() {
		return "ShUdrPermission [isAllowed=" + isAllowed + ", impuIdentifier=" + impuIdentifier + ", imsUserState="
				+ imsUserState + ", ifc=" + ifc + ", chargingInfo=" + chargingInfo + ", psiActivation=" + psiActivation
				+ ", dsai=" + dsai + ", aliasesRepositoryData=" + aliasesRepositoryData + "]";
	}


	
	
	
}
