package com.inted.csdr.domain.embeddables;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: ShPurPermission
 *
 */
@Embeddable
public class ShPurPermission implements Serializable {

	@Transient
	private static final long serialVersionUID = 1L;

	private Boolean isAllowed;
	private Boolean psiActivation;
	private Boolean dsai;

	// private String aliasesRepositoryData;

	public ShPurPermission() {
		super();
	}

	public Boolean getIsAllowed() {
		return isAllowed;
	}

	public void setIsAllowed(Boolean isAllowed) {
		this.isAllowed = isAllowed;
	}

	public Boolean getPsiActivation() {
		return psiActivation;
	}

	public void setpsiActivation(Boolean psiActivation) {
		this.psiActivation = psiActivation;
	}

	public Boolean getDsai() {
		return dsai;
	}

	public void setDsai(Boolean dsai) {
		this.dsai = dsai;
	}

	@Override
	public String toString() {
		return "ShPurPermission [isAllowed=" + isAllowed + ", psiActivation=" + psiActivation + ", dsai=" + dsai + "]";
	}
	
	

}
