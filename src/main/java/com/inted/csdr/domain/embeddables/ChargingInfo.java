package com.inted.csdr.domain.embeddables;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: ChargingInfo
 *
 */
@Embeddable
public class ChargingInfo implements Serializable {

	@Transient
	private static final long serialVersionUID = 1L;

	public ChargingInfo() {
		super();
	}
	//Diameter FQDNs
	private String primaryCcf;
	private String secondaryCcf;
	private String primaryEcf;
	private String secondaryEcf;

	public String getPrimaryCcf() {
		return primaryCcf;
	}
	public void setPrimaryCcf(String primaryCcf) {
		this.primaryCcf = primaryCcf;
	}
	public String getSecondaryCcf() {
		return secondaryCcf;
	}
	public void setSecondaryCcf(String secondaryCcf) {
		this.secondaryCcf = secondaryCcf;
	}
	public String getPrimaryEcf() {
		return primaryEcf;
	}
	public void setPrimaryEcf(String primaryEcf) {
		this.primaryEcf = primaryEcf;
	}
	public String getSecondaryEcf() {
		return secondaryEcf;
	}
	public void setSecondaryEcf(String secondaryEcf) {
		this.secondaryEcf = secondaryEcf;
	}
	
}
