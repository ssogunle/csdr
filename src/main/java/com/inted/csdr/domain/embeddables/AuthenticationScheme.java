package com.inted.csdr.domain.embeddables;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

import com.inted.csdr.domain.models.enumerated.SchemeEnum;

/**
 * Entity implementation class for Entity: AuthenticationScheme
 *
 */
@Embeddable
public class AuthenticationScheme implements Serializable {

	@Transient
	private static final long serialVersionUID = 1L;

	@ElementCollection
	private List<Integer> supportedSchemes;

	private Integer defaultScheme;

	public AuthenticationScheme() {
		super();
	}

	public List<Integer> getSupportedSchemes() {
		return supportedSchemes;
	}

	public void setSupportedSchemes(List<Integer> supportedSchemes) {
		this.supportedSchemes = supportedSchemes;
	}

	public Integer getDefaultScheme() {
		return defaultScheme;
	}

	public void setDefaultScheme(Integer defaultScheme) {
		this.defaultScheme = defaultScheme;
	}

}
