package com.inted.csdr.domain;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Capability
 *
 */
//@Entity

public class Capability implements Serializable {

	@Transient
	private static final long serialVersionUID = 1L;

	@Id
	private String Id;
	private String name;
	
	public Capability() {
		super();
	}

	public String getId() {
		return Id;
	}

	public void setId(String id) {
		Id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
   
}
