package com.inted.csdr.domain;

import java.io.Serializable;

import javax.persistence.*;

import com.inted.csdr.domain.embeddables.ShPurPermission;
import com.inted.csdr.domain.embeddables.ShUdrPermission;

/**
 * Entity implementation class for Entity: AppServer
 *
 */
@Entity
public class AppServer implements Serializable {

	@Transient
	private static final long serialVersionUID = 1L;

	public AppServer() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private String Id;
	private String name;
	private String serverName;
	private String diameterUri;
	private String defaultHandling;
	private String serviceInfo;
	private Boolean includeRegisterRequest;
	private Boolean includeRegisterResponse;

	@Embedded
	private ShUdrPermission shUdrPermission;

	@Embedded
	private ShPurPermission shPurPermission;

	public String getId() {
		return Id;
	}

	public void setId(String id) {
		Id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getServerName() {
		return serverName;
	}

	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

	public String getDiameterUri() {
		return diameterUri;
	}

	public void setDiameterUri(String diameterUri) {
		this.diameterUri = diameterUri;
	}

	public String getDefaultHandling() {
		return defaultHandling;
	}

	public void setDefaultHandling(String defaultHandling) {
		this.defaultHandling = defaultHandling;
	}

	public String getServiceInfo() {
		return serviceInfo;
	}

	public void setServiceInfo(String serviceInfo) {
		this.serviceInfo = serviceInfo;
	}

	public Boolean getIncludeRegisterRequest() {
		return includeRegisterRequest;
	}

	public void setIncludeRegisterRequest(Boolean includeRegisterRequest) {
		this.includeRegisterRequest = includeRegisterRequest;
	}

	public Boolean getIncludeRegisterResponse() {
		return includeRegisterResponse;
	}

	public void setIncludeRegisterResponse(Boolean includeRegisterResponse) {
		this.includeRegisterResponse = includeRegisterResponse;
	}

	public ShUdrPermission getShUdrPermission() {
		return shUdrPermission;
	}

	public void setShUdrPermission(ShUdrPermission shUdrPermission) {
		this.shUdrPermission = shUdrPermission;
	}

	public ShPurPermission getShPurPermission() {
		return shPurPermission;
	}

	public void setShPurPermission(ShPurPermission shPurPermission) {
		this.shPurPermission = shPurPermission;
	}

	@Override
	public int hashCode() {
		return Id != null ? this.getClass().hashCode() + Id.hashCode() : super.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		return obj instanceof AppServer && (Id != null) ? Id.equals(((AppServer) obj).Id) : (obj == this);
	}

	@Override
	public String toString() {
		return "AppServer [Id=" + Id + ", name=" + name + ", serverName=" + serverName + ", diameterUri=" + diameterUri
				+ ", defaultHandling=" + defaultHandling + ", serviceInfo=" + serviceInfo + ", includeRegisterRequest="
				+ includeRegisterRequest + ", includeRegisterResponse=" + includeRegisterResponse + ", shUdrPermission="
				+ shUdrPermission + ", shPurPermission=" + shPurPermission + "]";
	}

}
