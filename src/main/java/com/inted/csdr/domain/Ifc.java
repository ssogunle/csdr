package com.inted.csdr.domain;

import java.io.Serializable;
import javax.persistence.*;
import com.inted.csdr.domain.models.enumerated.ProfilePartIndicator;



/**
 * Entity implementation class for Entity: Ifc
 *
 */
@Entity
public class Ifc implements Serializable {

	@Transient
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private String Id;
	private Integer priority;
	private String name;
	
	@Enumerated(EnumType.STRING)
	private ProfilePartIndicator profilePartIndicator;
	
	@OneToOne
	private TriggerPoint triggerPoint;
	
	@OneToOne
	private AppServer appServer;
	
	public String getId() {
		return Id;
	}
	public void setId(String id) {
		Id = id;
	}
	public Integer getPriority() {
		return priority;
	}
	public void setPriority(Integer priority) {
		this.priority = priority;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public ProfilePartIndicator getProfilePartIndicator() {
		return profilePartIndicator;
	}
	public void setProfilePartIndicator(ProfilePartIndicator profilePartIndicator) {
		this.profilePartIndicator = profilePartIndicator;
	}
	public TriggerPoint getTriggerPoint() {
		return triggerPoint;
	}
	public void setTriggerPoint(TriggerPoint triggerPoint) {
		this.triggerPoint = triggerPoint;
	}
	public AppServer getAppServer() {
		return appServer;
	}
	public void setAppServer(AppServer appServer) {
		this.appServer = appServer;
	}
	
}
