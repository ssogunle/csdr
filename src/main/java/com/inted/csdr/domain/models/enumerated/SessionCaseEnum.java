package com.inted.csdr.domain.models.enumerated;

import java.util.HashMap;
import java.util.Map;

public enum SessionCaseEnum {
	ORIGINATING_REGISTERED(0), TERMINATING_REGISTERED(1), TERMINATING_UNREGISTERED(2), ORIGINATING_UNREGISTERED(3), ORIGINATING_CDIV(4);
	
	int value;
	
	private SessionCaseEnum(int value){
		this.value = value;
	}
	
	 // Mapping SessionCase to dSessionCase id
    private static final Map<Integer, SessionCaseEnum> map = new HashMap<Integer, SessionCaseEnum>();
    static
    {
        for (SessionCaseEnum sessionCase : SessionCaseEnum.values())
            map.put(sessionCase.value, sessionCase);
    }
 
    /**
     * Get SessionCaseEnum from value
     * @param value Value
     * @return SessionCaseEnum
     */
    public static SessionCaseEnum getEValue(int value)
    {
        return map.get(value);
    }
}
