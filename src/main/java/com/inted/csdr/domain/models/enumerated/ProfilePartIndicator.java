package com.inted.csdr.domain.models.enumerated;

import java.util.HashMap;
import java.util.Map;

public enum ProfilePartIndicator {
	REGISTERED(0), UNREGISTERED(1), ANY(-1);

	private int value;

	private ProfilePartIndicator(int value) {
		this.value = value;
	}

	// Mapping ProfilePartIndicator to ProfilePartIndicator id
	private static final Map<Integer, ProfilePartIndicator> map = new HashMap<Integer, ProfilePartIndicator>();
	static {
		for (ProfilePartIndicator ppi : ProfilePartIndicator.values())
			map.put(ppi.value, ppi);
	}

	/**
	 * Get ProfilePartIndicator from value
	 * 
	 * @param value
	 *            Value
	 * @return ProfilePartIndicator
	 */
	public static ProfilePartIndicator getEValue(int value) {
		return map.get(value);
	}
}
