package com.inted.csdr.domain.models.enumerated;

import java.util.HashMap;
import java.util.Map;

public enum SchemeEnum {

	AKAv1(1),AKAv2(2), Auth_Scheme_MD5(4), Digest(8), HTTP_Digest_MD5(16), Early(32), NASS_Bundled(64), SIP_Digest(128), All(255);
	
	int value;

	private SchemeEnum(int value) {
		this.value = value;
	}
	
	public int getValue(){
    	return value;
    }
	
	 // Mapping AuthScheme to AuthScheme id
    private static final Map<Integer, SchemeEnum> map = new HashMap<Integer, SchemeEnum>();
    static
    {
        for (SchemeEnum a : SchemeEnum.values())
            map.put(a.value, a);
    }
 
    /**
     * Get Scheme from value
     * @param value Value
     * @return Scheme
     */
    public static SchemeEnum getEValue(int value)
    {
        return map.get(value);
    }
}
