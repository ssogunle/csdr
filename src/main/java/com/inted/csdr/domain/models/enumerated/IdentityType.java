package com.inted.csdr.domain.models.enumerated;

import java.util.HashMap;
import java.util.Map;

public enum IdentityType {
	PUBLIC_USER_IDENTITY(0), DISTINCT_PSI(1), WILDCARDED_PSI(2);

	private int value;

	private IdentityType(int value) {
		this.value = value;
	}

	// Mapping IdentityType to IdentityType Id
	private static final Map<Integer, IdentityType> map = new HashMap<Integer, IdentityType>();
	static {
		for (IdentityType idType : IdentityType.values())
			map.put(idType.value, idType);
	}

	/**
	 * Get IdentityType from value
	 * 
	 * @param value
	 *            Value
	 * @return IdentityType
	 */
	public static IdentityType getEValue(int value) {
		return map.get(value);
	}
	
	public int getValue(){
		return value;
	}
}
