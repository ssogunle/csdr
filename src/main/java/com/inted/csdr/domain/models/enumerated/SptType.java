package com.inted.csdr.domain.models.enumerated;

import java.util.HashMap;
import java.util.Map;

public enum SptType {
	SIP_METHOD(0), REQUEST_URI(1), SIP_HEADER(2), SESSION_DESCRIPTION(3), SESSION_CASE(4);
	int value;

	private SptType(int value){
		this.value = value;
	}

	// Mapping SptType to SptType id
	private static final Map<Integer,SptType> map = new HashMap<Integer, SptType>();
	static {
		for (SptType type : SptType.values())
			map.put(type.value, type);
	}

	/**
	 * Get SptType from value
	 * 
	 * @param value
	 *            Value
	 * @return SptType
	 */
	public static SptType getEValue(int value) {
		return map.get(value);
	}
	
	  public int getValue(){
	    	return value;
	    }
}
