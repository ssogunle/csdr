package com.inted.csdr.domain.models.enumerated;
/*
 * 
 * @author Segun Sogunle
 * 
 * Enumerated data type for IMS User State according to TS 29.328
 * 
 * 
 * 0 (NOT_REGISTERED)
 * 1 (REGISTERED)
 * 2 (REGISTERED_UNREG_SERVICES)/ UNREGISTERED
 * 3 (AUTHENTICATION_PENDING
 * 
*/
public enum ImsUserState {
	
	NOT_REGISTERED,
	REGISTERED,
	UNREGISTERED,
	AUTHENTICATION_PENDING;
	/*
	private int value;

	private ImsUserState(int value) {
		this.value = value;
	}
	
	public int getValue(){
		return this.value;
	}
	*/
}
