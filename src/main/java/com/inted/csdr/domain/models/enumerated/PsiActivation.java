package com.inted.csdr.domain.models.enumerated;

import java.util.HashMap;
import java.util.Map;

public enum PsiActivation {
	INACTIVE(0), ACTIVE(1);

	private int value;

	private PsiActivation(int value) {
		this.value = value;
	}

	// Mapping PsiActivation to PsiActivation Id
	private static final Map<Integer, PsiActivation> map = new HashMap<Integer, PsiActivation>();
	static {
		for (PsiActivation psi : PsiActivation.values())
			map.put(psi.value, psi);
	}

	/**
	 * Get PsiActivation from value
	 * 
	 * @param value
	 *            Value
	 * @return PsiActivation
	 */
	public static PsiActivation getEValue(int value) {
		return map.get(value);
	}
}
