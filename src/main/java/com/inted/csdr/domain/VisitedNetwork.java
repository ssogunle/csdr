package com.inted.csdr.domain;

import java.io.Serializable;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: VisitedNetwork
 *
 */
@Entity

public class VisitedNetwork implements Serializable {

	@Transient
	private static final long serialVersionUID = 1L;

	public VisitedNetwork() {
		super();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer Id;
	private String name;

	public Integer getId() {
		return Id;
	}

	public void setId(Integer id) {
		Id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
