package com.inted.csdr.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

import com.inted.csdr.domain.embeddables.Spt;

/**
 * Entity implementation class for Entity: TriggerPoint
 *
 */
@Entity
public class TriggerPoint implements Serializable {

	@Transient
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private String Id;

	private String name;

	private Boolean conditionTypeCnf;

	@ElementCollection
	private List<Spt> sptList;

	public TriggerPoint() {
		super();
	}

	public String getId() {
		return Id;
	}

	public void setId(String id) {
		Id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getConditionTypeCnf() {
		return conditionTypeCnf;
	}

	public void setConditionTypeCnf(Boolean conditionTypeCnf) {
		this.conditionTypeCnf = conditionTypeCnf;
	}

	public List<Spt> getSptList() {
		return sptList;
	}

	public void setSptList(List<Spt> sptList) {
		this.sptList = sptList;
	}

	@Override
	public int hashCode() {
		return Id != null ? this.getClass().hashCode() + Id.hashCode() : super.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		return obj instanceof TriggerPoint && (Id != null) ? Id.equals(((TriggerPoint) obj).Id) : (obj == this);
	}

	@Override
	public String toString() {
		return "TriggerPoint [Id=" + Id + ", name=" + name + ", conditionTypeCnf=" + conditionTypeCnf + ", sptGroups="
				+ sptList + "]";
	}

}
