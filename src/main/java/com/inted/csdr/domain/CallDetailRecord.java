package com.inted.csdr.domain;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: CallDetailRecord
 *
 */
@Entity
public class CallDetailRecord implements Serializable {

	@Transient
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer Id;

	private String sipMethod;

	private String callDuration;

	private String toAddress;

	private String fromAddress;

	private String callId;

	private String cSeq;

	private String userAgent;

	private String contact;

	private Boolean containsSdp;

	private Integer sessionExpires;

	public CallDetailRecord() {
		super();
	}

	public Integer getId() {
		return Id;
	}

	public void setId(Integer id) {
		Id = id;
	}

	public String getSipMethod() {
		return sipMethod;
	}

	public void setSipMethod(String sipMethod) {
		this.sipMethod = sipMethod;
	}

	public String getCallDuration() {
		return callDuration;
	}

	public void setCallDuration(String callDuration) {
		this.callDuration = callDuration;
	}

	public String getToAddress() {
		return toAddress;
	}

	public void setToAddress(String toAddress) {
		this.toAddress = toAddress;
	}

	public String getFromAddress() {
		return fromAddress;
	}

	public void setFromAddress(String fromAddress) {
		this.fromAddress = fromAddress;
	}

	public String getCallId() {
		return callId;
	}

	public void setCallId(String callId) {
		this.callId = callId;
	}

	public String getcSeq() {
		return cSeq;
	}

	public void setcSeq(String cSeq) {
		this.cSeq = cSeq;
	}

	public String getUserAgent() {
		return userAgent;
	}

	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public Boolean getContainsSdp() {
		return containsSdp;
	}

	public void setContainsSdp(Boolean containsSdp) {
		this.containsSdp = containsSdp;
	}

	public Integer getSessionExpires() {
		return sessionExpires;
	}

	public void setSessionExpires(Integer sessionExpires) {
		this.sessionExpires = sessionExpires;
	}


}
