package com.inted.csdr.domain;

import java.io.Serializable;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Scscf
 *
 */
@Entity
public class Scscf implements Serializable {

	@Transient
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private String Id;
	private String name;
	private String sipUri; 
	private String diameterUri; 
	
	public Scscf() {
		super();
	}

	public String getId() {
		return Id;
	}

	public void setId(String id) {
		Id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSipUri() {
		return sipUri;
	}

	public void setSipUri(String sipUri) {
		this.sipUri = sipUri;
	}

	public String getDiameterUri() {
		return diameterUri;
	}

	public void setDiameterUri(String diameterUri) {
		this.diameterUri = diameterUri;
	}
   
}
