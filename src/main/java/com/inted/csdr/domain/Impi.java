package com.inted.csdr.domain;

import java.io.Serializable;

import javax.persistence.*;

import com.inted.csdr.domain.embeddables.AuthenticationData;
import com.inted.csdr.domain.embeddables.AuthenticationScheme;

/**
 * Entity implementation class for Entity: Impi
 *
 */
@Entity
public class Impi implements Serializable {

	@Transient
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private String Id;
	
	private String uri;

	@Embedded
	private AuthenticationScheme authScheme;
	
	@Embedded
	private AuthenticationData authData;
	
	public Impi() {
		super();
	}

	public String getId() {
		return Id;
	}

	public void setId(String id) {
		Id = id;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public AuthenticationScheme getAuthScheme() {
		return authScheme;
	}

	public void setAuthScheme(AuthenticationScheme authScheme) {
		this.authScheme = authScheme;
	}

	public AuthenticationData getAuthData() {
		return authData;
	}

	public void setAuthData(AuthenticationData authData) {
		this.authData = authData;
	}
}