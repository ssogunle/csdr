package com.inted.csdr.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: ServiceProfile
 *
 */
@Entity
public class ServiceProfile implements Serializable {

	@Transient
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private String Id;

	private String name;

	@OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
	private List<Ifc> ifcList;

	// private CoreNetAuth coreNetAuth;
	// private SharedIfcSet sharedIfcSet;

	public ServiceProfile() {
		super();
	}

	public String getId() {
		return Id;
	}

	public void setId(String id) {
		Id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Ifc> getIfcList() {
		return ifcList;
	}

	public void addIfc(Ifc ifc){
		
		if(this.ifcList==null)
			this.ifcList = new ArrayList<Ifc>();
		
			this.ifcList.add(ifc);
	}
	public void setIfcList(List<Ifc> ifcList) {
		this.ifcList = ifcList;
	}

}
