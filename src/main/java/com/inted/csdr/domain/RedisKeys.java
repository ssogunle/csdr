package com.inted.csdr.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: RedisKeys
 *
 */
@Entity
public class RedisKeys implements Serializable {

	@Transient
	private static final long serialVersionUID = 1L;

	@Id
	private String Id;

	@ElementCollection
	private List<String> keySet;

	public RedisKeys() {
		super();
	}

	public String getId() {
		return Id;
	}

	public void setId(String id) {
		Id = id;
	}

	public List<String> getKeySet() {

		if (keySet == null)
			keySet = new ArrayList<String>();

		return keySet;
	}

	public void setKeySet(List<String> keySet) {
		this.keySet = keySet;
	}

}
