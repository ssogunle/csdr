package com.inted.csdr.domain;

import java.io.Serializable;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: FeApplication
 *
 */
@Entity
public class FeData implements Serializable {
	@Transient
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer Id;
	private String name;
	private String ipAddress;
	private String odataNamespace;
	private String appViewId;
	//private List<ImsSubscription> imsSubscription;
	
	public FeData() {
		super();
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public Integer getId() {
		return Id;
	}

	public void setId(Integer id) {
		Id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOdataNamespace() {
		return odataNamespace;
	}

	public void setOdataNamespace(String odataNamespace) {
		this.odataNamespace = odataNamespace;
	}

	public String getAppViewId() {
		return appViewId;
	}

	public void setAppViewId(String appViewId) {
		this.appViewId = appViewId;
	}
/*
	public List<ImsSubscription> getImsSubscription() {
		return imsSubscription;
	}

	public void setImsSubscription(List<ImsSubscription> imsSubscription) {
		this.imsSubscription = imsSubscription;
	}
   */
	
}
