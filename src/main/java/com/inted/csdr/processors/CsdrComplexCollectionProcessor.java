package com.inted.csdr.processors;

import java.io.InputStream;
import java.util.List;
import java.util.Locale;


import org.apache.olingo.commons.api.data.ContextURL;
import org.apache.olingo.commons.api.data.Entity;

import org.apache.olingo.commons.api.data.Property;
import org.apache.olingo.commons.api.edm.EdmComplexType;
import org.apache.olingo.commons.api.edm.EdmEntitySet;
import org.apache.olingo.commons.api.edm.EdmProperty;

import org.apache.olingo.commons.api.format.ContentType;
import org.apache.olingo.commons.api.http.HttpHeader;
import org.apache.olingo.commons.api.http.HttpStatusCode;
import org.apache.olingo.server.api.OData;
import org.apache.olingo.server.api.ODataApplicationException;
import org.apache.olingo.server.api.ODataLibraryException;
import org.apache.olingo.server.api.ODataRequest;
import org.apache.olingo.server.api.ODataResponse;
import org.apache.olingo.server.api.ServiceMetadata;
import org.apache.olingo.server.api.processor.ComplexCollectionProcessor;
import org.apache.olingo.server.api.serializer.ComplexSerializerOptions;
import org.apache.olingo.server.api.serializer.ODataSerializer;
import org.apache.olingo.server.api.serializer.SerializerResult;
import org.apache.olingo.server.api.uri.UriInfo;
import org.apache.olingo.server.api.uri.UriParameter;
import org.apache.olingo.server.api.uri.UriResource;
import org.apache.olingo.server.api.uri.UriResourceEntitySet;
import org.apache.olingo.server.api.uri.UriResourceProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.inted.csdr.providers.CsdrDataProducer;

public class CsdrComplexCollectionProcessor implements ComplexCollectionProcessor  {

	private static final Logger LOG = LoggerFactory.getLogger(CsdrComplexCollectionProcessor.class);
	private OData odata;
	private ServiceMetadata serviceMetadata;
	private CsdrDataProducer dp;
	
	
	public CsdrComplexCollectionProcessor(final CsdrDataProducer dp) {
		this.dp = dp;
	}
	
	@Override
	public void init(OData odata, ServiceMetadata serviceMetadata) {
		LOG.info("Complex Collection Processor initialized");
		this.odata = odata;
		this.serviceMetadata = serviceMetadata;
	}

	@Override
	public void readComplexCollection(ODataRequest request, ODataResponse response, UriInfo uriInfo,
			ContentType responseFormat) throws ODataApplicationException, ODataLibraryException {
		
		LOG.info("Read Complex Collection");
		
		//1. Retrieve from URI
		//1.1 Split URI
		List<UriResource> resourceParts = uriInfo.getUriResourceParts();
		
		//1.2 Get EntitySet
		UriResourceEntitySet uriEntityset = (UriResourceEntitySet) resourceParts.get(0);
		EdmEntitySet edmEntitySet = uriEntityset.getEntitySet();
		
		//1.3 Get key for the entity
		List<UriParameter> keyPredicates = uriEntityset.getKeyPredicates();
		
		//1.4  Retrieve the ComplexCollection Property Info
		UriResourceProperty uriProperty = (UriResourceProperty) resourceParts.get(1);
		
		EdmProperty edmProperty =  uriProperty.getProperty();
		String edmPropertyName = edmProperty.getName();
		EdmComplexType edmComplexType = (EdmComplexType) edmProperty.getType();
		
		//2. Retrieve Data from backend
		//2.1 Fetch the specified Entity
		Entity et = dp.readEntityData(edmEntitySet,keyPredicates);
		
		if(et==null)
			throw new ODataApplicationException("Entity not found", HttpStatusCode.NOT_FOUND.getStatusCode(), Locale.ENGLISH);
		
		//2.2. Retrieve the Complex
		//Check if its a collection
		
		Property p=  et.getProperty(edmPropertyName);
		
		// 3. serialize
		if(p.isCollection())
		{
			// 3.1. configure the serializer
			ODataSerializer serializer = odata.createSerializer(responseFormat);

			ContextURL contextUrl = ContextURL.with().entitySet(edmEntitySet).navOrPropertyPath(edmPropertyName).build();
			ComplexSerializerOptions options = ComplexSerializerOptions.with().contextURL(contextUrl).build();
			
			// 3.2. serialize
			SerializerResult serializerResult = serializer.complexCollection(serviceMetadata, edmComplexType, p, options);
			InputStream propertyStream = serializerResult.getContent();

			//4. configure the response object
			response.setContent(propertyStream);
			response.setStatusCode(HttpStatusCode.OK.getStatusCode());
			response.setHeader(HttpHeader.CONTENT_TYPE, responseFormat.toContentTypeString());
		} else {
			// in case there's no value for the property, we can skip the serialization
			response.setStatusCode(HttpStatusCode.NO_CONTENT.getStatusCode());
		}
		
		
	}

	@Override
	public void updateComplexCollection(ODataRequest request, ODataResponse response, UriInfo uriInfo,
			ContentType requestFormat, ContentType responseFormat)
			throws ODataApplicationException, ODataLibraryException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteComplexCollection(ODataRequest request, ODataResponse response, UriInfo uriInfo)
			throws ODataApplicationException, ODataLibraryException {
		// TODO Auto-generated method stub
		
	}

}
