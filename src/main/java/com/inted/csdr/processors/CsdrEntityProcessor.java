package com.inted.csdr.processors;

import java.io.InputStream;
import java.util.List;
import java.util.Locale;



import org.apache.olingo.commons.api.data.ContextURL;
import org.apache.olingo.commons.api.data.ContextURL.Suffix;
import org.apache.olingo.commons.api.data.Entity;

import org.apache.olingo.commons.api.edm.EdmEntitySet;
import org.apache.olingo.commons.api.edm.EdmEntityType;
import org.apache.olingo.commons.api.edm.EdmNavigationProperty;

import org.apache.olingo.commons.api.format.ContentType;
import org.apache.olingo.commons.api.http.HttpHeader;
import org.apache.olingo.commons.api.http.HttpMethod;
import org.apache.olingo.commons.api.http.HttpStatusCode;

import org.apache.olingo.server.api.OData;
import org.apache.olingo.server.api.ODataApplicationException;
import org.apache.olingo.server.api.ODataLibraryException;
import org.apache.olingo.server.api.ODataRequest;
import org.apache.olingo.server.api.ODataResponse;
import org.apache.olingo.server.api.ServiceMetadata;
import org.apache.olingo.server.api.deserializer.DeserializerResult;
import org.apache.olingo.server.api.deserializer.ODataDeserializer;
import org.apache.olingo.server.api.processor.EntityProcessor;
import org.apache.olingo.server.api.serializer.EntitySerializerOptions;
import org.apache.olingo.server.api.serializer.ODataSerializer;
import org.apache.olingo.server.api.serializer.SerializerResult;
import org.apache.olingo.server.api.uri.UriInfo;

import org.apache.olingo.server.api.uri.UriParameter;
import org.apache.olingo.server.api.uri.UriResource;
import org.apache.olingo.server.api.uri.UriResourceEntitySet;
import org.apache.olingo.server.api.uri.UriResourceNavigation;
import org.apache.olingo.server.api.uri.queryoption.ExpandOption;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.inted.csdr.handlers.ExpandQueryHandler;
import com.inted.csdr.providers.CsdrDataProducer;
import com.inted.csdr.util.ODataUtil;

public class CsdrEntityProcessor implements EntityProcessor {

	private static final Logger LOG = LoggerFactory.getLogger(CsdrEntityProcessor.class);

	private OData odata;
	private ServiceMetadata serviceMetadata;
	private CsdrDataProducer dp;

	public CsdrEntityProcessor(CsdrDataProducer dp) {
		LOG.info("Csdr EntityProcessor at work....");
		this.dp = dp;
	}

	@Override
	public void init(OData odata, ServiceMetadata serviceMetadata) {
		this.odata = odata;
		this.serviceMetadata = serviceMetadata;
	}

	@Override
	public void createEntity(ODataRequest request, ODataResponse response, UriInfo uriInfo, ContentType requestFormat,
			ContentType responseFormat) throws ODataApplicationException, ODataLibraryException {
		    
		try {

			String contentType = request.getHeader(HttpHeader.CONTENT_TYPE);

			if (contentType == null) {
				throw new ODataApplicationException("The Content-Type HTTP header is missing.",
						HttpStatusCode.BAD_REQUEST.getStatusCode(), Locale.ROOT);
			}

			// 1. Retrieve the entity type from the URI

			EdmEntitySet edmEntitySet = ODataUtil.getEdmEntitySet(uriInfo.asUriInfoResource());
			EdmEntityType edmEntityType = edmEntitySet.getEntityType();

			// 2. create the data in backend
			// 2.1. retrieve the payload from the POST request for the entity to
			// create and deserialize it
			InputStream requestInputStream = request.getBody();
			ODataDeserializer deserializer = this.odata.createDeserializer(requestFormat);
			DeserializerResult result = deserializer.entity(requestInputStream, edmEntityType);
			Entity requestEntity = result.getEntity();
			
			// 2.2 do the creation in backend, which returns the newly created
			// entity
			Entity createdEntity = dp.createEntityData(edmEntitySet, requestEntity);

			// 3. serialize the response (we have to return the created entity)
			ContextURL contextUrl = ContextURL.with().entitySet(edmEntitySet).build();
			// expand and select currently not supported
			EntitySerializerOptions options = EntitySerializerOptions.with().contextURL(contextUrl).build();

			ODataSerializer serializer = this.odata.createSerializer(responseFormat);
			SerializerResult serializedResponse = serializer.entity(serviceMetadata, edmEntityType, createdEntity,
					options);

			// 4. configure the response object
			response.setContent(serializedResponse.getContent());
			response.setStatusCode(HttpStatusCode.CREATED.getStatusCode());
			response.setHeader(HttpHeader.CONTENT_TYPE, responseFormat.toContentTypeString());

		
		} catch (Exception ex) {
			LOG.error("Error Occured: " + ex);
			ex.printStackTrace();
		}
	}

	@Override
	public void deleteEntity(ODataRequest request, ODataResponse response, UriInfo uriInfo)
			throws ODataApplicationException, ODataLibraryException {
		
		 // 1. Retrieve the entity set which belongs to the requested entity
		  List<UriResource> resourcePaths = uriInfo.getUriResourceParts();
		  // Note: only in our example we can assume that the first segment is the EntitySet
		  UriResourceEntitySet uriResourceEntitySet = (UriResourceEntitySet) resourcePaths.get(0);
		  EdmEntitySet edmEntitySet = uriResourceEntitySet.getEntitySet();

		  // 2. delete the data in backend
		  List<UriParameter> keyPredicates = uriResourceEntitySet.getKeyPredicates();
		  dp.deleteEntityData(edmEntitySet, keyPredicates);

		  //3. configure the response object
		  response.setStatusCode(HttpStatusCode.NO_CONTENT.getStatusCode());

	}

	@Override
	public void readEntity(ODataRequest request, ODataResponse response, UriInfo uriInfo, ContentType responseFormat)
			throws ODataApplicationException, ODataLibraryException {
		LOG.info("Trying to read entity data");
		EdmEntityType responseEdmEntityType = null; // we'll need this to build the ContextURL
	    Entity responseEntity = null; // required for serialization of the response body
	    EdmEntitySet responseEdmEntitySet = null; // we need this for building the contextUrl

	    // 1st step: retrieve the requested Entity: can be "normal" read operation, or navigation (to-one)
	    List<UriResource> resourceParts = uriInfo.getUriResourceParts();
	    int segmentCount = resourceParts.size();

	    UriResource uriResource = resourceParts.get(0); // in our example, the first segment is the EntitySet
	    if (!(uriResource instanceof UriResourceEntitySet)) {
	      throw new ODataApplicationException("Only EntitySet is supported",
	          HttpStatusCode.NOT_IMPLEMENTED.getStatusCode(), Locale.ENGLISH);
	    }

	    UriResourceEntitySet uriResourceEntitySet = (UriResourceEntitySet) uriResource;
	    EdmEntitySet startEdmEntitySet = uriResourceEntitySet.getEntitySet();

	    // Analyze the URI segments
	    if (segmentCount == 1) { // no navigation
	      responseEdmEntityType = startEdmEntitySet.getEntityType();
	      responseEdmEntitySet = startEdmEntitySet; // since we have only one segment

	      // 2. step: retrieve the data from backend
	      List<UriParameter> keyPredicates = uriResourceEntitySet.getKeyPredicates();
	      responseEntity = dp.readEntityData(startEdmEntitySet, keyPredicates);
	      LOG.info("Response Entity is NULL: "+(responseEntity==null));
	    } else if (segmentCount == 2) { // navigation
	      UriResource navSegment = resourceParts.get(1); // in our example we don't support more complex URIs
	      if (navSegment instanceof UriResourceNavigation) {
	        UriResourceNavigation uriResourceNavigation = (UriResourceNavigation) navSegment;
	        EdmNavigationProperty edmNavigationProperty = uriResourceNavigation.getProperty();
	        responseEdmEntityType = edmNavigationProperty.getType();
	        // contextURL displays the last segment
	        responseEdmEntitySet = ODataUtil.getNavigationTargetEntitySet(startEdmEntitySet, edmNavigationProperty);

	        // 2nd: fetch the data from backend.
	        // e.g. for the URI: Products(1)/Category we have to find the correct Category entity
	        List<UriParameter> keyPredicates = uriResourceEntitySet.getKeyPredicates();
	        // e.g. for Products(1)/Category we have to find first the Products(1)
	        Entity sourceEntity = dp.readEntityData(startEdmEntitySet, keyPredicates);

	        LOG.info("Source Entity is NULL: "+(sourceEntity==null));
	        // now we have to check if the navigation is
	        // a) to-one: e.g. Products(1)/Category
	        // b) to-many with key: e.g. Categories(3)/Products(5)
	        // the key for nav is used in this case: Categories(3)/Products(5)
	        List<UriParameter> navKeyPredicates = uriResourceNavigation.getKeyPredicates();

	        if (navKeyPredicates.isEmpty()) { // e.g. DemoService.svc/Products(1)/Category
	        	  LOG.info("Source Entity is NULL[2]: "+(sourceEntity==null));
	          responseEntity = dp.getRelatedEntity(sourceEntity, responseEdmEntityType);
	          LOG.info("Response Entity is NULL[1]: "+(responseEntity==null));
	        } else { // e.g. DemoService.svc/Categories(3)/Products(5)
	        	  LOG.info("Source Entity is NULL[3]: "+(sourceEntity==null));
	          responseEntity = dp.getRelatedEntity(sourceEntity, responseEdmEntityType, navKeyPredicates);
	          LOG.info("Response Entity is NULL[2]: "+(responseEntity==null));
	        }
	      }
	    } else {
	     
	      throw new ODataApplicationException("Not supported", HttpStatusCode.NOT_IMPLEMENTED.getStatusCode(), Locale.ROOT);
	    }

	    if (responseEntity == null) {
	      throw new ODataApplicationException("Nothing found.", HttpStatusCode.NOT_FOUND.getStatusCode(), Locale.ROOT);
	    }

	   ExpandOption expandOption = uriInfo.getExpandOption();
	   
	    //3. Check for ExpandOption
	    responseEntity = ExpandQueryHandler.applyQuery(dp, uriInfo.getExpandOption(), responseEdmEntitySet, responseEntity);
	    
	  
	    // 4. serialize
	    ContextURL contextUrl =
	    		ContextURL.with()
	    		.entitySet(responseEdmEntitySet)
	    		.suffix(Suffix.ENTITY)
	    		.build();
	    
	    EntitySerializerOptions opts = 
	    		EntitySerializerOptions.with()
	    		.contextURL(contextUrl)
				.expand(expandOption)
	    		.build();

	    ODataSerializer serializer = this.odata.createSerializer(responseFormat);
	    SerializerResult serializerResult = serializer.entity(this.serviceMetadata,
	        responseEdmEntityType, responseEntity, opts);

	    // 4. configure the response object
	    response.setContent(serializerResult.getContent());
	    response.setStatusCode(HttpStatusCode.OK.getStatusCode());
	    response.setHeader(HttpHeader.CONTENT_TYPE, responseFormat.toContentTypeString());
	}

	@Override
	public void updateEntity(ODataRequest request, ODataResponse response, UriInfo uriInfo, ContentType requestFormat, ContentType responseFormat)
			throws ODataApplicationException, ODataLibraryException {
		
		 // 1. Retrieve the entity set which belongs to the requested entity
		  List<UriResource> resourcePaths = uriInfo.getUriResourceParts();
		
		  UriResourceEntitySet uriResourceEntitySet = (UriResourceEntitySet) resourcePaths.get(0);
		  EdmEntitySet edmEntitySet = uriResourceEntitySet.getEntitySet();
		  EdmEntityType edmEntityType = edmEntitySet.getEntityType();

		  // 2. update the data in backend
		  // 2.1. retrieve the payload from the PUT request for the entity to be updated
		  InputStream requestInputStream = request.getBody();
		  ODataDeserializer deserializer = this.odata.createDeserializer(requestFormat);
		  DeserializerResult result = deserializer.entity(requestInputStream, edmEntityType);
		  Entity requestEntity = result.getEntity();
		  // 2.2 do the modification in backend
		  List<UriParameter> keyPredicates = uriResourceEntitySet.getKeyPredicates();
		  // Note that this updateEntity()-method is invoked for both PUT or PATCH operations
		  HttpMethod httpMethod = request.getMethod();
		  dp.updateEntityData(edmEntitySet, keyPredicates, requestEntity, httpMethod);

		  //3. configure the response object
		  response.setStatusCode(HttpStatusCode.NO_CONTENT.getStatusCode());
	}

	

}
