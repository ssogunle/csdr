package com.inted.csdr.processors;

import java.io.InputStream;

import java.util.List;

import java.util.Locale;


import org.apache.olingo.commons.api.data.ContextURL;
import org.apache.olingo.commons.api.data.Entity;
import org.apache.olingo.commons.api.data.EntityCollection;
import org.apache.olingo.commons.api.edm.EdmEntitySet;
import org.apache.olingo.commons.api.edm.EdmEntityType;
import org.apache.olingo.commons.api.edm.EdmNavigationProperty;
import org.apache.olingo.commons.api.format.ContentType;
import org.apache.olingo.commons.api.http.HttpHeader;
import org.apache.olingo.commons.api.http.HttpStatusCode;
import org.apache.olingo.server.api.OData;
import org.apache.olingo.server.api.ODataApplicationException;
import org.apache.olingo.server.api.ODataRequest;
import org.apache.olingo.server.api.ODataResponse;
import org.apache.olingo.server.api.ServiceMetadata;
import org.apache.olingo.server.api.processor.EntityCollectionProcessor;
import org.apache.olingo.server.api.serializer.EntityCollectionSerializerOptions;
import org.apache.olingo.server.api.serializer.ODataSerializer;
import org.apache.olingo.server.api.serializer.SerializerException;
import org.apache.olingo.server.api.serializer.SerializerResult;
import org.apache.olingo.server.api.uri.UriInfo;
import org.apache.olingo.server.api.uri.UriParameter;
import org.apache.olingo.server.api.uri.UriResource;
import org.apache.olingo.server.api.uri.UriResourceEntitySet;
import org.apache.olingo.server.api.uri.UriResourceFunction;
import org.apache.olingo.server.api.uri.UriResourceNavigation;
import org.apache.olingo.server.api.uri.queryoption.SelectOption;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.inted.csdr.handlers.CountQueryHandler;
import com.inted.csdr.handlers.ExpandQueryHandler;
import com.inted.csdr.handlers.FilterQueryHandler;
import com.inted.csdr.handlers.OrderByQueryHandler;
import com.inted.csdr.handlers.SearchQueryHandler;
import com.inted.csdr.handlers.SkipQueryHandler;
import com.inted.csdr.handlers.TopQueryHandler;
import com.inted.csdr.providers.CsdrDataProducer;
import com.inted.csdr.util.ODataUtil;

public class CsdrEntityCollectionProcessor implements EntityCollectionProcessor {

	private static final Logger LOG = LoggerFactory.getLogger(CsdrEntityCollectionProcessor.class);

	private OData odata;
	private ServiceMetadata serviceMetadata;
	private CsdrDataProducer dp;

	public CsdrEntityCollectionProcessor(final CsdrDataProducer dp) {
		this.dp = dp;
	}

	@Override
	public void init(OData odata, ServiceMetadata serviceMetadata) {
		LOG.info("Entity Collection Processor initialized");
		this.odata = odata;
		this.serviceMetadata = serviceMetadata;
	}

	public void readEntityCollection(ODataRequest request, ODataResponse response, UriInfo uriInfo,
			ContentType responseFormat) throws ODataApplicationException, SerializerException {

		LOG.info("Read Collection");
		final UriResource firstResourceSegment = uriInfo.getUriResourceParts().get(0);

		if (firstResourceSegment instanceof UriResourceEntitySet) {
			readEntityCollectionInternal(request, response, uriInfo, responseFormat);
		} else if (firstResourceSegment instanceof UriResourceFunction) {
			readFunctionImportCollection(request, response, uriInfo, responseFormat);
		} else {
			throw new ODataApplicationException("Not implemented", HttpStatusCode.NOT_IMPLEMENTED.getStatusCode(),
					Locale.ENGLISH);
		}
	}

	private void readFunctionImportCollection(final ODataRequest request, final ODataResponse response,
			final UriInfo uriInfo, final ContentType responseFormat)
			throws ODataApplicationException, SerializerException {

		// 1st step: Analyze the URI and fetch the entity collection returned by
		// the function import
		// Function Imports are always the first segment of the resource path
		final UriResource firstSegment = uriInfo.getUriResourceParts().get(0);

		if (!(firstSegment instanceof UriResourceFunction)) {
			throw new ODataApplicationException("Not implemented", HttpStatusCode.NOT_IMPLEMENTED.getStatusCode(),
					Locale.ENGLISH);
		}

		final UriResourceFunction uriResourceFunction = (UriResourceFunction) firstSegment;
		final EntityCollection entityCol = dp.readFunctionImportCollection(uriResourceFunction, serviceMetadata);

		// 2nd step: Serialize the response entity
		final EdmEntityType edmEntityType = (EdmEntityType) uriResourceFunction.getFunction().getReturnType().getType();
		final ContextURL contextURL = ContextURL.with().asCollection().type(edmEntityType).build();
		EntityCollectionSerializerOptions opts = EntityCollectionSerializerOptions.with().contextURL(contextURL)
				.build();
		final ODataSerializer serializer = odata.createSerializer(responseFormat);
		final SerializerResult serializerResult = serializer.entityCollection(serviceMetadata, edmEntityType, entityCol,
				opts);

		// 3rd configure the response object: TBC
		response.setContent(serializerResult.getContent());
		response.setStatusCode(HttpStatusCode.OK.getStatusCode());
		response.setHeader(HttpHeader.CONTENT_TYPE, responseFormat.toContentTypeString());
	}

	private void readEntityCollectionInternal(ODataRequest request, ODataResponse response, UriInfo uriInfo,
			ContentType responseFormat) throws ODataApplicationException, SerializerException {
		LOG.info("Read Entity Collection ");
		// Read the collection or process ONE navigation property
		// we'll need this to build the ContextURL
		EdmEntitySet edmEntitySet = null;
		// we'll need this to set the response body
		EntityCollection entityCollection = null;

		// 1st retrieve the requested EntitySet from the uriInfo (representation
		// of the parsed URI)
		List<UriResource> resourceParts = uriInfo.getUriResourceParts();
		int segmentCount = resourceParts.size();

		UriResource uriResource = resourceParts.get(0);

		if (!(uriResource instanceof UriResourceEntitySet))
			throw new ODataApplicationException("Only EntitySet is supported",
					HttpStatusCode.NOT_IMPLEMENTED.getStatusCode(), Locale.ROOT);

		UriResourceEntitySet uriResourceEntitySet = (UriResourceEntitySet) uriResource;
		EdmEntitySet startEdmEntitySet = uriResourceEntitySet.getEntitySet();

		if (segmentCount == 1) {

			edmEntitySet = startEdmEntitySet;

			entityCollection = dp.readEntitySetData(startEdmEntitySet);
		} else if (segmentCount == 2) {
			UriResource lastSegment = resourceParts.get(1);

			if (lastSegment instanceof UriResourceNavigation) {
				UriResourceNavigation uriResourceNavigation = (UriResourceNavigation) lastSegment;
				EdmNavigationProperty edmNavigationProperty = uriResourceNavigation.getProperty();
				EdmEntityType targetEntityType = edmNavigationProperty.getType();
				LOG.info("ECP 1");
				// from Categories(1) to Products
				edmEntitySet = ODataUtil.getNavigationTargetEntitySet(startEdmEntitySet, edmNavigationProperty);
				LOG.info("ECP 2");
				// 2nd: fetch the data from backend
				// first fetch the entity where the first segment of the URI
				// points to
				List<UriParameter> keyPredicates = uriResourceEntitySet.getKeyPredicates();
				// e.g. for Categories(3)/Products we have to find the single
				// entity: Category with ID 3
				Entity sourceEntity = dp.readEntityData(startEdmEntitySet, keyPredicates);
				// error handling for e.g.
				// DemoService.svc/Categories(99)/Products
				if (sourceEntity == null) {
					throw new ODataApplicationException("Entity not found.", HttpStatusCode.NOT_FOUND.getStatusCode(),
							Locale.ROOT);
				}
				// then fetch the entity collection where the entity navigates
				// to
				// note: we don't need to check
				// uriResourceNavigation.isCollection(),
				// because we are the EntityCollectionProcessor
				LOG.info("Source is null: " + (sourceEntity == null));
				entityCollection = dp.getRelatedEntityCollection(sourceEntity, targetEntityType);
			}
		} else
			throw new ODataApplicationException("Not supported", HttpStatusCode.NOT_IMPLEMENTED.getStatusCode(),
					Locale.ROOT);

		List<Entity> responseEntityList = entityCollection.getEntities();
		EntityCollection responseEntityCollection = new EntityCollection();

		// 3rd: Apply system query option
		// The system query options have to be applied in a defined order
		LOG.info("Check Filter Query");
		// 3.1.) $filter
		responseEntityList = FilterQueryHandler.applyQuery(uriInfo.getFilterOption(), responseEntityList);
		LOG.info("Check Search Query");
		// 3.2.) $search
		responseEntityList = SearchQueryHandler.applyQuery(uriInfo.getSearchOption(), responseEntityList);
		LOG.info("Check OrderBy Query");
		// 3.3.) $orderby
		responseEntityList = OrderByQueryHandler.applyQuery(uriInfo.getOrderByOption(), responseEntityList);
		LOG.info("Check Count Query");
		// 3.4.) $count
		responseEntityList = CountQueryHandler.applyQuery(uriInfo.getCountOption(), responseEntityCollection,
				responseEntityList);
		LOG.info("Check Skip Query");
		// 3.5.) $skip
		responseEntityList = SkipQueryHandler.applyQuery(uriInfo.getSkipOption(), responseEntityList);
		LOG.info("Check Top Query");
		// 3.6.) $top
		responseEntityList = TopQueryHandler.applyQuery(uriInfo.getTopOption(), responseEntityList);
		// 3.7.) Server driven paging (not part of this tutorial)
		LOG.info("Check Expand Query");
		// 3.8.) $expand
		//3.8.1) 
		responseEntityList = ExpandQueryHandler.applyQuery(dp,uriInfo.getExpandOption(), edmEntitySet,responseEntityList);
		//3.8.2) Nested system query options are not implemented
		ExpandQueryHandler.validateNestedExpandSystemQueryOptions(uriInfo.getExpandOption());
		// 3.9.) $select
		SelectOption selectOption = uriInfo.getSelectOption();

		// Convert EntityList to EntityCollection
		responseEntityCollection.getEntities().addAll(responseEntityList);

		// 4th: create a serializer based on the requested format (json)
		ODataSerializer serializer = odata.createSerializer(responseFormat);

		// we need the property names of the $select, in order to build the
		// context URL
		EdmEntityType edmEntityType = edmEntitySet.getEntityType();
		String selectList = odata.createUriHelper().buildContextURLSelectList(edmEntityType, uriInfo.getExpandOption(),
				selectOption);
		ContextURL contextUrl = ContextURL.with().entitySet(edmEntitySet).selectList(selectList).build();

		// adding the selectOption to the serializerOpts will actually tell the
		// lib to do the job
		final String id = request.getRawBaseUri() + "/" + edmEntitySet.getName();
		EntityCollectionSerializerOptions opts = EntityCollectionSerializerOptions.with().contextURL(contextUrl)
				.count(uriInfo.getCountOption()).select(selectOption).expand(uriInfo.getExpandOption()).id(id).build();

		// and serialize the content: transform from the EntitySet object to
		// InputStream
		SerializerResult serializerResult = serializer.entityCollection(serviceMetadata, edmEntityType,
				responseEntityCollection, opts);
		InputStream serializedContent = serializerResult.getContent();

		LOG.info("Create Response");
		// 5th: configure the response object: set the body, headers and status
		// code
		response.setContent(serializedContent);
		response.setStatusCode(HttpStatusCode.OK.getStatusCode());
		response.setHeader(HttpHeader.CONTENT_TYPE, responseFormat.toContentTypeString());
	}

}
