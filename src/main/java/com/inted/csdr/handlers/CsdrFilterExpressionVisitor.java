package com.inted.csdr.handlers;

import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import org.apache.olingo.commons.api.data.ComplexValue;
import org.apache.olingo.commons.api.data.Entity;
import org.apache.olingo.commons.api.data.Property;
import org.apache.olingo.commons.api.edm.EdmEnumType;
import org.apache.olingo.commons.api.edm.EdmType;
import org.apache.olingo.commons.api.http.HttpStatusCode;
import org.apache.olingo.commons.core.edm.primitivetype.EdmString;
import org.apache.olingo.server.api.ODataApplicationException;
import org.apache.olingo.server.api.uri.UriResource;
import org.apache.olingo.server.api.uri.UriResourceComplexProperty;

import org.apache.olingo.server.api.uri.UriResourcePrimitiveProperty;

import org.apache.olingo.server.api.uri.queryoption.expression.BinaryOperatorKind;
import org.apache.olingo.server.api.uri.queryoption.expression.Expression;
import org.apache.olingo.server.api.uri.queryoption.expression.ExpressionVisitException;
import org.apache.olingo.server.api.uri.queryoption.expression.ExpressionVisitor;
import org.apache.olingo.server.api.uri.queryoption.expression.Literal;
import org.apache.olingo.server.api.uri.queryoption.expression.Member;
import org.apache.olingo.server.api.uri.queryoption.expression.MethodKind;
import org.apache.olingo.server.api.uri.queryoption.expression.UnaryOperatorKind;


public class CsdrFilterExpressionVisitor implements ExpressionVisitor<Object> {

//	private static final Logger LOG = LoggerFactory.getLogger(CsdrFilterExpressionVisitor.class);
	private Entity currentEntity;

	public CsdrFilterExpressionVisitor(Entity currentEntity) {
		this.currentEntity = currentEntity;
	}

	@Override
	public Object visitMember(Member member) throws ExpressionVisitException, ODataApplicationException {

		final List<UriResource> uriResourceParts = member.getResourcePath().getUriResourceParts();

		// PrimitiveType
		if (uriResourceParts.size() == 1 && uriResourceParts.get(0) instanceof UriResourcePrimitiveProperty) {

			UriResourcePrimitiveProperty uriResourceProperty = (UriResourcePrimitiveProperty) uriResourceParts.get(0);

			return currentEntity.getProperty(uriResourceProperty.getProperty().getName()).getValue();
		}
		// ComplexType/PrimitiveType
		else if (uriResourceParts.size() == 2 && uriResourceParts.get(0) instanceof UriResourceComplexProperty
				&& uriResourceParts.get(1) instanceof UriResourcePrimitiveProperty) {

			UriResourceComplexProperty uriComplexResourceProperty = (UriResourceComplexProperty) uriResourceParts
					.get(0);

			ComplexValue cv = currentEntity.getProperty(uriComplexResourceProperty.getProperty().getName()).asComplex();

			if (cv != null) {
				UriResourcePrimitiveProperty uriPrimitiveResourceProperty = (UriResourcePrimitiveProperty) uriResourceParts
						.get(1);

				String propName = uriPrimitiveResourceProperty.getProperty().getName();

				Iterator<Property> values = cv.getValue().iterator();

				while (values.hasNext()) {
					Property p = values.next();

					if (p.getName().equals(propName)) {
						return p.getValue();
					}
				}
			}

			else {
				throw new ODataApplicationException("Given Complex value not found",
						HttpStatusCode.NOT_FOUND.getStatusCode(), Locale.ENGLISH);
			}

			return currentEntity.getProperty(uriComplexResourceProperty.getProperty().getName()).getValue();
		}
		/* ComplexTypeSet/PrimitiveType: TBC TBC TBC
		else if (uriResourceParts.size() == 2 && uriResourceParts.get(0) instanceof UriResourceProperty
				&& uriResourceParts.get(1) instanceof UriResourcePrimitiveProperty) {

			LOG.info("FE 1");
			UriResourceProperty uriResourceProperty = (UriResourceProperty) uriResourceParts.get(0);

			List<ComplexValue> cvList = (List<ComplexValue>) currentEntity
					.getProperty(uriResourceProperty.getProperty().getName()).asCollection();
			LOG.info("FE 2");
			if (cvList != null) {
				LOG.info("FE 3");
				UriResourcePrimitiveProperty uriPrimitiveResourceProperty = (UriResourcePrimitiveProperty) uriResourceParts
						.get(1);

				String propName = uriPrimitiveResourceProperty.getProperty().getName();

				Iterator<ComplexValue> it = cvList.iterator();

				while (it.hasNext()) {

					ComplexValue cv = it.next();

					Iterator<Property> values = cv.getValue().iterator();

					while (values.hasNext()) {
						Property p = values.next();

						if (p.getName().equals(propName)) {
							return p.getValue();
						}
					}

				}
			} else
				throw new ODataApplicationException("Given Complex value Collection not found",
						HttpStatusCode.NOT_FOUND.getStatusCode(), Locale.ENGLISH);

			LOG.info("FE 4");
			return currentEntity.getProperty(uriResourceProperty.getProperty().getName()).getValue();
		}*/
		// ComplexType/ComplexType/PrimitiveType
		else if (uriResourceParts.size() == 3 && uriResourceParts.get(0) instanceof UriResourceComplexProperty
				&& uriResourceParts.get(1) instanceof UriResourceComplexProperty
				&& uriResourceParts.get(2) instanceof UriResourcePrimitiveProperty) {

			UriResourceComplexProperty uriComplexResourceProperty = (UriResourceComplexProperty) uriResourceParts
					.get(0);

			ComplexValue cv0 = currentEntity.getProperty(uriComplexResourceProperty.getProperty().getName())
					.asComplex();

			if (cv0 != null) {
				UriResourceComplexProperty uriComplexResourceProperty1 = (UriResourceComplexProperty) uriResourceParts
						.get(1);

				String complexTypeName = uriComplexResourceProperty1.getProperty().getName();

				Iterator<Property> it = cv0.getValue().iterator();

				while (it.hasNext()) {

					Property p = it.next();

					if (p.getName().equals(complexTypeName)) {

						ComplexValue cv1 = p.asComplex();

						if (cv1 != null) {

							UriResourcePrimitiveProperty uriPrimitiveResourceProperty = (UriResourcePrimitiveProperty) uriResourceParts
									.get(2);
							String propName = uriPrimitiveResourceProperty.getProperty().getName();

							Iterator<Property> values = cv1.getValue().iterator();

							while (values.hasNext()) {
								Property p1 = values.next();

								if (p1.getName().equals(propName)) {
									return p.getValue();
								}
							}
						} else {
							throw new ODataApplicationException("Second Complex value not found",
									HttpStatusCode.NOT_FOUND.getStatusCode(), Locale.ENGLISH);
						}
					}
				}
			} else {
				throw new ODataApplicationException("First Complex value not found",
						HttpStatusCode.NOT_FOUND.getStatusCode(), Locale.ENGLISH);
			}
			return currentEntity.getProperty(uriComplexResourceProperty.getProperty().getName()).getValue();
		}

		else {
			throw new ODataApplicationException(
					"Only primitive and complex properties of nested level 2 are implemented in filter expressions",
					HttpStatusCode.NOT_IMPLEMENTED.getStatusCode(), Locale.ENGLISH);
		}

	}

	@Override
	public Object visitLiteral(Literal literal) throws ExpressionVisitException, ODataApplicationException {

		// String literals start and end with an single quotation mark
		String literalAsString = literal.getText();
		if (literal.getType() instanceof EdmString) {
			String stringLiteral = "";
			if (literal.getText().length() > 2) {
				stringLiteral = literalAsString.substring(1, literalAsString.length() - 1);
			}

			return stringLiteral;
		} else {
			// Try to convert the literal into an Java Integer
			try {
				return Integer.parseInt(literalAsString);
			} catch (NumberFormatException e) {
				throw new ODataApplicationException("Only Edm.Int32 and Edm.String literals are implemented",
						HttpStatusCode.NOT_IMPLEMENTED.getStatusCode(), Locale.ENGLISH);
			}
		}
	}

	@Override
	public Object visitUnaryOperator(UnaryOperatorKind operator, Object operand)
			throws ExpressionVisitException, ODataApplicationException {
		// OData allows two different unary operators. We have to take care,
		// that the type of the
		// operand fits to the operand

		if (operator == UnaryOperatorKind.NOT && operand instanceof Boolean) {
			// 1.) boolean negation
			return !(Boolean) operand;
		} else if (operator == UnaryOperatorKind.MINUS && operand instanceof Integer) {
			// 2.) arithmetic minus
			return -(Integer) operand;
		}

		// Operation not processed, throw an exception
		throw new ODataApplicationException("Invalid type for unary operator",
				HttpStatusCode.BAD_REQUEST.getStatusCode(), Locale.ENGLISH);
	}

	@Override
	public Object visitBinaryOperator(BinaryOperatorKind operator, Object left, Object right)
			throws ExpressionVisitException, ODataApplicationException {

		// Binary Operators are split up in three different kinds. Up to the
		// kind of the
		// operator it can be applied to different types
		// - Arithmetic operations like add, minus, modulo, etc. are allowed on
		// numeric
		// types like Edm.Int32
		// - Logical operations are allowed on numeric types and also Edm.String
		// - Boolean operations like and, or are allowed on Edm.Boolean
		// A detailed explanation can be found in OData Version 4.0 Part 2: URL
		// Conventions

		if (operator == BinaryOperatorKind.ADD || operator == BinaryOperatorKind.MOD
				|| operator == BinaryOperatorKind.MUL || operator == BinaryOperatorKind.DIV
				|| operator == BinaryOperatorKind.SUB) {
			return evaluateArithmeticOperation(operator, left, right);
		} else if (operator == BinaryOperatorKind.EQ || operator == BinaryOperatorKind.NE
				|| operator == BinaryOperatorKind.GE || operator == BinaryOperatorKind.GT
				|| operator == BinaryOperatorKind.LE || operator == BinaryOperatorKind.LT) {
			return evaluateComparisonOperation(operator, left, right);
		} else if (operator == BinaryOperatorKind.AND || operator == BinaryOperatorKind.OR) {
			return evaluateBooleanOperation(operator, left, right);
		} else {
			throw new ODataApplicationException("Binary operation " + operator.name() + " is not implemented",
					HttpStatusCode.NOT_IMPLEMENTED.getStatusCode(), Locale.ENGLISH);
		}
	}

	private Object evaluateBooleanOperation(BinaryOperatorKind operator, Object left, Object right)
			throws ODataApplicationException {

		// First check that both operands are of type Boolean
		if (left instanceof Boolean && right instanceof Boolean) {
			Boolean valueLeft = (Boolean) left;
			Boolean valueRight = (Boolean) right;

			// Than calculate the result value
			if (operator == BinaryOperatorKind.AND) {
				return valueLeft && valueRight;
			} else {
				// OR
				return valueLeft || valueRight;
			}
		} else {
			throw new ODataApplicationException("Boolean operations needs two numeric operands",
					HttpStatusCode.BAD_REQUEST.getStatusCode(), Locale.ENGLISH);
		}
	}

	private Object evaluateComparisonOperation(BinaryOperatorKind operator, Object left, Object right)
			throws ODataApplicationException {

		// All types in our tutorial supports all logical operations, but we
		// have to make sure that
		// the types are equal
		if (left.getClass().equals(right.getClass())) {
			// Luckily all used types String, Boolean and also Integer support
			// the interface
			// Comparable
			int result;
			if (left instanceof Integer) {
				result = ((Comparable<Integer>) (Integer) left).compareTo((Integer) right);
			} else if (left instanceof String) {
				result = ((Comparable<String>) (String) left).compareTo((String) right);
			} else if (left instanceof Boolean) {
				result = ((Comparable<Boolean>) (Boolean) left).compareTo((Boolean) right);
			} else {
				throw new ODataApplicationException("Class " + left.getClass().getCanonicalName() + " not expected",
						HttpStatusCode.INTERNAL_SERVER_ERROR.getStatusCode(), Locale.ENGLISH);
			}

			if (operator == BinaryOperatorKind.EQ) {
				return result == 0;
			} else if (operator == BinaryOperatorKind.NE) {
				return result != 0;
			} else if (operator == BinaryOperatorKind.GE) {
				return result >= 0;
			} else if (operator == BinaryOperatorKind.GT) {
				return result > 0;
			} else if (operator == BinaryOperatorKind.LE) {
				return result <= 0;
			} else {
				// BinaryOperatorKind.LT
				return result < 0;
			}

		} else {
			throw new ODataApplicationException("Comparison needs two equal types",
					HttpStatusCode.BAD_REQUEST.getStatusCode(), Locale.ENGLISH);
		}
	}

	private Object evaluateArithmeticOperation(BinaryOperatorKind operator, Object left, Object right)
			throws ODataApplicationException {

		// First check if the type of both operands is numerical
		if (left instanceof Integer && right instanceof Integer) {
			Integer valueLeft = (Integer) left;
			Integer valueRight = (Integer) right;

			// Than calculate the result value
			if (operator == BinaryOperatorKind.ADD) {
				return valueLeft + valueRight;
			} else if (operator == BinaryOperatorKind.SUB) {
				return valueLeft - valueRight;
			} else if (operator == BinaryOperatorKind.MUL) {
				return valueLeft * valueRight;
			} else if (operator == BinaryOperatorKind.DIV) {
				return valueLeft / valueRight;
			} else {
				// BinaryOperatorKind,MOD
				return valueLeft % valueRight;
			}
		} else {
			throw new ODataApplicationException("Arithmetic operations needs two numeric operands",
					HttpStatusCode.BAD_REQUEST.getStatusCode(), Locale.ENGLISH);
		}
	}

	@Override
	public Object visitMethodCall(MethodKind methodCall, List<Object> parameters)
			throws ExpressionVisitException, ODataApplicationException {

		// To keep this tutorial small and simple, we implement only one method
		// call
		// contains(String, String) -> Boolean
		if (methodCall == MethodKind.CONTAINS) {
			if (parameters.get(0) instanceof String && parameters.get(1) instanceof String) {
				String valueParam1 = (String) parameters.get(0);
				String valueParam2 = (String) parameters.get(1);

				return valueParam1.contains(valueParam2);
			} else {
				throw new ODataApplicationException("Contains needs two parametes of type Edm.String",
						HttpStatusCode.BAD_REQUEST.getStatusCode(), Locale.ENGLISH);
			}
		} else {
			throw new ODataApplicationException("Method call " + methodCall + " not implemented",
					HttpStatusCode.NOT_IMPLEMENTED.getStatusCode(), Locale.ENGLISH);
		}
	}

	@Override
	public Object visitLambdaExpression(String lambdaFunction, String lambdaVariable, Expression expression)
			throws ExpressionVisitException, ODataApplicationException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object visitAlias(String aliasName) throws ExpressionVisitException, ODataApplicationException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object visitTypeLiteral(EdmType type) throws ExpressionVisitException, ODataApplicationException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object visitLambdaReference(String variableName) throws ExpressionVisitException, ODataApplicationException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object visitEnum(EdmEnumType type, List<String> enumValues)
			throws ExpressionVisitException, ODataApplicationException {
		// TODO Auto-generated method stub
		return null;
	}

}
