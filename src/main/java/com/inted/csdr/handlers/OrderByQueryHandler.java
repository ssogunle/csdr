package com.inted.csdr.handlers;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.olingo.commons.api.data.Entity;
import org.apache.olingo.commons.api.edm.EdmProperty;
import org.apache.olingo.server.api.uri.UriInfoResource;
import org.apache.olingo.server.api.uri.UriResource;
import org.apache.olingo.server.api.uri.UriResourcePrimitiveProperty;
import org.apache.olingo.server.api.uri.queryoption.OrderByItem;
import org.apache.olingo.server.api.uri.queryoption.OrderByOption;
import org.apache.olingo.server.api.uri.queryoption.expression.Expression;
import org.apache.olingo.server.api.uri.queryoption.expression.Member;

public class OrderByQueryHandler {
	
	public static List<Entity> applyQuery(OrderByOption orderByOption, List<Entity> entityList) {

		if (orderByOption != null) {
			List<OrderByItem> orderItemList = orderByOption.getOrders();
			final OrderByItem orderByItem = orderItemList.get(0); // in our
																	// example
																	// we
																	// support
																	// only one
			Expression expression = orderByItem.getExpression();
			if (expression instanceof Member) {
				UriInfoResource resourcePath = ((Member) expression).getResourcePath();
				UriResource uriResource = resourcePath.getUriResourceParts().get(0);
				if (uriResource instanceof UriResourcePrimitiveProperty) {
					EdmProperty edmProperty = ((UriResourcePrimitiveProperty) uriResource).getProperty();
					final String sortPropertyName = edmProperty.getName();

					// do the sorting for the list of entities
					Collections.sort(entityList, new Comparator<Entity>() {

						// we delegate the sorting to the native sorter of
						// Integer and String
						public int compare(Entity entity1, Entity entity2) {
							int compareResult = 0;

							if (sortPropertyName.equals("ID")) {
								Integer integer1 = (Integer) entity1.getProperty(sortPropertyName).getValue();
								Integer integer2 = (Integer) entity2.getProperty(sortPropertyName).getValue();

								compareResult = integer1.compareTo(integer2);
							} else {
								String propertyValue1 = (String) entity1.getProperty(sortPropertyName).getValue();
								String propertyValue2 = (String) entity2.getProperty(sortPropertyName).getValue();

								compareResult = propertyValue1.compareTo(propertyValue2);
							}

							// if 'desc' is specified in the URI, change the
							// order of the list
							if (orderByItem.isDescending()) {
								return -compareResult; // just convert the
														// result to negative
														// value to change the
														// order
							}

							return compareResult;
						}
					});
				}
			}
		}

		return entityList;
	}
	

}
