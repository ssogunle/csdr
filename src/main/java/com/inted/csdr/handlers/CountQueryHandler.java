package com.inted.csdr.handlers;

import java.util.List;

import org.apache.olingo.commons.api.data.Entity;
import org.apache.olingo.commons.api.data.EntityCollection;
import org.apache.olingo.server.api.uri.queryoption.CountOption;

public class CountQueryHandler {

	
	public static List<Entity> applyQuery(CountOption countOption, EntityCollection entityCollection,
			List<Entity> modifiedEntityList) {

		// handle $count: always return the original number of entities, without
		// considering $top and $skip
		if (countOption != null) {
			boolean isCount = countOption.getValue();
			if (isCount) {
				entityCollection.setCount(modifiedEntityList.size());
			}
		}

		return modifiedEntityList;
	}

}
