package com.inted.csdr.handlers;

import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import org.apache.olingo.commons.api.data.Entity;
import org.apache.olingo.commons.api.data.Property;
import org.apache.olingo.server.api.ODataApplicationException;
import org.apache.olingo.server.api.uri.queryoption.SearchOption;
import org.apache.olingo.server.api.uri.queryoption.search.SearchExpression;

public class SearchQueryHandler {

	public static List<Entity> applyQuery(final SearchOption searchOption, final List<Entity> entityList)
			throws ODataApplicationException {

		if (searchOption != null) {

			SearchExpression se = searchOption.getSearchExpression();
			Iterator<Entity> entityIterator = entityList.iterator();

			while (entityIterator.hasNext()) {

				boolean keep = false;
				Entity currentEntity = entityIterator.next();
				ListIterator<Property> properties = currentEntity.getProperties().listIterator();

				while (properties.hasNext() && !keep) {
					keep = SearchExpressionHandler.isTrue(se, properties.next());
				}

				if (!keep)
					entityIterator.remove();
			}
		}

		return entityList;
	}
}
