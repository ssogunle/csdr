package com.inted.csdr.handlers;

import java.util.List;
import java.util.Locale;

import org.apache.olingo.commons.api.Constants;
import org.apache.olingo.commons.api.data.Entity;
import org.apache.olingo.commons.api.data.EntityCollection;
import org.apache.olingo.commons.api.data.Link;
import org.apache.olingo.commons.api.edm.EdmElement;
import org.apache.olingo.commons.api.edm.EdmEntitySet;
import org.apache.olingo.commons.api.edm.EdmEntityType;
import org.apache.olingo.commons.api.edm.EdmNavigationProperty;
import org.apache.olingo.commons.api.edm.EdmNavigationPropertyBinding;
import org.apache.olingo.commons.api.http.HttpStatusCode;
import org.apache.olingo.server.api.ODataApplicationException;
import org.apache.olingo.server.api.uri.UriResource;
import org.apache.olingo.server.api.uri.UriResourceNavigation;
import org.apache.olingo.server.api.uri.queryoption.ExpandItem;
import org.apache.olingo.server.api.uri.queryoption.ExpandOption;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.inted.csdr.providers.CsdrDataProducer;

public class ExpandQueryHandler {
	private static final Logger LOG = LoggerFactory.getLogger(ExpandQueryHandler.class);
	
	public static  List<Entity> applyQuery(CsdrDataProducer dp, ExpandOption expandOption,
			EdmEntitySet edmEntitySet, List<Entity> entityList) {
		 LOG.info("EXPAND");
		 if (expandOption != null) {
			 LOG.info("EXPAND NOT NULL");
			 EdmNavigationProperty edmNavigationProperty = null;
			 ExpandItem expandItem = expandOption.getExpandItems().get(0);
			 
			 if(expandItem.isStar()) {
			        List<EdmNavigationPropertyBinding> bindings = edmEntitySet.getNavigationPropertyBindings();
			        // we know that there are navigation bindings
			        // however normally in this case a check if navigation bindings exists is done
			        if(!bindings.isEmpty()) {
			        	  LOG.info("MEET1");
			          // can in our case only be 'Category' or 'Products', so we can take the first
			          EdmNavigationPropertyBinding binding = bindings.get(0);
			          EdmElement property = edmEntitySet.getEntityType().getProperty(binding.getPath());
			          // we don't need to handle error cases, as it is done in the Olingo library
			          if(property instanceof EdmNavigationProperty) {
			        	  LOG.info("MEET2");
			            edmNavigationProperty = (EdmNavigationProperty) property;
			          }
			        }
			      } else {
			    	  LOG.info("MEET3");
			        // can be 'Category' or 'Products', no path supported
			        UriResource uriResource = expandItem.getResourcePath().getUriResourceParts().get(0);
			        // we don't need to handle error cases, as it is done in the Olingo library
			        if(uriResource instanceof UriResourceNavigation) {
			        	  LOG.info("MEET4");
			          edmNavigationProperty = ((UriResourceNavigation) uriResource).getProperty();
			        }
			      }
			 LOG.info("HERE");
			 if(edmNavigationProperty != null) {
			        String navPropName = edmNavigationProperty.getName();
			        EdmEntityType expandEdmEntityType = edmNavigationProperty.getType();
			        LOG.info("HERE2");
			      
			        for (Entity entity : entityList) {
			        	LOG.info("HERE3i");
			          Link link = new Link();
			          link.setTitle(navPropName);
			          link.setType(Constants.ENTITY_NAVIGATION_LINK_TYPE);
			          link.setRel(Constants.NS_ASSOCIATION_LINK_REL + navPropName);

			          if (edmNavigationProperty.isCollection()) { // in case of Categories/$expand=Products
			        	  LOG.info("HERE4i");
			            // fetch the data for the $expand (to-many navigation) from backend
			            EntityCollection expandEntityCollection = dp.getRelatedEntityCollection(entity, expandEdmEntityType);
			            LOG.info("HERE4ii");
			            link.setInlineEntitySet(expandEntityCollection);
			            LOG.info("HERE4iii");
			           link.setHref(expandEntityCollection.getId().toASCIIString()); 
			            LOG.info("HERE4iv");
			          } else { // in case of Products?$expand=Category
			            // fetch the data for the $expand (to-one navigation) from backend
			            // here we get the data for the expand
			        	  LOG.info("HERE5i");
			            Entity expandEntity = dp.getRelatedEntity(entity, expandEdmEntityType);
			            LOG.info("HERE5ii");
			            link.setInlineEntity(expandEntity);
			            LOG.info("HERE5iii");
			            link.setHref(expandEntity.getId().toASCIIString()); 
			            LOG.info("HERE5iv");
			          }

			          // set the link - containing the expanded data - to the current entity
			          entity.getNavigationLinks().add(link);
			          LOG.info("HERE6i");
			        }
			      }
		 }
		return entityList;
	}
	
	public static Entity applyQuery(CsdrDataProducer dp, ExpandOption expandOption,EdmEntitySet edmEntitySet,Entity entity){
		if(expandOption != null) {
			 LOG.info("E1");
			// retrieve the EdmNavigationProperty from the expand expression
			// Note: in our example, we have only one NavigationProperty, so we can directly access it
			EdmNavigationProperty edmNavigationProperty = null;
			ExpandItem expandItem = expandOption.getExpandItems().get(0);
			if(expandItem.isStar()) {
				 LOG.info("E2");
				List<EdmNavigationPropertyBinding> bindings = edmEntitySet.getNavigationPropertyBindings();
				// we know that there are navigation bindings
				// however normally in this case a check if navigation bindings exists is done
				if(!bindings.isEmpty()) {
					 LOG.info("E3");
					// can in our case only be 'Category' or 'Products', so we can take the first
					EdmNavigationPropertyBinding binding = bindings.get(0);
					EdmElement property = edmEntitySet.getEntityType().getProperty(binding.getPath());
					// we don't need to handle error cases, as it is done in the Olingo library
					if(property instanceof EdmNavigationProperty) {
						 LOG.info("E4");
						edmNavigationProperty = (EdmNavigationProperty) property;
					}
				}
			} else { LOG.info("E5");
				// can be 'Category' or 'Products', no path supported
				UriResource uriResource = expandItem.getResourcePath().getUriResourceParts().get(0);
				// we don't need to handle error cases, as it is done in the Olingo library
				if(uriResource instanceof UriResourceNavigation) {
					 LOG.info("E6");
					edmNavigationProperty = ((UriResourceNavigation) uriResource).getProperty();
				}
			}

			// can be 'Category' or 'Products', no path supported
			// we don't need to handle error cases, as it is done in the Olingo library
			if(edmNavigationProperty != null) {
				 LOG.info("E7");
				EdmEntityType expandEdmEntityType = edmNavigationProperty.getType();
				String navPropName = edmNavigationProperty.getName();

				// build the inline data
				Link link = new Link();
				//LOG.info("NAV PROP NAME: "+navPropName);
				link.setTitle(navPropName);
				link.setType(Constants.ENTITY_NAVIGATION_LINK_TYPE);
				link.setRel(Constants.NS_ASSOCIATION_LINK_REL + navPropName);

				if(edmNavigationProperty.isCollection()){ 
					 LOG.info("E8");
					// in case of Categories(1)/$expand=Products
					// fetch the data for the $expand (to-many navigation) from backend
					// here we get the data for the expand
					EntityCollection expandEntityCollection = dp.getRelatedEntityCollection(entity, expandEdmEntityType);
					
					 LOG.info("E9 EEC Size: "+(expandEntityCollection.getEntities().size()));
					link.setInlineEntitySet(expandEntityCollection);
					 LOG.info("E10");
					link.setHref(expandEntityCollection.getId().toASCIIString()); 
					
				} else {
					 LOG.info("E11");
					// in case of Products(1)?$expand=Category
					// fetch the data for the $expand (to-one navigation) from backend
					// here we get the data for the expand
					Entity expandEntity = dp.getRelatedEntity(entity, expandEdmEntityType);
					 LOG.info("E12");
					link.setInlineEntity(expandEntity);
					 LOG.info("E13");
					 LOG.info("Expand Entity ID: "+expandEntity.getId());
					link.setHref(expandEntity.getId().toASCIIString()); 
				}
				 LOG.info("E14");
				// set the link - containing the expanded data - to the current entity
				entity.getNavigationLinks().add(link);
			}
			
		}
		return entity;
	}
	public static void validateNestedExpandSystemQueryOptions(final ExpandOption expandOption)
			throws ODataApplicationException {
		if (expandOption == null) {
			return;
		}

		for (final ExpandItem item : expandOption.getExpandItems()) {
			if (item.getCountOption() != null || item.getFilterOption() != null || item.getLevelsOption() != null
					|| item.getOrderByOption() != null || item.getSearchOption() != null
					|| item.getSelectOption() != null || item.getSkipOption() != null || item.getTopOption() != null) {

				throw new ODataApplicationException("Nested expand system query options are not implemented",
						HttpStatusCode.NOT_IMPLEMENTED.getStatusCode(), Locale.ENGLISH);
			}
		}
	}
}
