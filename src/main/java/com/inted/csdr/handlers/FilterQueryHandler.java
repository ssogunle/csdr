package com.inted.csdr.handlers;

import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import org.apache.olingo.commons.api.data.Entity;
import org.apache.olingo.commons.api.http.HttpStatusCode;
import org.apache.olingo.server.api.ODataApplicationException;
import org.apache.olingo.server.api.uri.queryoption.FilterOption;
import org.apache.olingo.server.api.uri.queryoption.expression.Expression;
import org.apache.olingo.server.api.uri.queryoption.expression.ExpressionVisitException;

public class FilterQueryHandler {

	public static  List<Entity> applyQuery(final FilterOption filterOption, final List<Entity> entityList)
			throws ODataApplicationException {

		if (filterOption != null) {

			try {
				Iterator<Entity> entityIterator = entityList.iterator();

				while (entityIterator.hasNext()) {

					Entity currentEntity = entityIterator.next();
					Expression filterExpression = filterOption.getExpression();
					CsdrFilterExpressionVisitor expressionVisitor = new CsdrFilterExpressionVisitor(currentEntity);

					// Start evaluating the expression
					Object visitorResult = filterExpression.accept(expressionVisitor);

					// The result of the filter expression must be of type
					// Edm.Boolean
					if (visitorResult instanceof Boolean) {
						if (!Boolean.TRUE.equals(visitorResult)) {
							// The expression evaluated to false (or null), so
							// we have to remove the currentEntity from
							// entityList
							entityIterator.remove();
						}
					} else {
						throw new ODataApplicationException("A filter expression must evaulate to type Edm.Boolean",
								HttpStatusCode.BAD_REQUEST.getStatusCode(), Locale.ENGLISH);
					}
				}

			} catch (ExpressionVisitException e) {
				throw new ODataApplicationException("Exception in filter evaluation",
						HttpStatusCode.INTERNAL_SERVER_ERROR.getStatusCode(), Locale.ENGLISH);
			}

		}
		return entityList;
	}

	
}
