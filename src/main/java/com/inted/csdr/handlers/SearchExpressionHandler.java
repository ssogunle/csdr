package com.inted.csdr.handlers;

import java.util.Calendar;
import java.util.Locale;

import javax.xml.bind.DatatypeConverter;

import org.apache.olingo.commons.api.data.ComplexValue;
import org.apache.olingo.commons.api.data.Property;
import org.apache.olingo.commons.api.http.HttpStatusCode;
import org.apache.olingo.server.api.ODataApplicationException;
import org.apache.olingo.server.api.uri.queryoption.search.SearchBinary;
import org.apache.olingo.server.api.uri.queryoption.search.SearchBinaryOperatorKind;
import org.apache.olingo.server.api.uri.queryoption.search.SearchExpression;
import org.apache.olingo.server.api.uri.queryoption.search.SearchTerm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SearchExpressionHandler {

	private static final Logger LOG = LoggerFactory.getLogger(SearchExpressionHandler.class);
	
	 public static boolean isTrue(final SearchTerm term, final Property property) {
		    if (property.isNull()) {
		    //	LOG.info("SE 1");
		      return false;
		    } else if (property.isPrimitive()) {
		    //	LOG.info("SE 2");
		      if (property.isCollection()) {
		    //		LOG.info("SE 3");
		        for (final Object primitive : property.asCollection()) {
		          final String propertyString = asString(primitive);
		          if (propertyString != null && propertyString.contains(term.getSearchTerm())) { 
		            return true;
		          }
		        }
		    //	LOG.info("SE 4");
		        return false;
		      } else {
		    //		LOG.info("SE 5");
		        final String propertyString = asString(property.asPrimitive());
		   // 	LOG.info("SE 6");
		        return propertyString != null && propertyString.contains(term.getSearchTerm());
		      }
		    } else if (property.isComplex()) {
		    //	LOG.info("SE 7");
		      if (property.isCollection()) {
		    //		LOG.info("SE 8");
		  
		        for (final Object member : property.asCollection()) {
		        	
		        	//New chunk edited -Start: June 28,2016
		        	try{
		        			if (isTrue(term, (Property) member)) 
		        				return true;    
		        	}catch(Exception e){LOG.error("Could no cast a member object to Property");}
		        	
		        	try{
		        		ComplexValue cv = (ComplexValue) member;
		        		for (final Property innerProperty : cv.getValue()) {
		  		          if (isTrue(term, innerProperty)) {
		  		            return true;
		  		          }
		  		        }
		        	}catch(Exception e){LOG.error("Could no cast a member object to ComplexValue");}
		          //End
		      //	LOG.info("SE 9");
		        }
		        return false;
		      } else {
		    //		LOG.info("SE 10");
		        for (final Property innerProperty : property.asComplex().getValue()) {
		          if (isTrue(term, innerProperty)) {
		            return true;
		          }
		        }
		  //  	LOG.info("SE 11"); 
		        return false;
		      }
		    } else {
		  //  	LOG.info("SE 12");
		      return false;
		    }
		  }

		  public static String asString(final Object primitive) {
		    // TODO: improve 'string' conversion; maybe consider only String properties
		    if (primitive instanceof String) {
		      return (String) primitive;
		    } else if (primitive instanceof Calendar) {
		      return DatatypeConverter.printDateTime((Calendar) primitive);
		    } else if (primitive instanceof byte[]) {
		      return DatatypeConverter.printBase64Binary((byte[]) primitive);
		    } else {
		      return primitive.toString();
		    }
		  }

		  public static boolean isTrue(final SearchBinary binary, final Property property) throws ODataApplicationException {
		    SearchExpression left = binary.getLeftOperand();
		    SearchExpression right = binary.getRightOperand();
		    if (binary.getOperator() == SearchBinaryOperatorKind.AND) {
		      return isTrue(left, property) && isTrue(right, property);
		    } else if (binary.getOperator() == SearchBinaryOperatorKind.OR) {
		      return isTrue(left, property) || isTrue(right, property);
		    } else {
		      throw new ODataApplicationException("Found unknown SearchBinaryOperatorKind: " + binary.getOperator(),
		          HttpStatusCode.INTERNAL_SERVER_ERROR.getStatusCode(), Locale.ROOT);
		    }
		  }

		  public static boolean isTrue(final SearchExpression searchExpression, final Property property)
		      throws ODataApplicationException {
		    if (searchExpression.isSearchBinary()) {
		      return isTrue(searchExpression.asSearchBinary(), property);
		    } else if (searchExpression.isSearchTerm()) {
		      return isTrue(searchExpression.asSearchTerm(), property);
		    } else if (searchExpression.isSearchUnary()) {
		      return !isTrue(searchExpression.asSearchUnary().getOperand(), property);
		    }
		    throw new ODataApplicationException("Found unknown SearchExpression: " + searchExpression,
		        HttpStatusCode.INTERNAL_SERVER_ERROR.getStatusCode(), Locale.ROOT);
		  }
}
