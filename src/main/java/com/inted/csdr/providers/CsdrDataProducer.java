package com.inted.csdr.providers;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Locale;

import org.apache.olingo.commons.api.data.Entity;
import org.apache.olingo.commons.api.data.EntityCollection;
import org.apache.olingo.commons.api.data.Property;
import org.apache.olingo.commons.api.edm.EdmEntitySet;
import org.apache.olingo.commons.api.edm.EdmEntityType;
import org.apache.olingo.commons.api.edm.FullQualifiedName;
import org.apache.olingo.commons.api.ex.ODataException;
import org.apache.olingo.commons.api.ex.ODataRuntimeException;
import org.apache.olingo.commons.api.http.HttpMethod;
import org.apache.olingo.commons.api.http.HttpStatusCode;
import org.apache.olingo.server.api.ODataApplicationException;
import org.apache.olingo.server.api.ServiceMetadata;
import org.apache.olingo.server.api.uri.UriParameter;
import org.apache.olingo.server.api.uri.UriResourceFunction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.inted.csdr.domain.ClientSessionData;
import com.inted.csdr.domain.Ifc;
import com.inted.csdr.domain.Impu;
import com.inted.csdr.domain.RegisteredIdentity;
import com.inted.csdr.domain.ServiceProfile;

import com.inted.csdr.domain.SubscriptionProfile;
import com.inted.csdr.providers.domain.AppServerProvider;
import com.inted.csdr.providers.domain.CallDetailRecordProvider;
import com.inted.csdr.providers.domain.ClientSessionDataProvider;
import com.inted.csdr.providers.domain.FeDataProvider;
import com.inted.csdr.providers.domain.IfcProvider;
import com.inted.csdr.providers.domain.ImpiProvider;
import com.inted.csdr.providers.domain.ImpuProvider;
import com.inted.csdr.providers.domain.MergedDataProvider;
import com.inted.csdr.providers.domain.RegisteredIdentityProvider;
import com.inted.csdr.providers.domain.ScscfProvider;
import com.inted.csdr.providers.domain.ServiceProfileProvider;
import com.inted.csdr.providers.domain.SubscriptionProfileProvider;
import com.inted.csdr.providers.domain.TriggerPointProvider;
import com.inted.csdr.providers.domain.VisitedNetworkProvider;
import com.inted.csdr.providers.domain.XcapDocumentProvider;
//import com.inted.csdr.services.domain.UserDocumentService1;
//import com.inted.csdr.services.domain.XcapDocumentService;
import com.inted.csdr.util.ODataUtil;

public class CsdrDataProducer {

	private static final Logger LOG = LoggerFactory.getLogger(CsdrDataProducer.class);

	public CsdrDataProducer() {
		LOG.info("Data Producer initialized ");
	}

	public EntityCollection readFunctionImportCollection(final UriResourceFunction uriResourceFunction,
			final ServiceMetadata serviceMetadata) throws ODataApplicationException {
		
		throw new ODataApplicationException("Function not implemented", HttpStatusCode.NOT_IMPLEMENTED.getStatusCode(),
				Locale.ROOT);
	}

	public Entity createEntityData(EdmEntitySet edmEntitySet, Entity et) throws ODataApplicationException {

		LOG.info("Trying to create entity");

		//if (edmEntitySet.getName().equals(CsdrEdmProvider.ES_APP_SERVER_NAME))
		//	return new AppServerProvider().add(et);

		if (edmEntitySet.getName().equals(CsdrEdmProvider.ES_FE_APP_DATA_NAME))
			return new FeDataProvider().addFeData(et);
		
		if (edmEntitySet.getName().equals(CsdrEdmProvider.ES_CALL_DETAIL_RECORD_NAME))
			return new CallDetailRecordProvider().add(et);
		
		if (edmEntitySet.getName().equals(CsdrEdmProvider.ES_CLIENT_SESSION_DATA_NAME))
			return new ClientSessionDataProvider().add(et);

		if (edmEntitySet.getName().equals(CsdrEdmProvider.ES_VISITED_NETWORK_NAME))
			return new VisitedNetworkProvider().addVisitedNetwork(et);

		if (edmEntitySet.getName().equals(CsdrEdmProvider.ES_XCAP_DOCUMENT_NAME))
			return new XcapDocumentProvider().addXcapDocument(et);
		return null;
	}

	// Get a specific Entity 
	public Entity readEntityData(EdmEntitySet edmEntitySet, List<UriParameter> keyParams)
			throws ODataApplicationException {

		LOG.info("Trying to fetch entity");

		if (edmEntitySet.getName().equals(CsdrEdmProvider.ES_APP_SERVER_NAME))
			return new AppServerProvider().findEAppServer(edmEntitySet, keyParams);

		if (edmEntitySet.getName().equals(CsdrEdmProvider.ES_CALL_DETAIL_RECORD_NAME))
			return new CallDetailRecordProvider().findECallDetailRecord(edmEntitySet, keyParams);
		
		if (edmEntitySet.getName().equals(CsdrEdmProvider.ES_CLIENT_SESSION_DATA_NAME))
			return new ClientSessionDataProvider().findEClientSessionData(edmEntitySet, keyParams);
		
		if (edmEntitySet.getName().equals(CsdrEdmProvider.ES_IFC_NAME))
			return new IfcProvider().findEIfc(edmEntitySet, keyParams);

		if (edmEntitySet.getName().equals(CsdrEdmProvider.ES_SCSCF_NAME))
			return new ScscfProvider().findEScscf(edmEntitySet, keyParams);

		if (edmEntitySet.getName().equals(CsdrEdmProvider.ES_IMS_PRIVATE_IDENTITY_NAME))
			return new ImpiProvider().findEImpi(edmEntitySet, keyParams);

		if (edmEntitySet.getName().equals(CsdrEdmProvider.ES_IMS_PUBLIC_IDENTITY_NAME))
			return new ImpuProvider().findEImpu(edmEntitySet, keyParams);

		if (edmEntitySet.getName().equals(CsdrEdmProvider.ES_MERGED_DATA_NAME))
			return new MergedDataProvider().findEMergedData(edmEntitySet, keyParams);

		if (edmEntitySet.getName().equals(CsdrEdmProvider.ES_REGISTERED_IDENTITY_NAME))
			return new RegisteredIdentityProvider().findERegisteredIdentity(edmEntitySet, keyParams);

		if (edmEntitySet.getName().equals(CsdrEdmProvider.ES_SERVICE_PROFILE_NAME))
			return new ServiceProfileProvider().findEServiceProfile(edmEntitySet, keyParams);

		
		if (edmEntitySet.getName().equals(CsdrEdmProvider.ES_SUBSCRIPTION_PROFILE_NAME))
			return new SubscriptionProfileProvider().findESubscriptionProfile(edmEntitySet, keyParams);


		if (edmEntitySet.getName().equals(CsdrEdmProvider.ES_TRIGGER_POINT_NAME))
			return new TriggerPointProvider().findETriggerPoint(edmEntitySet, keyParams);
		
		if (edmEntitySet.getName().equals(CsdrEdmProvider.ES_XCAP_DOCUMENT_NAME))
			return new TriggerPointProvider().findETriggerPoint(edmEntitySet, keyParams);

		return null;
	}

	public Entity updateEntityData(EdmEntitySet edmEntitySet, List<UriParameter> keyParams, Entity et,
			HttpMethod httpMethod) throws ODataApplicationException {

		return null;
	}

	public EntityCollection readEntitySetData(EdmEntitySet edmEntitySet) throws ODataApplicationException {
		LOG.info("Trying to fetch entity set");

		if (edmEntitySet.getName().equals(CsdrEdmProvider.ES_APP_SERVER_NAME))
			return new AppServerProvider().getEAppServers();

		if (edmEntitySet.getName().equals(CsdrEdmProvider.ES_FE_APP_DATA_NAME))
			return new FeDataProvider().getFeApplicationSet();

		if (edmEntitySet.getName().equals(CsdrEdmProvider.ES_CALL_DETAIL_RECORD_NAME))
			return new CallDetailRecordProvider().getECallDetailRecords();
		
		if (edmEntitySet.getName().equals(CsdrEdmProvider.ES_CLIENT_SESSION_DATA_NAME))
			return new ClientSessionDataProvider().getEClientSessionDataSet();
		
		if (edmEntitySet.getName().equals(CsdrEdmProvider.ES_IFC_NAME))
			return new IfcProvider().getIfcSet(null);

		if (edmEntitySet.getName().equals(CsdrEdmProvider.ES_IMS_PRIVATE_IDENTITY_NAME))
			return new ImpiProvider().getEImpis();

		if (edmEntitySet.getName().equals(CsdrEdmProvider.ES_IMS_PUBLIC_IDENTITY_NAME))
			return new ImpuProvider().getEImpus();

		if (edmEntitySet.getName().equals(CsdrEdmProvider.ES_MERGED_DATA_NAME))
			return new MergedDataProvider().getEMergedDataSet();

		if (edmEntitySet.getName().equals(CsdrEdmProvider.ES_REGISTERED_IDENTITY_NAME))
			return new RegisteredIdentityProvider().getERegisteredIdentities();

		if (edmEntitySet.getName().equals(CsdrEdmProvider.ES_SCSCF_NAME))
			return new ScscfProvider().getEScscfs();

		if (edmEntitySet.getName().equals(CsdrEdmProvider.ES_SERVICE_PROFILE_NAME))
			return new ServiceProfileProvider().getEServiceProfiles();


		if (edmEntitySet.getName().equals(CsdrEdmProvider.ES_SUBSCRIPTION_PROFILE_NAME))
			return new SubscriptionProfileProvider().getESubscriptionProfiles();

		if (edmEntitySet.getName().equals(CsdrEdmProvider.ES_VISITED_NETWORK_NAME))
			return new VisitedNetworkProvider().getVisitedNetworks();

		if (edmEntitySet.getName().equals(CsdrEdmProvider.ES_TRIGGER_POINT_NAME))
			return new TriggerPointProvider().getETriggerPoints();
		
		if (edmEntitySet.getName().equals(CsdrEdmProvider.ES_XCAP_DOCUMENT_NAME))
			return new XcapDocumentProvider().getEXcapDocuments();

		return null;
	}

	public void deleteEntityData(EdmEntitySet edmEntitySet, List<UriParameter> keyParams)
			throws ODataApplicationException {
	
	}

	/*
	 * Navigation Properties
	 */
	public EntityCollection getRelatedEntityCollection(Entity sourceEntity, EdmEntityType targetEntityType) {

		EntityCollection navigationTargetEntityCollection = new EntityCollection();
		FullQualifiedName relatedEntityFqn = targetEntityType.getFullQualifiedName();
		String sourceEntityFqn = sourceEntity.getType();

		LOG.info("Source FQN: " + sourceEntityFqn);
		LOG.info("Related FQN: " + relatedEntityFqn);

		try {
			
			/*
			 * IMS Public Identity -> ClientSessionData (one-to-one, unidirectional),
			 * 
			 */
			if (sourceEntityFqn.equals(CsdrEdmProvider.ET_IMS_PUBLIC_IDENTITY.getFullQualifiedNameAsString())
					&& relatedEntityFqn.equals(CsdrEdmProvider.ET_CLIENT_SESSION_DATA)) {

				navigationTargetEntityCollection
						.setId(createId(sourceEntity, "ID", CsdrEdmProvider.NAV_TO_SESSION_DATA));
				String sipUri = (String) sourceEntity.getProperty("SipUri").getValue();

				List<Impu> impus = new ImpuProvider().getPImpus();
				ClientSessionDataProvider csdp = new ClientSessionDataProvider();
				for (Impu impu : impus) {

					if (impu.getSipUri().equals(sipUri)) {
					
						ClientSessionData csd = csdp.findPClientSessionData(sipUri);
						if(csd!=null){
							Entity et = new ClientSessionDataProvider().getEClientSessionData(csd, 1);
							navigationTargetEntityCollection.getEntities().add(et);
						}
						break;
					}
				}
				csdp.destroy();
			}
			

			/*
			 * RegisteredIdentity -> ClientSessionData (one-to-one, unidirectional),
			 * 
			 */
			if (sourceEntityFqn.equals(CsdrEdmProvider.ET_REGISTERED_IDENTITY.getFullQualifiedNameAsString())
					&& relatedEntityFqn.equals(CsdrEdmProvider.ET_CLIENT_SESSION_DATA)) {

				navigationTargetEntityCollection
						.setId(createId(sourceEntity, "ID", CsdrEdmProvider.NAV_TO_SESSION_DATA));
				
				String impu = (String) sourceEntity.getProperty("Impu").getValue();

				List<RegisteredIdentity> ris = new RegisteredIdentityProvider().getPRegisteredIdentities();
				ClientSessionDataProvider csdp = new ClientSessionDataProvider();
				for (RegisteredIdentity ri : ris) {

					if (ri.getImpu().equals(impu)) {
					
						ClientSessionData csd = csdp.findPClientSessionData(impu);
						if(csd!=null){
							Entity et = new ClientSessionDataProvider().getEClientSessionData(csd, 1);
							navigationTargetEntityCollection.getEntities().add(et);
						}
						break;
					}
				}
				csdp.destroy();
			}
			/*
			 * Subscription Profile > MergedData (one-to-one, unidirectional)
			 */

			if (sourceEntityFqn.equals(CsdrEdmProvider.ET_SUBSCRIPTION_PROFILE.getFullQualifiedNameAsString())
					&& relatedEntityFqn.equals(CsdrEdmProvider.ET_MERGED_DATA)) {

				navigationTargetEntityCollection
						.setId(createId(sourceEntity, "ID", CsdrEdmProvider.NAV_TO_MERGED_DATA));

				String sourceId = (String) sourceEntity.getProperty("Sid").getValue();

				List<SubscriptionProfile> sps = new SubscriptionProfileProvider().getPSubscriptionProfiles();

				for (SubscriptionProfile sp : sps) {

					if (sp.getId().equals(sourceId)) {

						if (sp.getServiceProfile() != null && sp.getServiceProfile().getIfcList() != null) {

							Entity et = new MergedDataProvider().getEMergedData(sp.getServiceProfile().getIfcList(),
									sp.getImpuList(), 1);
							navigationTargetEntityCollection.getEntities().add(et);
						}
						break;
					}
				}
			}
			/*
			 * IFC -> AppServer (one-to-one, unidirectional)
			 */

			if (sourceEntityFqn.equals(CsdrEdmProvider.ET_IFC.getFullQualifiedNameAsString())
					&& relatedEntityFqn.equals(CsdrEdmProvider.ET_APP_SERVER)) {

				navigationTargetEntityCollection.setId(createId(sourceEntity, "ID", CsdrEdmProvider.NAV_TO_APP_SERVER));
				String sourceId = (String) sourceEntity.getProperty("Sid").getValue();

				List<Ifc> ifcs = new IfcProvider().getPIfcSet();

				for (Ifc ifc : ifcs) {

					if (ifc.getId().equals(sourceId)) {

						if (ifc.getAppServer() != null) {
							Entity et = new AppServerProvider().getEAppServer(ifc.getAppServer(), 1);
							navigationTargetEntityCollection.getEntities().add(et);
						}
						break;
					}
				}
			}

			/*
			 * IFC -> TriggerPoint (one-to-one, unidirectional),
			 * 
			 */
			if (sourceEntityFqn.equals(CsdrEdmProvider.ET_IFC.getFullQualifiedNameAsString())
					&& relatedEntityFqn.equals(CsdrEdmProvider.ET_TRIGGER_POINT)) {

				navigationTargetEntityCollection
						.setId(createId(sourceEntity, "ID", CsdrEdmProvider.NAV_TO_TRIGGER_POINT));
				String sourceId = (String) sourceEntity.getProperty("Sid").getValue();

				List<Ifc> ifcs = new IfcProvider().getPIfcSet();

				for (Ifc ifc : ifcs) {

					if (ifc.getId().equals(sourceId)) {

						if (ifc.getAppServer() != null) {
							Entity et = new TriggerPointProvider().getETriggerPoint(ifc.getTriggerPoint(), 1);
							navigationTargetEntityCollection.getEntities().add(et);
						}
						break;
					}
				}
			}

			/*
			 * ServiceProfile -> IFC (one-to-many, unidirectional)
			 */

			if (sourceEntityFqn.equals(CsdrEdmProvider.ET_SERVICE_PROFILE.getFullQualifiedNameAsString())
					&& relatedEntityFqn.equals(CsdrEdmProvider.ET_IFC)) {

				navigationTargetEntityCollection.setId(createId(sourceEntity, "ID", CsdrEdmProvider.NAV_TO_IFC));

				String sourceId = (String) sourceEntity.getProperty("Sid").getValue();

				List<ServiceProfile> serviceProfiles = new ServiceProfileProvider().getPServiceProfiles();

				for (ServiceProfile serviceProfile : serviceProfiles) {

					if (serviceProfile.getId().equals(sourceId)) {

						if (serviceProfile.getIfcList() != null) {
							int i = 1;
							for (Ifc ifc : serviceProfile.getIfcList()) {

								Entity et = new IfcProvider().getEIfc(ifc, i);
								navigationTargetEntityCollection.getEntities().add(et);
								i++;
							}
						}
						break;
					}
				}
			}

			/*
			 * SubscriptionProfile -> ServiceProfile (one-to-one,
			 * unidirectional)
			 */

			if (sourceEntityFqn.equals(CsdrEdmProvider.ET_SUBSCRIPTION_PROFILE.getFullQualifiedNameAsString())
					&& relatedEntityFqn.equals(CsdrEdmProvider.ET_SERVICE_PROFILE)) {

				navigationTargetEntityCollection
						.setId(createId(sourceEntity, "ID", CsdrEdmProvider.NAV_TO_SERVICE_PROFILE));
				String sourceId = (String) sourceEntity.getProperty("Sid").getValue();

				List<SubscriptionProfile> subscriptionProfiles = new SubscriptionProfileProvider()
						.getPSubscriptionProfiles();

				for (SubscriptionProfile subscriptionProfile : subscriptionProfiles) {

					if (subscriptionProfile.getId().equals(sourceId)) {

						if (subscriptionProfile.getServiceProfile() != null) {
							Entity et = new ServiceProfileProvider()
									.getEServiceProfile(subscriptionProfile.getServiceProfile(), 1);
							navigationTargetEntityCollection.getEntities().add(et);
						}
						break;
					}
				}
			}

			/*
			 * SubscriptionProfile -> ImsPrivateIdentity (one-to-many,
			 * unidirectional)
			 */

			if (sourceEntityFqn.equals(CsdrEdmProvider.ET_SUBSCRIPTION_PROFILE.getFullQualifiedNameAsString())
					&& relatedEntityFqn.equals(CsdrEdmProvider.ET_IMS_PRIVATE_IDENTITY)) {

				navigationTargetEntityCollection
				.setId(createId(sourceEntity, "ID", CsdrEdmProvider.NAV_TO_IMS_PRIVATE_IDENTITY));
				
				String sourceId = (String) sourceEntity.getProperty("Sid").getValue();

				List<SubscriptionProfile> subscriptionProfiles = new SubscriptionProfileProvider()
						.getPSubscriptionProfiles();

				for (SubscriptionProfile subscriptionProfile : subscriptionProfiles) {

					if (subscriptionProfile.getId().equals(sourceId)) {

						if (subscriptionProfile.getImpi() != null) {
							Entity et = new ImpiProvider().getEImpi(subscriptionProfile.getImpi(), 1);
							navigationTargetEntityCollection.getEntities().add(et);
						}
						break;
					}
				}
			}
			/*
			 * SubscriptionProfile -> ImsPublicIdentites (one-to-many,
			 * unidirectional)
			 */
			if (sourceEntityFqn.equals(CsdrEdmProvider.ET_SUBSCRIPTION_PROFILE.getFullQualifiedNameAsString())
					&& relatedEntityFqn.equals(CsdrEdmProvider.ET_IMS_PUBLIC_IDENTITY)) {

				navigationTargetEntityCollection
						.setId(createId(sourceEntity, "ID", CsdrEdmProvider.NAV_TO_IMS_PUBLIC_IDENTITY));
				String sourceId = (String) sourceEntity.getProperty("Sid").getValue();

				List<SubscriptionProfile> subscriptionProfiles = new SubscriptionProfileProvider()
						.getPSubscriptionProfiles();

				for (SubscriptionProfile subscriptionProfile : subscriptionProfiles) {

					if (subscriptionProfile.getId().equals(sourceId)) {

						if (subscriptionProfile.getImpuList() != null) {
							ImpuProvider impuWorker = new ImpuProvider();

							int i = 1;
							for (Impu impu : subscriptionProfile.getImpuList()) {
								navigationTargetEntityCollection.getEntities().add(impuWorker.getEImpu(impu, i));
								i++;
							}

						}
						break;
					}
				}
			}

			/*
			 * ImsPublicIdentity -> VisitedNetworks (one-to-many,
			 * unidirectional)
			 */
			/*
			 * if
			 * (sourceEntityFqn.equals(CsdrEdmProvider.ET_IMS_PUBLIC_IDENTITY.
			 * getFullQualifiedNameAsString()) &&
			 * relatedEntityFqn.equals(CsdrEdmProvider.ET_VISITED_NETWORK)) {
			 * 
			 * String sourceId = (String)
			 * sourceEntity.getProperty("Sid").getValue();
			 * 
			 * List<Impu> impus = new ImpuWorker().getPImpus();
			 * 
			 * for (Impu impu : impus) {
			 * 
			 * if (impu.getId().equals(sourceId)) {
			 * 
			 * if (impu.getVisitedNetworkList() != null) { VisitedNetworkWorker
			 * vnWorker = new VisitedNetworkWorker(); int i = 1; for
			 * (VisitedNetwork vn : impu.getVisitedNetworkList()) {
			 * navigationTargetEntityCollection.getEntities().add(vnWorker.
			 * getEVisitedNetwork(vn, i)); i++; } } break; } } }
			 */
		} catch (Exception e) {
			LOG.error("Error with Navigation: " + e);
			e.printStackTrace();
		}

		if (navigationTargetEntityCollection.getEntities().isEmpty()) {
			return null;
		}

		return navigationTargetEntityCollection;
	}

	public Entity getRelatedEntity(Entity entity, EdmEntityType relatedEntityType) {

		LOG.info("Entity is NULL[**]: " + (entity == null));
		LOG.info("Related Entity Type: " + relatedEntityType.getFullQualifiedName());
		EntityCollection collection = getRelatedEntityCollection(entity, relatedEntityType);

		LOG.info("Entity Collection Size is: " + collection.getEntities().size());
		if (collection.getEntities().isEmpty()) {
			return null;
		}
		Entity relEntity =  collection.getEntities().get(0);
		relEntity.setId(createId(relEntity, "ID"));
		return relEntity;
	}

	public Entity getRelatedEntity(Entity entity, EdmEntityType relatedEntityType, List<UriParameter> keyPredicates)
			throws ODataApplicationException {

		LOG.info("Entity Type[2]: " + entity.getType());
		EntityCollection relatedEntities = getRelatedEntityCollection(entity, relatedEntityType);
		Entity relEntity = ODataUtil.findEntity(relatedEntityType, relatedEntities, keyPredicates);
		
		relEntity.setId(createId(relEntity, "ID"));
		return relEntity;
	}

	public static class DataProducerException extends ODataException {
		private static final long serialVersionUID = 5098059649321796156L;

		public DataProducerException(String message, Throwable throwable) {
			super(message, throwable);
		}

		public DataProducerException(String message) {
			super(message);
		}
	}

	private URI createId(Entity entity, String idPropertyName) {
		return createId(entity, idPropertyName, null);
	}

	private URI createId(Entity entity, String idPropertyName, String navigationName) {
		try {
			StringBuilder sb = new StringBuilder(getEntitySetName(entity)).append("(");
			final Property property = entity.getProperty(idPropertyName);
			sb.append(property.asPrimitive()).append(")");
			if (navigationName != null) {
				sb.append("/").append(navigationName);
			}
			return new URI(sb.toString());
		} catch (URISyntaxException e) {
			throw new ODataRuntimeException("Unable to create (Atom) id for entity: " + entity, e);
		}
	}

	private String getEntitySetName(Entity entity) {

		if (CsdrEdmProvider.ET_APP_SERVER.getFullQualifiedNameAsString().equals(entity.getType()))
			return CsdrEdmProvider.ES_APP_SERVER_NAME;

		if (CsdrEdmProvider.ET_CAPABILITY.getFullQualifiedNameAsString().equals(entity.getType()))
			return CsdrEdmProvider.ES_CAPABILITY_NAME;

		if (CsdrEdmProvider.ET_CAPABILITY_SET.getFullQualifiedNameAsString().equals(entity.getType()))
			return CsdrEdmProvider.ES_CAPABILITY_SET_NAME;

		if (CsdrEdmProvider.ET_CLIENT_SESSION_DATA.getFullQualifiedNameAsString().equals(entity.getType()))
			return CsdrEdmProvider.ES_CLIENT_SESSION_DATA_NAME;
		
		if (CsdrEdmProvider.ET_FE_APP_DATA.getFullQualifiedNameAsString().equals(entity.getType()))
			return CsdrEdmProvider.ES_FE_APP_DATA_NAME;

		if (CsdrEdmProvider.ET_IFC.getFullQualifiedNameAsString().equals(entity.getType()))
			return CsdrEdmProvider.ES_IFC_NAME;

		if (CsdrEdmProvider.ET_IMS_PRIVATE_IDENTITY.getFullQualifiedNameAsString().equals(entity.getType()))
			return CsdrEdmProvider.ES_IMS_PRIVATE_IDENTITY_NAME;

		if (CsdrEdmProvider.ET_IMS_PUBLIC_IDENTITY.getFullQualifiedNameAsString().equals(entity.getType()))
			return CsdrEdmProvider.ES_IMS_PUBLIC_IDENTITY_NAME;

		if (CsdrEdmProvider.ET_MERGED_DATA.getFullQualifiedNameAsString().equals(entity.getType()))
			return CsdrEdmProvider.ES_MERGED_DATA_NAME;

		if (CsdrEdmProvider.ET_PREF_SCSCF_SET.getFullQualifiedNameAsString().equals(entity.getType()))
			return CsdrEdmProvider.ES_PREF_SCSCF_SET_NAME;

		if (CsdrEdmProvider.ET_REGISTERED_IDENTITY.getFullQualifiedNameAsString().equals(entity.getType()))
			return CsdrEdmProvider.ES_REGISTERED_IDENTITY_NAME;

		if (CsdrEdmProvider.ET_SCSCF.getFullQualifiedNameAsString().equals(entity.getType()))
			return CsdrEdmProvider.ES_SCSCF_NAME;

		if (CsdrEdmProvider.ET_SERVICE_PROFILE.getFullQualifiedNameAsString().equals(entity.getType()))
			return CsdrEdmProvider.ES_SERVICE_PROFILE_NAME;

		if (CsdrEdmProvider.ET_SUBSCRIPTION_PROFILE.getFullQualifiedNameAsString().equals(entity.getType()))
			return CsdrEdmProvider.ES_SUBSCRIPTION_PROFILE_NAME;

		if (CsdrEdmProvider.ET_TRIGGER_POINT.getFullQualifiedNameAsString().equals(entity.getType()))
			return CsdrEdmProvider.ES_TRIGGER_POINT_NAME;

		if (CsdrEdmProvider.ET_VISITED_NETWORK.getFullQualifiedNameAsString().equals(entity.getType()))
			return CsdrEdmProvider.ES_VISITED_NETWORK_NAME;
		
		if (CsdrEdmProvider.ET_XCAP_DOCUMENT.getFullQualifiedNameAsString().equals(entity.getType()))
			return CsdrEdmProvider.ES_XCAP_DOCUMENT_NAME;

		return entity.getType();
	}
}
