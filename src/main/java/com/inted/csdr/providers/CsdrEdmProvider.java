package com.inted.csdr.providers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.olingo.commons.api.edm.EdmPrimitiveTypeKind;
import org.apache.olingo.commons.api.edm.FullQualifiedName;
import org.apache.olingo.commons.api.edm.provider.CsdlAbstractEdmProvider;
import org.apache.olingo.commons.api.edm.provider.CsdlComplexType;
import org.apache.olingo.commons.api.edm.provider.CsdlEntityContainer;
import org.apache.olingo.commons.api.edm.provider.CsdlEntityContainerInfo;
import org.apache.olingo.commons.api.edm.provider.CsdlEntitySet;
import org.apache.olingo.commons.api.edm.provider.CsdlEntityType;
import org.apache.olingo.commons.api.edm.provider.CsdlEnumMember;
import org.apache.olingo.commons.api.edm.provider.CsdlEnumType;
import org.apache.olingo.commons.api.edm.provider.CsdlNavigationProperty;
import org.apache.olingo.commons.api.edm.provider.CsdlNavigationPropertyBinding;
import org.apache.olingo.commons.api.edm.provider.CsdlProperty;
import org.apache.olingo.commons.api.edm.provider.CsdlPropertyRef;
import org.apache.olingo.commons.api.edm.provider.CsdlSchema;
import org.apache.olingo.commons.api.ex.ODataException;

/**
 * @author Segun Sogunle
 */

public class CsdrEdmProvider extends CsdlAbstractEdmProvider {

	// EDM Namespace
	public static final String CSDR_NAMESPACE = "com.inted.csdr";

	// EDM Container
	public static final String CSDR_CONTAINER_NAME = "CsdrEdmContainer";
	public static final FullQualifiedName CSDR_CONTAINER = new FullQualifiedName(CSDR_NAMESPACE, CSDR_CONTAINER_NAME);

	// A. Enumerated Types

	// Auth Scheme
	public static final String ENUM_AUTH_SCHEME_NAME = "AuthSchemeEnum";
	public static final FullQualifiedName ENUM_AUTH_SCHEME = new FullQualifiedName(CSDR_NAMESPACE,
			ENUM_AUTH_SCHEME_NAME);

	// IMS User State
	public static final String ENUM_IMS_USER_STATE_NAME = "ImsUserState";
	public static final FullQualifiedName ENUM_IMS_USER_STATE = new FullQualifiedName(CSDR_NAMESPACE,
			ENUM_IMS_USER_STATE_NAME);

	// Public Identity Type
	public static final String ENUM_PUBLIC_IDENTITY_TYPE_NAME = "PublicIdentityType";
	public static final FullQualifiedName ENUM_PUBLIC_IDENTITY_TYPE = new FullQualifiedName(CSDR_NAMESPACE,
			ENUM_PUBLIC_IDENTITY_TYPE_NAME);

	// PSI Activation
	public static final String ENUM_PSI_ACTIVATION_NAME = "PsiActivation";
	public static final FullQualifiedName ENUM_PSI_ACTIVATION = new FullQualifiedName(CSDR_NAMESPACE,
			ENUM_PSI_ACTIVATION_NAME);

	// Call Type
	public static final String ENUM_CALL_TYPE_NAME = "CallType";
	public static final FullQualifiedName ENUM_CALL_TYPE = new FullQualifiedName(CSDR_NAMESPACE, ENUM_CALL_TYPE_NAME);

	// DSAI
	public static final String ENUM_DSAI_NAME = "Dsai";
	public static final FullQualifiedName ENUM_DSAI = new FullQualifiedName(CSDR_NAMESPACE, ENUM_DSAI_NAME);

	// Profile Part Indicator
	public static final String ENUM_PPI_NAME = "ProfilePartIndicator";
	public static final FullQualifiedName ENUM_PPI = new FullQualifiedName(CSDR_NAMESPACE, ENUM_PPI_NAME);

	// Session Case
	public static final String ENUM_SESSION_CASE_NAME = "SessionCase";
	public static final FullQualifiedName ENUM_SESSION_CASE = new FullQualifiedName(CSDR_NAMESPACE,
			ENUM_SESSION_CASE_NAME);

	// Service Point Trigger Type
	public static final String ENUM_SPT_TYPE_NAME = "ServicePointTriggerType";
	public static final FullQualifiedName ENUM_SPT_TYPE = new FullQualifiedName(CSDR_NAMESPACE, ENUM_SPT_TYPE_NAME);

	// B. Complex Types
	// Authentication Data
	public static final String CT_AUTH_DATA_NAME = "AuthenticationData";
	public static final FullQualifiedName CT_AUTH_DATA = new FullQualifiedName(CSDR_NAMESPACE, CT_AUTH_DATA_NAME);

	// Authentication Scheme
	public static final String CT_AUTH_SCHEME_NAME = "AuthenticationScheme";
	public static final FullQualifiedName CT_AUTH_SCHEME = new FullQualifiedName(CSDR_NAMESPACE, CT_AUTH_SCHEME_NAME);

	// Charging Information
	public static final String CT_CHARGING_INFORMATION_NAME = "ChargingInfo";
	public static final FullQualifiedName CT_CHARGING_INFORMATION = new FullQualifiedName(CSDR_NAMESPACE,
			CT_CHARGING_INFORMATION_NAME);

	// Charging Information
	public static final String CT_TRIMMED_IFC_NAME = "TrimmedIfc";
	public static final FullQualifiedName CT_TRIMMED_IFC = new FullQualifiedName(CSDR_NAMESPACE, CT_TRIMMED_IFC_NAME);

	// Charging Information
	public static final String CT_TRIMMED_IMPU_NAME = "TrimmedImpu";
	public static final FullQualifiedName CT_TRIMMED_IMPU = new FullQualifiedName(CSDR_NAMESPACE, CT_TRIMMED_IMPU_NAME);

	// Service Point Trigger Instance
	public static final String CT_SPT_NAME = "ServicePointTrigger";
	public static final FullQualifiedName CT_SPT = new FullQualifiedName(CSDR_NAMESPACE, CT_SPT_NAME);

	// Sh-UDR Permissions
	public static final String CT_SH_UDR_PERMISSIONS_NAME = "ShUdrPermissions";
	public static final FullQualifiedName CT_SH_UDR_PERMISSIONS = new FullQualifiedName(CSDR_NAMESPACE,
			CT_SH_UDR_PERMISSIONS_NAME);

	// Sh-Pur Permissions
	public static final String CT_SH_PUR_PERMISSIONS_NAME = "ShPurPermissions";
	public static final FullQualifiedName CT_SH_PUR_PERMISSIONS = new FullQualifiedName(CSDR_NAMESPACE,
			CT_SH_PUR_PERMISSIONS_NAME);

	// C. Entity Types
	// Application Server
	public static final String ET_APP_SERVER_NAME = "ApplicationServer";
	public static final FullQualifiedName ET_APP_SERVER = new FullQualifiedName(CSDR_NAMESPACE, ET_APP_SERVER_NAME);

	// Call Detail Record
	public static final String ET_CALL_DETAIL_RECORD_NAME = "CallDetailRecord";
	public static final FullQualifiedName ET_CALL_DETAIL_RECORD = new FullQualifiedName(CSDR_NAMESPACE,
			ET_CALL_DETAIL_RECORD_NAME);

	// Capability
	public static final String ET_CAPABILITY_NAME = "Capability";
	public static final FullQualifiedName ET_CAPABILITY = new FullQualifiedName(CSDR_NAMESPACE, ET_CAPABILITY_NAME);

	// Capability Set
	public static final String ET_CAPABILITY_SET_NAME = "CapabilitySet";
	public static final FullQualifiedName ET_CAPABILITY_SET = new FullQualifiedName(CSDR_NAMESPACE,
			ET_CAPABILITY_SET_NAME);

	// Client Session Data
	public static final String ET_CLIENT_SESSION_DATA_NAME = "ClientSessionData";
	public static final FullQualifiedName ET_CLIENT_SESSION_DATA = new FullQualifiedName(CSDR_NAMESPACE,
			ET_CLIENT_SESSION_DATA_NAME);

	// Merged Data
	public static final String ET_MERGED_DATA_NAME = "MergedData";
	public static final FullQualifiedName ET_MERGED_DATA = new FullQualifiedName(CSDR_NAMESPACE, ET_MERGED_DATA_NAME);

	// FE Application Data
	public static final String ET_FE_APP_DATA_NAME = "FeApplicationData";
	public static final FullQualifiedName ET_FE_APP_DATA = new FullQualifiedName(CSDR_NAMESPACE, ET_FE_APP_DATA_NAME);

	// Ifc
	public static final String ET_IFC_NAME = "Ifc";
	public static final FullQualifiedName ET_IFC = new FullQualifiedName(CSDR_NAMESPACE, ET_IFC_NAME);

	// IMS Private Identity
	public static final String ET_IMS_PRIVATE_IDENTITY_NAME = "ImsPrivateIdentity";
	public static final FullQualifiedName ET_IMS_PRIVATE_IDENTITY = new FullQualifiedName(CSDR_NAMESPACE,
			ET_IMS_PRIVATE_IDENTITY_NAME);

	// IMS Public Identity
	public static final String ET_IMS_PUBLIC_IDENTITY_NAME = "ImsPublicIdentity";
	public static final FullQualifiedName ET_IMS_PUBLIC_IDENTITY = new FullQualifiedName(CSDR_NAMESPACE,
			ET_IMS_PUBLIC_IDENTITY_NAME);

	// Preferred SCSCF Set
	public static final String ET_PREF_SCSCF_SET_NAME = "PrefScscfSet";
	public static final FullQualifiedName ET_PREF_SCSCF_SET = new FullQualifiedName(CSDR_NAMESPACE,
			ET_PREF_SCSCF_SET_NAME);

	// Registered Identity
	public static final String ET_REGISTERED_IDENTITY_NAME = "RegisteredIdentity";
	public static final FullQualifiedName ET_REGISTERED_IDENTITY = new FullQualifiedName(CSDR_NAMESPACE,
			ET_REGISTERED_IDENTITY_NAME);

	// S-CSCF
	public static final String ET_SCSCF_NAME = "Scscf";
	public static final FullQualifiedName ET_SCSCF = new FullQualifiedName(CSDR_NAMESPACE, ET_SCSCF_NAME);

	// Service Profile
	public static final String ET_SERVICE_PROFILE_NAME = "ServiceProfile";
	public static final FullQualifiedName ET_SERVICE_PROFILE = new FullQualifiedName(CSDR_NAMESPACE,
			ET_SERVICE_PROFILE_NAME);

	// Subscription Profile
	public static final String ET_SUBSCRIPTION_PROFILE_NAME = "SubscriptionProfile";
	public static final FullQualifiedName ET_SUBSCRIPTION_PROFILE = new FullQualifiedName(CSDR_NAMESPACE,
			ET_SUBSCRIPTION_PROFILE_NAME);

	// Trigger Point
	public static final String ET_TRIGGER_POINT_NAME = "TriggerPoint";
	public static final FullQualifiedName ET_TRIGGER_POINT = new FullQualifiedName(CSDR_NAMESPACE,
			ET_TRIGGER_POINT_NAME);

	// Visited Network
	public static final String ET_VISITED_NETWORK_NAME = "VisitedNetwork";
	public static final FullQualifiedName ET_VISITED_NETWORK = new FullQualifiedName(CSDR_NAMESPACE,
			ET_VISITED_NETWORK_NAME);

	// XCAP Document
	public static final String ET_XCAP_DOCUMENT_NAME = "XcapDocument";
	public static final FullQualifiedName ET_XCAP_DOCUMENT = new FullQualifiedName(CSDR_NAMESPACE,
			ET_VISITED_NETWORK_NAME);

	// D. Entity Set Collections
	// Collection of Application Server
	public static final String ES_APP_SERVER_NAME = "ApplicationServers";

	// Collection of Call Detail Records
	public static final String ES_CALL_DETAIL_RECORD_NAME = "CallDetailRecords";

	// Collection of Capability
	public static final String ES_CAPABILITY_NAME = "Capabilities";

	// Collection of Capability Set
	public static final String ES_CAPABILITY_SET_NAME = "CapabilitySets";

	// Collection of Capability Set
	public static final String ES_CLIENT_SESSION_DATA_NAME = "ClientSessionDataSet";

	// Collection of FE Application Data
	public static final String ES_FE_APP_DATA_NAME = "FeApplicationDataSet";

	// Collection of IFCs
	public static final String ES_IFC_NAME = "IfcSet";

	// Collection of IMS Private Identity
	public static final String ES_IMS_PRIVATE_IDENTITY_NAME = "ImsPrivateIdentities";

	// Collection of IMS Public Identity
	public static final String ES_IMS_PUBLIC_IDENTITY_NAME = "ImsPublicIdentities";

	// Collection of MErged Subscription Data
	public static final String ES_MERGED_DATA_NAME = "MergedDataSet";

	// Collection of S-CSCFs
	public static final String ES_SCSCF_NAME = "ScscfSet";

	// Collection of Preferred SCSCF Set
	public static final String ES_PREF_SCSCF_SET_NAME = "PrefScscfSets";

	// Collection of Preferred SCSCF Set
	public static final String ES_REGISTERED_IDENTITY_NAME = "RegisteredIdentities";

	// Collection of Service Profiles
	public static final String ES_SERVICE_PROFILE_NAME = "ServiceProfiles";

	// Collection of IMS Subscriptions
	public static final String ES_SUBSCRIPTION_PROFILE_NAME = "SubscriptionProfiles";

	// Collection of Trigger Points
	public static final String ES_TRIGGER_POINT_NAME = "TriggerPoints";

	// Collection of Visited Networks
	public static final String ES_VISITED_NETWORK_NAME = "VisitedNetworks";

	// Collection of XCAP Documents
	public static final String ES_XCAP_DOCUMENT_NAME = "XcapDocuments";

	// E. Navigation Names
	public static final String NAV_TO_MERGED_DATA = "MergedData";
	public static final String NAV_TO_SESSION_DATA = "SessionData";
	public static final String NAV_TO_APP_SERVER = "ApplicationServer";
	public static final String NAV_TO_TRIGGER_POINT = "TriggerPoint";
	public static final String NAV_TO_IFC = "IfcSet";
	public static final String NAV_TO_SERVICE_PROFILE = "ServiceProfile";
	public static final String NAV_TO_IMS_PRIVATE_IDENTITY = "FullImpi";
	public static final String NAV_TO_IMS_PUBLIC_IDENTITY = "Impus";
	public static final String NAV_TO_VISITED_NETWORK = "VisitedNetwork";

	// E. Get Enumerated Type
	@Override
	public CsdlEnumType getEnumType(FullQualifiedName enumTypeName) throws ODataException {

		CsdlEnumType enumType = null;

		// 1. Auth Scheme
		if (ENUM_AUTH_SCHEME.equals(enumTypeName)) {
			enumType = new CsdlEnumType();
			enumType.setMembers(Arrays.asList(new CsdlEnumMember().setName("AKAv1").setValue("1"),
					new CsdlEnumMember().setName("AKAv2").setValue("2"),
					new CsdlEnumMember().setName("Auth_Scheme_MD5").setValue("4"),
					new CsdlEnumMember().setName("Digest").setValue("8"),
					new CsdlEnumMember().setName("HTTP_Digest_MD5").setValue("16"),
					new CsdlEnumMember().setName("Early").setValue("32"),
					new CsdlEnumMember().setName("NASS_Bundled").setValue("64"),
					new CsdlEnumMember().setName("SIP_Digest").setValue("128")));

			enumType.setName(ENUM_AUTH_SCHEME.getName());
			enumType.setUnderlyingType(EdmPrimitiveTypeKind.Int32.getFullQualifiedName());
		}

		// 1. IMS User State
		if (ENUM_IMS_USER_STATE.equals(enumTypeName)) {

			enumType = new CsdlEnumType();
			enumType.setMembers(Arrays.asList(new CsdlEnumMember().setName("NotRegistered").setValue("0"),
					new CsdlEnumMember().setName("Registered").setValue("1"),
					new CsdlEnumMember().setName("UnRegistered").setValue("2"),
					new CsdlEnumMember().setName("AuthenticationPending").setValue("3")));

			enumType.setName(ENUM_IMS_USER_STATE.getName());
			enumType.setUnderlyingType(EdmPrimitiveTypeKind.Int16.getFullQualifiedName());
		}

		// 2. Public Identity Identity
		if (ENUM_PUBLIC_IDENTITY_TYPE.equals(enumTypeName)) {
			enumType = new CsdlEnumType();

			enumType.setMembers(Arrays.asList(new CsdlEnumMember().setName("PublicUserIdentity").setValue("0"),
					new CsdlEnumMember().setName("DistinctPsi").setValue("1"),
					new CsdlEnumMember().setName("WildcardedPsi").setValue("2")));

			enumType.setName(ENUM_PUBLIC_IDENTITY_TYPE.getName());
			enumType.setUnderlyingType(EdmPrimitiveTypeKind.Int16.getFullQualifiedName());
			enumType.setFlags(true);
		}

		// 3. PSI Activation
		if (ENUM_PSI_ACTIVATION.equals(enumTypeName)) {

			enumType = new CsdlEnumType();
			enumType.setMembers(Arrays.asList(new CsdlEnumMember().setName("Inactive").setValue("0"),
					new CsdlEnumMember().setName("Active").setValue("1")));

			enumType.setName(ENUM_PSI_ACTIVATION.getName());
			enumType.setUnderlyingType(EdmPrimitiveTypeKind.Int16.getFullQualifiedName());
		}

		// 4. Call Type
		if (ENUM_CALL_TYPE.equals(enumTypeName)) {

			enumType = new CsdlEnumType();
			enumType.setMembers(Arrays.asList(new CsdlEnumMember().setName("VoiceCall").setValue("0"),
					new CsdlEnumMember().setName("VideoCall").setValue("1"),
					new CsdlEnumMember().setName("LegacySms").setValue("2"),
					new CsdlEnumMember().setName("InstantMessage").setValue("3"),
					new CsdlEnumMember().setName("FileTransfer").setValue("4"),
					new CsdlEnumMember().setName("ContentShare").setValue("5"),
					new CsdlEnumMember().setName("LocationExchange").setValue("6")));

			enumType.setName(ENUM_CALL_TYPE.getName());
			enumType.setUnderlyingType(EdmPrimitiveTypeKind.Int16.getFullQualifiedName());
		}

		// 4. DSAI
		if (ENUM_DSAI.equals(enumTypeName)) {

			enumType = new CsdlEnumType();
			enumType.setMembers(Arrays.asList(new CsdlEnumMember().setName("Active").setValue("0"),
					new CsdlEnumMember().setName("Inactive").setValue("1")));

			enumType.setName(ENUM_DSAI.getName());
			enumType.setUnderlyingType(EdmPrimitiveTypeKind.Int16.getFullQualifiedName());
		}

		// 5. Profile Part Indicator
		if (ENUM_PPI.equals(enumTypeName)) {

			enumType = new CsdlEnumType();
			enumType.setMembers(Arrays.asList(new CsdlEnumMember().setName("Registered").setValue("0"),
					new CsdlEnumMember().setName("Unregistered").setValue("1"),
					new CsdlEnumMember().setName("Any").setValue("2")));

			enumType.setName(ENUM_PPI.getName());
			enumType.setUnderlyingType(EdmPrimitiveTypeKind.Int16.getFullQualifiedName());
		}

		// 6. Session Case
		if (ENUM_SESSION_CASE.equals(enumTypeName)) {

			enumType = new CsdlEnumType();
			enumType.setMembers(Arrays.asList(new CsdlEnumMember().setName("OriginatingRegistered").setValue("0"),
					new CsdlEnumMember().setName("TerminatingRegistered").setValue("1"),
					new CsdlEnumMember().setName("TerminatingUnregistered").setValue("2"),
					new CsdlEnumMember().setName("OriginatingUnregistered").setValue("3"),
					new CsdlEnumMember().setName("OriginatingCdiv").setValue("4")));

			enumType.setName(ENUM_SESSION_CASE.getName());
			enumType.setUnderlyingType(EdmPrimitiveTypeKind.Int16.getFullQualifiedName());
		}

		// 7. Service Point Trigger Types
		if (ENUM_SPT_TYPE.equals(enumTypeName)) {

			enumType = new CsdlEnumType();
			enumType.setMembers(Arrays.asList(new CsdlEnumMember().setName("SipMethod").setValue("0"),
					new CsdlEnumMember().setName("RequestUri").setValue("1"),
					new CsdlEnumMember().setName("SipHeader").setValue("2"),
					new CsdlEnumMember().setName("SessionDescription").setValue("3"),
					new CsdlEnumMember().setName("SessionCase").setValue("4")));

			enumType.setName(ENUM_SPT_TYPE.getName());
			enumType.setUnderlyingType(EdmPrimitiveTypeKind.Int16.getFullQualifiedName());
		}
		return enumType;
	}

	// F. Get Get Entity Type
	@Override
	public CsdlEntityType getEntityType(FullQualifiedName entityTypeName) throws ODataException {
		CsdlEntityType et = null;

		// 1. Application Server
		if (entityTypeName.equals(ET_APP_SERVER)) {
			// Id
			CsdlProperty id = new CsdlProperty().setName("ID")
					.setType(EdmPrimitiveTypeKind.Int32.getFullQualifiedName());

			// String Id
			CsdlProperty sid = new CsdlProperty().setName("Sid")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// Name
			CsdlProperty name = new CsdlProperty().setName("Name")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// Server Name
			CsdlProperty sipUri = new CsdlProperty().setName("SipUri")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// Diameter FQDN
			CsdlProperty diameterUri = new CsdlProperty().setName("DiameterUri")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// Default Handling
			CsdlProperty dh = new CsdlProperty().setName("DefaultHandling")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// Service Info
			CsdlProperty serviceInfo = new CsdlProperty().setName("ServiceInfo")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// Sh Udr Permissions
			CsdlProperty udrP = new CsdlProperty().setName("ShUdrPermissions").setType(CT_SH_UDR_PERMISSIONS);

			// Sh Pur Permissions
			CsdlProperty purP = new CsdlProperty().setName("ShPurPermissions").setType(CT_SH_PUR_PERMISSIONS);

			// Include Register Request
			CsdlProperty irreq = new CsdlProperty().setName("IncludeRegisterRequest")
					.setType(EdmPrimitiveTypeKind.Boolean.getFullQualifiedName());

			// Include Register Response
			CsdlProperty irres = new CsdlProperty().setName("IncludeRegisterResponse")
					.setType(EdmPrimitiveTypeKind.Boolean.getFullQualifiedName());

			// CsdlPropertyReference for Key
			CsdlPropertyRef propertyRef = new CsdlPropertyRef();
			propertyRef.setName("ID");

			// configuring entity type
			et = new CsdlEntityType();
			et.setName(ET_APP_SERVER.getName());
			et.setProperties(
					Arrays.asList(id, sid, name, sipUri, diameterUri, dh, udrP, purP, serviceInfo, irreq, irres));
			et.setKey(Collections.singletonList(propertyRef));
		}

		// 3. Call Detail Record
		if (entityTypeName.equals(ET_CALL_DETAIL_RECORD)) {
			// Id
			CsdlProperty id = new CsdlProperty().setName("ID")
					.setType(EdmPrimitiveTypeKind.Int32.getFullQualifiedName());

			// SIP Method
			CsdlProperty method = new CsdlProperty().setName("SipMethod")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// From
			CsdlProperty from = new CsdlProperty().setName("From")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// To
			CsdlProperty to = new CsdlProperty().setName("To")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// Duration
			CsdlProperty duration = new CsdlProperty().setName("Duration")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// CallId
			CsdlProperty callId = new CsdlProperty().setName("CallId")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// CSeq Header
			CsdlProperty cSeq = new CsdlProperty().setName("CSeq")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// User Agent Header
			CsdlProperty userAgent = new CsdlProperty().setName("UserAgent")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// Contact Header
			CsdlProperty contact = new CsdlProperty().setName("Contact")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// Content-Length > 0
			CsdlProperty containsSdp = new CsdlProperty().setName("ContainsSdp")
					.setType(EdmPrimitiveTypeKind.Boolean.getFullQualifiedName());

			// Session Expires Header
			CsdlProperty expires = new CsdlProperty().setName("SessionExpires")
					.setType(EdmPrimitiveTypeKind.Int32.getFullQualifiedName());

			// CsdlPropertyReference for Key
			CsdlPropertyRef propertyRef = new CsdlPropertyRef();
			propertyRef.setName("ID");

			// configuring entity type
			et = new CsdlEntityType();
			et.setName(ET_CALL_DETAIL_RECORD.getName());
			et.setProperties(Arrays.asList(id, method, callId, cSeq, to, from, duration, userAgent, contact,
					containsSdp, expires));
			et.setKey(Collections.singletonList(propertyRef));
		}

		// 2. Capability
		if (entityTypeName.equals(ET_CAPABILITY)) {
			// Id
			CsdlProperty id = new CsdlProperty().setName("ID")
					.setType(EdmPrimitiveTypeKind.Int32.getFullQualifiedName());

			// String Id
			CsdlProperty sid = new CsdlProperty().setName("Sid")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// Name
			CsdlProperty name = new CsdlProperty().setName("Name")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// CsdlPropertyReference for Key
			CsdlPropertyRef propertyRef = new CsdlPropertyRef();
			propertyRef.setName("ID");

			// configuring entity type
			et = new CsdlEntityType();
			et.setName(ET_CAPABILITY.getName());
			et.setProperties(Arrays.asList(id, sid, name));
			et.setKey(Collections.singletonList(propertyRef));
		}

		// 3. Capability Set
		if (entityTypeName.equals(ET_CAPABILITY_SET)) {
			// Id
			CsdlProperty id = new CsdlProperty().setName("ID")
					.setType(EdmPrimitiveTypeKind.Int32.getFullQualifiedName());

			// String Id
			CsdlProperty sid = new CsdlProperty().setName("Sid")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// Name
			CsdlProperty name = new CsdlProperty().setName("Name")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// CsdlPropertyReference for Key
			CsdlPropertyRef propertyRef = new CsdlPropertyRef();
			propertyRef.setName("ID");

			// navigation property: one-to-many
			CsdlNavigationProperty navProp = new CsdlNavigationProperty().setName("Capabilities").setType(ET_CAPABILITY)
					.setCollection(true);

			List<CsdlNavigationProperty> navPropList = new ArrayList<CsdlNavigationProperty>();
			navPropList.add(navProp);

			// configuring entity type
			et = new CsdlEntityType();
			et.setName(ET_CAPABILITY_SET.getName());
			et.setProperties(Arrays.asList(id, sid, name));
			et.setKey(Collections.singletonList(propertyRef));
			et.setNavigationProperties(navPropList);
		}

		// 3. Client Session Data
		if (entityTypeName.equals(ET_CLIENT_SESSION_DATA)) {

			// ID
			CsdlProperty Id = new CsdlProperty().setName("ID")
					.setType(EdmPrimitiveTypeKind.Int32.getFullQualifiedName());

			// Subscriber URI
			CsdlProperty subscriberUri = new CsdlProperty().setName("SubscriberUri")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName()).setNullable(false);

			// Last Known Session
			CsdlProperty lastKnownSession = new CsdlProperty().setName("LastKnownSessionId")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName()).setNullable(false);

			// Last Known (Session) State
			CsdlProperty lastKnownState = new CsdlProperty().setName("LastKnownState")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName()).setNullable(false);

			// User Agent
			CsdlProperty userAgent = new CsdlProperty().setName("UserAgent")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// Media Type
			CsdlProperty mediaType = new CsdlProperty().setName("MediaType")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// Media Port
			CsdlProperty mediaPort = new CsdlProperty().setName("MediaPort")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// Media Protocol
			CsdlProperty mediaProtocol = new CsdlProperty().setName("MediaProtocol")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// CsdlPropertyReference for Key
			CsdlPropertyRef propertyRef = new CsdlPropertyRef();
			propertyRef.setName("ID");

			// configuring entity type
			et = new CsdlEntityType();
			et.setName(ET_CLIENT_SESSION_DATA.getName());
			et.setProperties(Arrays.asList(Id, subscriberUri, lastKnownSession, lastKnownState, userAgent, mediaType,
					mediaPort, mediaProtocol));
			et.setKey(Collections.singletonList(propertyRef));
		}

		// 4. FE Application Data
		if (entityTypeName.equals(ET_FE_APP_DATA)) {

			// Id
			CsdlProperty id = new CsdlProperty().setName("ID")
					.setType(EdmPrimitiveTypeKind.Int32.getFullQualifiedName());

			// Name
			CsdlProperty name = new CsdlProperty().setName("Name")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// Name
			CsdlProperty ip = new CsdlProperty().setName("IpAddress")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// OData Namespace
			CsdlProperty on = new CsdlProperty().setName("OdataNamespace")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// Application View Id
			CsdlProperty appView = new CsdlProperty().setName("ApplicationViewIdentifier")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// CsdlPropertyReference for Key
			CsdlPropertyRef propertyRef = new CsdlPropertyRef();
			propertyRef.setName("ID");

			// configuring entity type
			et = new CsdlEntityType();
			et.setName(ET_FE_APP_DATA.getName());
			et.setProperties(Arrays.asList(id, name, ip, on, appView));
			et.setKey(Collections.singletonList(propertyRef));

		}

		// 5. IFC
		if (entityTypeName.equals(ET_IFC)) {

			// Id
			CsdlProperty id = new CsdlProperty().setName("ID")
					.setType(EdmPrimitiveTypeKind.Int32.getFullQualifiedName());

			// String Id
			CsdlProperty sid = new CsdlProperty().setName("Sid")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// Name
			CsdlProperty name = new CsdlProperty().setName("Name")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// Priority
			CsdlProperty priority = new CsdlProperty().setName("Priority")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// Profile Part Indicator
			CsdlProperty ppi = new CsdlProperty().setName("ProfilePartIndicator").setType(ENUM_PPI);

			// CsdlPropertyReference for Key
			CsdlPropertyRef propertyRef = new CsdlPropertyRef();
			propertyRef.setName("ID");

			// OneToOne: IFC > Application Server
			CsdlNavigationProperty navPropAs = new CsdlNavigationProperty().setName("ApplicationServer")
					.setType(ET_APP_SERVER).setNullable(false);

			// OneToOne: IFC > Trigger Point
			CsdlNavigationProperty navPropTp = new CsdlNavigationProperty().setName("TriggerPoint")
					.setType(ET_TRIGGER_POINT).setNullable(false);

			List<CsdlNavigationProperty> navPropList = new ArrayList<CsdlNavigationProperty>();
			navPropList.add(navPropAs);
			navPropList.add(navPropTp);

			// configuring entity type
			et = new CsdlEntityType();
			et.setName(ET_IFC.getName());
			et.setProperties(Arrays.asList(id, sid, name, priority, ppi));
			et.setKey(Collections.singletonList(propertyRef));
			et.setNavigationProperties(navPropList);
		}

		// 6. IMS Private Identity
		if (entityTypeName.equals(ET_IMS_PRIVATE_IDENTITY)) {

			// Id
			CsdlProperty id = new CsdlProperty().setName("ID")
					.setType(EdmPrimitiveTypeKind.Int32.getFullQualifiedName());

			// String Id
			CsdlProperty sid = new CsdlProperty().setName("Sid")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// Uri
			CsdlProperty uri = new CsdlProperty().setName("Uri")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// Authentication Data
			CsdlProperty authData = new CsdlProperty().setName("AuthenticationData").setType(CT_AUTH_DATA);

			// Authentication Scheme
			CsdlProperty authScheme = new CsdlProperty().setName("AuthenticationScheme").setType(CT_AUTH_SCHEME);

			// CsdlPropertyReference for Key
			CsdlPropertyRef propertyRef = new CsdlPropertyRef();
			propertyRef.setName("ID");

			// configuring entity type
			et = new CsdlEntityType();
			et.setName(ET_IMS_PRIVATE_IDENTITY.getName());
			et.setProperties(Arrays.asList(id, sid, uri, authData, authScheme));
			et.setKey(Collections.singletonList(propertyRef));

		}

		// 7. IMS Public Identity
		if (entityTypeName.equals(ET_IMS_PUBLIC_IDENTITY)) {

			// Id
			CsdlProperty id = new CsdlProperty().setName("ID")
					.setType(EdmPrimitiveTypeKind.Int32.getFullQualifiedName());

			// String Id
			CsdlProperty sid = new CsdlProperty().setName("Sid")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// SIP URI
			CsdlProperty sipUri = new CsdlProperty().setName("SipUri")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// Identity Type
			CsdlProperty idType = new CsdlProperty().setName("IdentityType").setType(ENUM_PUBLIC_IDENTITY_TYPE);

			// IMS User State
			CsdlProperty imsUserState = new CsdlProperty().setName("ImsUserState").setType(ENUM_IMS_USER_STATE);

			// Barring
			CsdlProperty barring = new CsdlProperty().setName("BarringIndication")
					.setType(EdmPrimitiveTypeKind.Boolean.getFullQualifiedName());

			// Can Register
			CsdlProperty canRegister = new CsdlProperty().setName("CanRegister")
					.setType(EdmPrimitiveTypeKind.Boolean.getFullQualifiedName());

			// Display Name
			CsdlProperty displayName = new CsdlProperty().setName("DisplayName")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// PSI Activation State
			CsdlProperty psia = new CsdlProperty().setName("PsiActivation").setType(ENUM_PSI_ACTIVATION);

			// navigation property: one-to-many
			CsdlNavigationProperty navProp = new CsdlNavigationProperty().setName("SessionData")
					.setType(ET_CLIENT_SESSION_DATA).setCollection(true);

			List<CsdlNavigationProperty> navPropList = new ArrayList<CsdlNavigationProperty>();
			navPropList.add(navProp);

			// CsdlPropertyReference for Key
			CsdlPropertyRef propertyRef = new CsdlPropertyRef();
			propertyRef.setName("ID");

			// configuring complex type
			et = new CsdlEntityType();
			et.setName(ET_IMS_PUBLIC_IDENTITY.getName());
			et.setProperties(
					Arrays.asList(id, sid, sipUri, idType, imsUserState, barring, canRegister, displayName, psia));
			et.setKey(Collections.singletonList(propertyRef));
			et.setNavigationProperties(navPropList);
		}

		// 8. IMS Private Identity
		if (entityTypeName.equals(ET_MERGED_DATA)) {

			// Id
			CsdlProperty id = new CsdlProperty().setName("ID")
					.setType(EdmPrimitiveTypeKind.Int32.getFullQualifiedName());
			// String Id
			CsdlProperty ifcs = new CsdlProperty().setName("Ifcs").setType(CT_TRIMMED_IFC).setCollection(true);

			CsdlProperty impus = new CsdlProperty().setName("Impus").setType(CT_TRIMMED_IMPU).setCollection(true);

			// CsdlPropertyReference for Key
			CsdlPropertyRef propertyRef = new CsdlPropertyRef();
			propertyRef.setName("ID");
			et = new CsdlEntityType();
			et.setName(ET_MERGED_DATA.getName());
			et.setProperties(Arrays.asList(id, ifcs, impus));
			et.setKey(Collections.singletonList(propertyRef));
		}

		// 8. SCSCF
		if (entityTypeName.equals(ET_SCSCF)) {

			// Id
			CsdlProperty id = new CsdlProperty().setName("ID")
					.setType(EdmPrimitiveTypeKind.Int32.getFullQualifiedName());

			// String Id
			CsdlProperty sid = new CsdlProperty().setName("Sid")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// Name
			CsdlProperty name = new CsdlProperty().setName("Name")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// SIP URI
			CsdlProperty sipUri = new CsdlProperty().setName("SipUri")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// CsdlPropertyReference for Key
			CsdlPropertyRef propertyRef = new CsdlPropertyRef();
			propertyRef.setName("ID");

			// configuring complex type
			et = new CsdlEntityType();
			et.setName(ET_SCSCF.getName());
			et.setKey(Collections.singletonList(propertyRef));
			et.setProperties(Arrays.asList(id, sid, name, sipUri));

		}

		// 9. Preferred SCSCF Set
		if (entityTypeName.equals(ET_PREF_SCSCF_SET)) {

			// Id
			CsdlProperty id = new CsdlProperty().setName("ID")
					.setType(EdmPrimitiveTypeKind.Int32.getFullQualifiedName());

			// String Id
			CsdlProperty sid = new CsdlProperty().setName("Sid")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// Name
			CsdlProperty name = new CsdlProperty().setName("Name")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// CsdlPropertyReference for Key
			CsdlPropertyRef propertyRef = new CsdlPropertyRef();
			propertyRef.setName("ID");

			// OneToMany: Preferred SCSCF Set > SCSCF
			CsdlNavigationProperty navProp = new CsdlNavigationProperty().setName("Scscfs").setType(ET_SCSCF)
					.setCollection(true);

			List<CsdlNavigationProperty> navPropList = new ArrayList<CsdlNavigationProperty>();
			navPropList.add(navProp);

			// configuring entity type
			et = new CsdlEntityType();
			et.setName(ET_PREF_SCSCF_SET.getName());
			et.setProperties(Arrays.asList(id, sid, name));
			et.setKey(Collections.singletonList(propertyRef));
			et.setNavigationProperties(navPropList);

		}

		// 10. Registered Identity
		if (entityTypeName.equals(ET_REGISTERED_IDENTITY)) {

			// Id
			CsdlProperty id = new CsdlProperty().setName("ID")
					.setType(EdmPrimitiveTypeKind.Int32.getFullQualifiedName());

			// IMS Private Identity
			CsdlProperty impi = new CsdlProperty().setName("Impi")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// IMS Public Identity
			CsdlProperty impu = new CsdlProperty().setName("Impu")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());
			// IMS Public Identity
			CsdlProperty authKey = new CsdlProperty().setName("AuthKey")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// IMS Public Identity
			CsdlProperty authOp = new CsdlProperty().setName("AuthOp")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// IMS Public Identity
			CsdlProperty authAmf = new CsdlProperty().setName("AuthAmf")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// IMS Public Identity
			CsdlProperty authSqn = new CsdlProperty().setName("AuthSqn")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// IMS Public Identity
			CsdlProperty authScheme = new CsdlProperty().setName("AuthScheme").setType(ENUM_AUTH_SCHEME);

			// IMS Public Identity
			CsdlProperty authLineId = new CsdlProperty().setName("AuthLineId")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// IMS Public Identity
			CsdlProperty authIpAddress = new CsdlProperty().setName("AuthIpAddress")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// IMS Public Identity
			CsdlProperty barring = new CsdlProperty().setName("Barred")
					.setType(EdmPrimitiveTypeKind.Boolean.getFullQualifiedName());

			// IMS Public Identity
			CsdlProperty canRegister = new CsdlProperty().setName("CanRegister")
					.setType(EdmPrimitiveTypeKind.Boolean.getFullQualifiedName());

			// IMS Public Identity
			CsdlProperty visitedNetwork = new CsdlProperty().setName("VisitedNetwork")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// IMS Public Identity
			CsdlProperty scscf = new CsdlProperty().setName("ScscfId")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// IMS Public Identity
			CsdlProperty impuType = new CsdlProperty().setName("ImpuType").setType(ENUM_PUBLIC_IDENTITY_TYPE);

			// IMS Public Identity
			CsdlProperty psi = new CsdlProperty().setName("PsiActivated").setType(ENUM_PSI_ACTIVATION);

			// IMS Public Identity
			CsdlProperty userState = new CsdlProperty().setName("UserState").setType(ENUM_IMS_USER_STATE);

			// navigation property: one-to-many
			CsdlNavigationProperty navProp = new CsdlNavigationProperty().setName("SessionData")
					.setType(ET_CLIENT_SESSION_DATA).setCollection(true);

			List<CsdlNavigationProperty> navPropList = new ArrayList<CsdlNavigationProperty>();
			navPropList.add(navProp);

			// CsdlPropertyReference for Key
			CsdlPropertyRef propertyRef = new CsdlPropertyRef();
			propertyRef.setName("ID");

			// configuring entity type
			et = new CsdlEntityType();
			et.setName(ET_REGISTERED_IDENTITY.getName());
			et.setProperties(Arrays.asList(id, impi, impu, authKey, authOp, authAmf, authSqn, authScheme, authLineId,
					authIpAddress, barring, canRegister, visitedNetwork, scscf, impuType, psi, userState));
			et.setKey(Collections.singletonList(propertyRef));
			et.setNavigationProperties(navPropList);
		}

		// 12. Service Profile
		if (entityTypeName.equals(ET_SERVICE_PROFILE)) {

			// Id
			CsdlProperty id = new CsdlProperty().setName("ID")
					.setType(EdmPrimitiveTypeKind.Int32.getFullQualifiedName());

			// String Id
			CsdlProperty sid = new CsdlProperty().setName("Sid")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// Name
			CsdlProperty name = new CsdlProperty().setName("Name")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// CsdlPropertyReference for Key
			CsdlPropertyRef propertyRef = new CsdlPropertyRef();
			propertyRef.setName("ID");

			// navigation property: one-to-many
			CsdlNavigationProperty navProp = new CsdlNavigationProperty().setName("IfcSet").setType(ET_IFC)
					.setCollection(true);

			List<CsdlNavigationProperty> navPropList = new ArrayList<CsdlNavigationProperty>();
			navPropList.add(navProp);

			// configuring entity type
			et = new CsdlEntityType();
			et.setName(ET_SERVICE_PROFILE.getName());
			et.setProperties(Arrays.asList(id, sid, name));
			et.setKey(Collections.singletonList(propertyRef));
			et.setNavigationProperties(navPropList);
		}

		// 13. Subscription Profile
		if (entityTypeName.equals(ET_SUBSCRIPTION_PROFILE)) {

			// Id
			CsdlProperty id = new CsdlProperty().setName("ID")
					.setType(EdmPrimitiveTypeKind.Int32.getFullQualifiedName());

			// String Id
			CsdlProperty sid = new CsdlProperty().setName("Sid")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// String IMPI URI
			CsdlProperty impi = new CsdlProperty().setName("Impi")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// Name
			CsdlProperty name = new CsdlProperty().setName("Name")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// Name
			CsdlProperty scscf = new CsdlProperty().setName("ScscfName")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// DSAI
			CsdlProperty dsai = new CsdlProperty().setName("Dsai").setType(ENUM_DSAI);

			// Charging Information
			CsdlProperty chargingInfo = new CsdlProperty().setName("ChargingInfo").setType(CT_CHARGING_INFORMATION);

			// CsdlPropertyReference for Key
			CsdlPropertyRef propertyRef = new CsdlPropertyRef();
			propertyRef.setName("ID");

			// OneToOne: Subscription Profile > Ims Private Identity
			CsdlNavigationProperty impiNavProp = new CsdlNavigationProperty().setName("FullImpi")
					.setType(ET_IMS_PRIVATE_IDENTITY).setNullable(false);

			// OneToOne: Subscription Profile > MergedDataSet
			CsdlNavigationProperty mergedDataNavProp = new CsdlNavigationProperty().setName("MergedData")
					.setType(ET_MERGED_DATA).setNullable(false);

			// OneToMany: Subscription Profile > Ims Public Identity
			CsdlNavigationProperty impuNavProp = new CsdlNavigationProperty().setName("Impus")
					.setType(ET_IMS_PUBLIC_IDENTITY).setCollection(true);

			// OneToOne: Subscription Profile > Service Profile : Potential
			// OneToMany
			CsdlNavigationProperty spNavProp = new CsdlNavigationProperty().setName("ServiceProfile")
					.setType(ET_SERVICE_PROFILE).setNullable(false);

			List<CsdlNavigationProperty> navPropList = new ArrayList<CsdlNavigationProperty>();
			navPropList.add(impiNavProp);
			navPropList.add(impuNavProp);
			navPropList.add(mergedDataNavProp);
			navPropList.add(spNavProp);

			// configuring entity type
			et = new CsdlEntityType();
			et.setName(ET_SUBSCRIPTION_PROFILE.getName());
			et.setProperties(Arrays.asList(id, sid, name, impi, scscf, chargingInfo, dsai));
			et.setKey(Collections.singletonList(propertyRef));
			et.setNavigationProperties(navPropList);
		}

		// 14. Trigger Point
		if (entityTypeName.equals(ET_TRIGGER_POINT)) {

			// Id
			CsdlProperty id = new CsdlProperty().setName("ID")
					.setType(EdmPrimitiveTypeKind.Int32.getFullQualifiedName());

			// String Id
			CsdlProperty sid = new CsdlProperty().setName("Sid")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// Name
			CsdlProperty name = new CsdlProperty().setName("Name")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// Condition Type CNF
			CsdlProperty cnf = new CsdlProperty().setName("ConditionTypeCnf")
					.setType(EdmPrimitiveTypeKind.Boolean.getFullQualifiedName());

			// Condition Type CNF
			CsdlProperty spts = new CsdlProperty().setName("ServicePointTriggers").setType(CT_SPT).setCollection(true);

			// CsdlPropertyReference for Key
			CsdlPropertyRef propertyRef = new CsdlPropertyRef();
			propertyRef.setName("ID");

			// configuring entity type
			et = new CsdlEntityType();
			et.setName(ET_TRIGGER_POINT.getName());
			et.setProperties(Arrays.asList(id, sid, name, cnf, spts));
			et.setKey(Collections.singletonList(propertyRef));
		}

		// 15. Visited Network
		if (entityTypeName.equals(ET_VISITED_NETWORK)) {

			// Id
			CsdlProperty id = new CsdlProperty().setName("ID")
					.setType(EdmPrimitiveTypeKind.Int32.getFullQualifiedName());

			// String Id
			// CsdlProperty sid = new CsdlProperty().setName("Sid")
			// .setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());
			// Name
			CsdlProperty name = new CsdlProperty().setName("Name")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// CsdlPropertyReference for Key
			CsdlPropertyRef propertyRef = new CsdlPropertyRef();
			propertyRef.setName("ID");

			// configuring entity type
			et = new CsdlEntityType();
			et.setName(ET_VISITED_NETWORK.getName());
			et.setProperties(Arrays.asList(id, name));
			et.setKey(Collections.singletonList(propertyRef));
		}

		// 16. XCAP Document
		if (entityTypeName.equals(ET_XCAP_DOCUMENT)) {

			// Id
			CsdlProperty id = new CsdlProperty().setName("ID")
					.setType(EdmPrimitiveTypeKind.Int32.getFullQualifiedName());

			// String Id
			CsdlProperty sid = new CsdlProperty().setName("Sid")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// AppUsage
			CsdlProperty appUsage = new CsdlProperty().setName("AppUsage")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// Document Name
			CsdlProperty documentName = new CsdlProperty().setName("DocumentName")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// XUI
			CsdlProperty xui = new CsdlProperty().setName("Xui")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// XML Data
			CsdlProperty data = new CsdlProperty().setName("Data")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// e-tag
			CsdlProperty eTag = new CsdlProperty().setName("eTag")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// Event update trigger
			CsdlProperty isUpdated = new CsdlProperty().setName("isUpdated")
					.setType(EdmPrimitiveTypeKind.Boolean.getFullQualifiedName());

			// CsdlPropertyReference for Key
			CsdlPropertyRef propertyRef = new CsdlPropertyRef();
			propertyRef.setName("ID");

			// configuring entity type
			et = new CsdlEntityType();
			et.setName(ET_XCAP_DOCUMENT.getName());
			et.setProperties(Arrays.asList(id, sid, appUsage, documentName, xui, data, eTag, isUpdated));
			et.setKey(Collections.singletonList(propertyRef));
		}
		return et;
	}

	// G. Get Complex Type
	@Override
	public CsdlComplexType getComplexType(FullQualifiedName complexTypeName) throws ODataException {
		CsdlComplexType ct = null;

		// 1. Authentication Data
		if (complexTypeName.equals(CT_AUTH_DATA)) {

			// Secret Key : k
			CsdlProperty k = new CsdlProperty().setName("SecretKey")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// Quality of Protection ?
			CsdlProperty op = new CsdlProperty().setName("Op")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			CsdlProperty amf = new CsdlProperty().setName("Amf")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			CsdlProperty sqn = new CsdlProperty().setName("Sqn")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			CsdlProperty ipAddress = new CsdlProperty().setName("IpAddress")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// Line Identifier
			CsdlProperty lineIdentifier = new CsdlProperty().setName("LineIdentifier")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// configuring complex type
			ct = new CsdlComplexType();
			ct.setName(CT_AUTH_DATA.getName());
			ct.setProperties(Arrays.asList(k, op, amf, sqn, ipAddress, lineIdentifier));
		}

		// 2. Authentication Scheme
		if (complexTypeName.equals(CT_AUTH_SCHEME)) {

			CsdlProperty supportedAuthSchemes = new CsdlProperty().setName("SupportedAuthSchemes")
					.setType(ENUM_AUTH_SCHEME).setCollection(true);

			CsdlProperty defaultAuthScheme = new CsdlProperty().setName("DefaultAuthScheme").setType(ENUM_AUTH_SCHEME);

			// configuring complex type
			ct = new CsdlComplexType();
			ct.setName(CT_AUTH_SCHEME.getName());
			ct.setProperties(Arrays.asList(supportedAuthSchemes, defaultAuthScheme));
		}

		// 3. Charging Info
		if (complexTypeName.equals(CT_CHARGING_INFORMATION)) {

			CsdlProperty primaryCcf = new CsdlProperty().setName("PrimaryCcf")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			CsdlProperty secondaryCcf = new CsdlProperty().setName("SecondaryCcf")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			CsdlProperty primaryEcf = new CsdlProperty().setName("PrimaryEcf")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			CsdlProperty secondaryEcf = new CsdlProperty().setName("SecondaryEcf")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// configuring complex type
			ct = new CsdlComplexType();
			ct.setName(CT_CHARGING_INFORMATION.getName());
			ct.setProperties(Arrays.asList(primaryCcf, secondaryCcf, primaryEcf, secondaryEcf));
		}

		// 11. Service Point Trigger Instance
		if (complexTypeName.equals(CT_SPT)) {

			// Group ID
			CsdlProperty gid = new CsdlProperty().setName("GroupId")
					.setType(EdmPrimitiveTypeKind.Int32.getFullQualifiedName());

			// Condition Negated
			CsdlProperty cn = new CsdlProperty().setName("ConditionNegated")
					.setType(EdmPrimitiveTypeKind.Boolean.getFullQualifiedName());

			// SPT Type
			CsdlProperty sptType = new CsdlProperty().setName("Type").setType(ENUM_SPT_TYPE);

			// SPT Data
			CsdlProperty sptData = new CsdlProperty().setName("Data")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// configuring complex type
			ct = new CsdlComplexType();
			ct.setName(CT_SPT.getName());
			ct.setProperties(Arrays.asList(gid, cn, sptType, sptData));

		}

		// 3. Charging Info
		if (complexTypeName.equals(CT_TRIMMED_IFC)) {

			CsdlProperty priority = new CsdlProperty().setName("Priority")
					.setType(EdmPrimitiveTypeKind.Int32.getFullQualifiedName());

			CsdlProperty tp = new CsdlProperty().setName("TriggerPointCnf")
					.setType(EdmPrimitiveTypeKind.Boolean.getFullQualifiedName());

			CsdlProperty spts = new CsdlProperty().setName("ServicePointTriggers").setType(CT_SPT).setCollection(true);

			CsdlProperty appServerUri = new CsdlProperty().setName("ApplicationServerUri")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			// configuring complex type
			ct = new CsdlComplexType();
			ct.setName(CT_TRIMMED_IFC.getName());
			ct.setProperties(Arrays.asList(priority, tp, spts, appServerUri));
		}

		// 3. Charging Info
		if (complexTypeName.equals(CT_TRIMMED_IMPU)) {

			CsdlProperty sipUri = new CsdlProperty().setName("SipUri")
					.setType(EdmPrimitiveTypeKind.String.getFullQualifiedName());

			CsdlProperty barred = new CsdlProperty().setName("Barred")
					.setType(EdmPrimitiveTypeKind.Boolean.getFullQualifiedName());

			// configuring complex type
			ct = new CsdlComplexType();
			ct.setName(CT_TRIMMED_IMPU.getName());
			ct.setProperties(Arrays.asList(sipUri, barred));
		}

		// 4. Sh-UDR Permission
		if (complexTypeName.equals(CT_SH_UDR_PERMISSIONS)) {

			// It's Allowed by HSS
			CsdlProperty isAllowed = new CsdlProperty().setName("IsAllowed")
					.setType(EdmPrimitiveTypeKind.Boolean.getFullQualifiedName());

			// IMS User State
			CsdlProperty imsUserState = new CsdlProperty().setName("ImsUserState")
					.setType(EdmPrimitiveTypeKind.Boolean.getFullQualifiedName());

			// IMPU Identifier
			CsdlProperty impuIdentifier = new CsdlProperty().setName("ImpuIdentifier")
					.setType(EdmPrimitiveTypeKind.Boolean.getFullQualifiedName());

			// IFC
			CsdlProperty ifc = new CsdlProperty().setName("Ifc")
					.setType(EdmPrimitiveTypeKind.Boolean.getFullQualifiedName());

			// Charging Info
			CsdlProperty chargingInfo = new CsdlProperty().setName("ChargingInfo")
					.setType(EdmPrimitiveTypeKind.Boolean.getFullQualifiedName());

			// PSI Activation Info
			CsdlProperty psiActivation = new CsdlProperty().setName("PsiActivation")
					.setType(EdmPrimitiveTypeKind.Boolean.getFullQualifiedName());

			// DSAI
			CsdlProperty dsai = new CsdlProperty().setName("Dsai")
					.setType(EdmPrimitiveTypeKind.Boolean.getFullQualifiedName());

			// Aliases Repository Data
			CsdlProperty aliasesRepoData = new CsdlProperty().setName("AliasesRepoData")
					.setType(EdmPrimitiveTypeKind.Boolean.getFullQualifiedName());

			// configuring complex type
			ct = new CsdlComplexType();
			ct.setName(CT_SH_UDR_PERMISSIONS.getName());
			ct.setProperties(Arrays.asList(isAllowed, imsUserState, impuIdentifier, ifc, chargingInfo, psiActivation,
					dsai, aliasesRepoData));

		}

		// 5. Sh-PUR Permission
		if (complexTypeName.equals(CT_SH_PUR_PERMISSIONS)) {

			// It's Allowed by HSS
			CsdlProperty isAllowed = new CsdlProperty().setName("IsAllowed")
					.setType(EdmPrimitiveTypeKind.Boolean.getFullQualifiedName());

			// PSI Activation Info
			CsdlProperty psiActivation = new CsdlProperty().setName("PsiActivation")
					.setType(EdmPrimitiveTypeKind.Boolean.getFullQualifiedName());

			// DSAI
			CsdlProperty dsai = new CsdlProperty().setName("Dsai")
					.setType(EdmPrimitiveTypeKind.Boolean.getFullQualifiedName());

			// configuring complex type
			ct = new CsdlComplexType();

			ct.setName(CT_SH_PUR_PERMISSIONS.getName());

			ct.setProperties(Arrays.asList(isAllowed, psiActivation, dsai));

		}
		return ct;
	}

	// H. Get Entity Set
	@Override
	public CsdlEntitySet getEntitySet(FullQualifiedName entityContainer, String entitySetName) throws ODataException {

		CsdlEntitySet entitySet = null;
		List<CsdlNavigationPropertyBinding> navPropBindingList = null;
		// 1. Application Server
		if (entitySetName.equals(ES_APP_SERVER_NAME)) {
			entitySet = new CsdlEntitySet();
			entitySet.setName(ES_APP_SERVER_NAME);
			entitySet.setType(ET_APP_SERVER);
		}

		// 1. Call Detail Record
		if (entitySetName.equals(ES_CALL_DETAIL_RECORD_NAME)) {
			entitySet = new CsdlEntitySet();
			entitySet.setName(ES_CALL_DETAIL_RECORD_NAME);
			entitySet.setType(ET_CALL_DETAIL_RECORD);
		}

		// 2. Capability
		if (entitySetName.equals(ES_CAPABILITY_NAME)) {
			entitySet = new CsdlEntitySet();
			entitySet.setName(ES_CAPABILITY_NAME);
			entitySet.setType(ET_CAPABILITY);
		}

		// 3. Capability Set
		if (entitySetName.equals(ES_CAPABILITY_SET_NAME)) {

			entitySet = new CsdlEntitySet();
			entitySet.setName(ES_CAPABILITY_SET_NAME);
			entitySet.setType(ET_CAPABILITY_SET);

			// OneToMany
			CsdlNavigationPropertyBinding capBinding = new CsdlNavigationPropertyBinding();
			capBinding.setTarget("Capabilities");
			capBinding.setPath("Capabilities");

			navPropBindingList = new ArrayList<CsdlNavigationPropertyBinding>();
			navPropBindingList.add(capBinding);
			entitySet.setNavigationPropertyBindings(navPropBindingList);
		}

		// 4. Client Session Data
		if (entitySetName.equals(ES_CLIENT_SESSION_DATA_NAME)) {
			entitySet = new CsdlEntitySet();
			entitySet.setName(ES_CLIENT_SESSION_DATA_NAME);
			entitySet.setType(ET_CLIENT_SESSION_DATA);

		}

		// 4. FE Application Data
		if (entitySetName.equals(ES_FE_APP_DATA_NAME)) {
			entitySet = new CsdlEntitySet();
			entitySet.setName(ES_FE_APP_DATA_NAME);
			entitySet.setType(ET_FE_APP_DATA);
		}

		// 5. IFC
		if (entitySetName.equals(ES_IFC_NAME)) {

			entitySet = new CsdlEntitySet();
			entitySet.setName(ES_IFC_NAME);
			entitySet.setType(ET_IFC);

			// OneToOne
			CsdlNavigationPropertyBinding asBinding = new CsdlNavigationPropertyBinding();
			asBinding.setTarget("ApplicationServers");
			asBinding.setPath("ApplicationServer");

			// OneToOne
			CsdlNavigationPropertyBinding tpBinding = new CsdlNavigationPropertyBinding();
			tpBinding.setTarget("TriggerPoints");
			tpBinding.setPath("TriggerPoint");

			navPropBindingList = new ArrayList<CsdlNavigationPropertyBinding>();
			navPropBindingList.add(asBinding);
			navPropBindingList.add(tpBinding);

			entitySet.setNavigationPropertyBindings(navPropBindingList);
		}

		// 6. IMS Private Identity
		if (entitySetName.equals(ES_IMS_PRIVATE_IDENTITY_NAME)) {
			entitySet = new CsdlEntitySet();
			entitySet.setName(ES_IMS_PRIVATE_IDENTITY_NAME);
			entitySet.setType(ET_IMS_PRIVATE_IDENTITY);
		}

		// 7. IMS Public Identity*
		if (entitySetName.equals(ES_IMS_PUBLIC_IDENTITY_NAME)) {
			entitySet = new CsdlEntitySet();
			entitySet.setName(ES_IMS_PUBLIC_IDENTITY_NAME);
			entitySet.setType(ET_IMS_PUBLIC_IDENTITY);

			// OneToOne
			CsdlNavigationPropertyBinding csdBinding = new CsdlNavigationPropertyBinding();
			csdBinding.setTarget("ClientSessionDataSet");
			csdBinding.setPath("SessionData");

			navPropBindingList = new ArrayList<CsdlNavigationPropertyBinding>();
			navPropBindingList.add(csdBinding);
			entitySet.setNavigationPropertyBindings(navPropBindingList);
		}

		// 10.MergedData
		if (entitySetName.equals(ES_MERGED_DATA_NAME)) {
			entitySet = new CsdlEntitySet();
			entitySet.setName(ES_MERGED_DATA_NAME);
			entitySet.setType(ET_MERGED_DATA);
		}

		// 8. Preferred SCSCF Set
		if (entitySetName.equals(ES_PREF_SCSCF_SET_NAME)) {
			entitySet = new CsdlEntitySet();
			entitySet.setName(ES_PREF_SCSCF_SET_NAME);
			entitySet.setType(ET_PREF_SCSCF_SET);

			// OneToMany
			CsdlNavigationPropertyBinding scscfBinding = new CsdlNavigationPropertyBinding();
			scscfBinding.setTarget("PrefScscfSets");
			scscfBinding.setPath("PrefScscfSets");

			navPropBindingList = new ArrayList<CsdlNavigationPropertyBinding>();
			navPropBindingList.add(scscfBinding);

			entitySet.setNavigationPropertyBindings(navPropBindingList);
		}

		// 9. Registered Identity
		if (entitySetName.equals(ES_REGISTERED_IDENTITY_NAME)) {
			entitySet = new CsdlEntitySet();
			entitySet.setName(ES_REGISTERED_IDENTITY_NAME);
			entitySet.setType(ET_REGISTERED_IDENTITY);

			// OneToOne
			CsdlNavigationPropertyBinding csdBinding = new CsdlNavigationPropertyBinding();
			csdBinding.setTarget("ClientSessionDataSet");
			csdBinding.setPath("SessionData");

			navPropBindingList = new ArrayList<CsdlNavigationPropertyBinding>();
			navPropBindingList.add(csdBinding);
			entitySet.setNavigationPropertyBindings(navPropBindingList);
		}

		// 10. SCSCF
		if (entitySetName.equals(ES_SCSCF_NAME)) {
			entitySet = new CsdlEntitySet();
			entitySet.setName(ES_SCSCF_NAME);
			entitySet.setType(ET_SCSCF);
		}

		// 11. Service Profile
		if (entitySetName.equals(ES_SERVICE_PROFILE_NAME)) {
			entitySet = new CsdlEntitySet();
			entitySet.setName(ES_SERVICE_PROFILE_NAME);
			entitySet.setType(ET_SERVICE_PROFILE);

			// OneToMany
			CsdlNavigationPropertyBinding ifcBinding = new CsdlNavigationPropertyBinding();
			ifcBinding.setTarget("IfcSet");
			ifcBinding.setPath("IfcSet");

			navPropBindingList = new ArrayList<CsdlNavigationPropertyBinding>();
			navPropBindingList.add(ifcBinding);

			entitySet.setNavigationPropertyBindings(navPropBindingList);
		}

		// 13. Subscription Profile
		if (entitySetName.equals(ES_SUBSCRIPTION_PROFILE_NAME)) {
			entitySet = new CsdlEntitySet();
			entitySet.setName(ES_SUBSCRIPTION_PROFILE_NAME);
			entitySet.setType(ET_SUBSCRIPTION_PROFILE);

			// OneToOne
			CsdlNavigationPropertyBinding serviceProfileBinding = new CsdlNavigationPropertyBinding();
			serviceProfileBinding.setTarget("ServiceProfiles");
			serviceProfileBinding.setPath("ServiceProfile");

			// OneToOne
			CsdlNavigationPropertyBinding mergedDataProfileBinding = new CsdlNavigationPropertyBinding();
			mergedDataProfileBinding.setTarget("MergedDataSet");
			mergedDataProfileBinding.setPath("MergedData");

			// OneToOne
			CsdlNavigationPropertyBinding impiBinding = new CsdlNavigationPropertyBinding();
			impiBinding.setTarget("ImsPrivateIdentities");
			impiBinding.setPath("FullImpi");

			// OneToMany
			CsdlNavigationPropertyBinding impuBinding = new CsdlNavigationPropertyBinding();
			impuBinding.setTarget("ImsPublicIdentities");
			impuBinding.setPath("Impus");

			navPropBindingList = new ArrayList<CsdlNavigationPropertyBinding>();
			navPropBindingList.add(serviceProfileBinding);
			navPropBindingList.add(impiBinding);
			navPropBindingList.add(impuBinding);
			navPropBindingList.add(mergedDataProfileBinding);

			entitySet.setNavigationPropertyBindings(navPropBindingList);
		}

		// 14. Trigger Point
		if (entitySetName.equals(ES_TRIGGER_POINT_NAME)) {
			entitySet = new CsdlEntitySet();
			entitySet.setName(ES_TRIGGER_POINT_NAME);
			entitySet.setType(ET_TRIGGER_POINT);

			// OneToMany
			CsdlNavigationPropertyBinding sptBinding = new CsdlNavigationPropertyBinding();
			sptBinding.setTarget("ServicePointTriggers");
			sptBinding.setPath("ServicePointTriggers");

			navPropBindingList = new ArrayList<CsdlNavigationPropertyBinding>();
			navPropBindingList.add(sptBinding);

			entitySet.setNavigationPropertyBindings(navPropBindingList);
		}

		// 15. Visited Network
		if (entitySetName.equals(ES_VISITED_NETWORK_NAME)) {
			entitySet = new CsdlEntitySet();
			entitySet.setName(ES_VISITED_NETWORK_NAME);
			entitySet.setType(ET_VISITED_NETWORK);
		}

		// 16. XAP Document
		if (entitySetName.equals(ES_XCAP_DOCUMENT_NAME)) {
			entitySet = new CsdlEntitySet();
			entitySet.setName(ES_XCAP_DOCUMENT_NAME);
			entitySet.setType(ET_XCAP_DOCUMENT);
		}
		return entitySet;
	}

	// I. Get Defined Schemas
	@Override
	public List<CsdlSchema> getSchemas() throws ODataException {

		List<CsdlSchema> schemas = new ArrayList<CsdlSchema>();

		// CSDR Schema
		CsdlSchema csdrSchema = new CsdlSchema();
		csdrSchema.setNamespace(CSDR_NAMESPACE);

		// 1. Entity Types
		List<CsdlEntityType> entityTypes = new ArrayList<CsdlEntityType>();
		entityTypes.add(getEntityType(ET_APP_SERVER));
		entityTypes.add(getEntityType(ET_CALL_DETAIL_RECORD));
		entityTypes.add(getEntityType(ET_CAPABILITY));
		entityTypes.add(getEntityType(ET_CAPABILITY_SET));
		entityTypes.add(getEntityType(ET_CLIENT_SESSION_DATA));
		entityTypes.add(getEntityType(ET_FE_APP_DATA));
		entityTypes.add(getEntityType(ET_IFC));
		entityTypes.add(getEntityType(ET_IMS_PRIVATE_IDENTITY));
		entityTypes.add(getEntityType(ET_IMS_PUBLIC_IDENTITY));
		entityTypes.add(getEntityType(ET_MERGED_DATA));
		entityTypes.add(getEntityType(ET_SCSCF));
		entityTypes.add(getEntityType(ET_PREF_SCSCF_SET));
		entityTypes.add(getEntityType(ET_REGISTERED_IDENTITY));
		entityTypes.add(getEntityType(ET_SERVICE_PROFILE));
		entityTypes.add(getEntityType(ET_SUBSCRIPTION_PROFILE));
		entityTypes.add(getEntityType(ET_TRIGGER_POINT));
		entityTypes.add(getEntityType(ET_VISITED_NETWORK));
		entityTypes.add(getEntityType(ET_XCAP_DOCUMENT));
		csdrSchema.setEntityTypes(entityTypes);

		// 2. Complex Types
		List<CsdlComplexType> complexTypes = new ArrayList<CsdlComplexType>();
		complexTypes.add(getComplexType(CT_AUTH_DATA));
		complexTypes.add(getComplexType(CT_AUTH_SCHEME));
		complexTypes.add(getComplexType(CT_CHARGING_INFORMATION));
		complexTypes.add(getComplexType(CT_SPT));
		complexTypes.add(getComplexType(CT_TRIMMED_IFC));
		complexTypes.add(getComplexType(CT_TRIMMED_IMPU));
		complexTypes.add(getComplexType(CT_SH_UDR_PERMISSIONS));
		complexTypes.add(getComplexType(CT_SH_PUR_PERMISSIONS));
		csdrSchema.setComplexTypes(complexTypes);

		// 3. Enum Types
		List<CsdlEnumType> enumTypes = new ArrayList<CsdlEnumType>();
		enumTypes.add(getEnumType(ENUM_AUTH_SCHEME));
		enumTypes.add(getEnumType(ENUM_CALL_TYPE));
		enumTypes.add(getEnumType(ENUM_DSAI));
		enumTypes.add(getEnumType(ENUM_IMS_USER_STATE));
		enumTypes.add(getEnumType(ENUM_PPI));
		enumTypes.add(getEnumType(ENUM_PSI_ACTIVATION));
		enumTypes.add(getEnumType(ENUM_PUBLIC_IDENTITY_TYPE));
		enumTypes.add(getEnumType(ENUM_SESSION_CASE));
		enumTypes.add(getEnumType(ENUM_SPT_TYPE));
		csdrSchema.setEnumTypes(enumTypes);

		// 4. Entity Container
		csdrSchema.setEntityContainer(getEntityContainer());

		// 5. Add CSDR Schema
		schemas.add(csdrSchema);

		return schemas;
	}

	// J. Get Entity Container
	@Override
	public CsdlEntityContainer getEntityContainer() throws ODataException {

		// Create default Entity Container for all containers
		CsdlEntityContainer entityContainer = new CsdlEntityContainer();
		List<CsdlEntitySet> csdrEntitySets = new ArrayList<CsdlEntitySet>();
		csdrEntitySets.add(getEntitySet(CSDR_CONTAINER, ES_APP_SERVER_NAME));
		csdrEntitySets.add(getEntitySet(CSDR_CONTAINER, ES_CALL_DETAIL_RECORD_NAME));
		csdrEntitySets.add(getEntitySet(CSDR_CONTAINER, ES_CAPABILITY_NAME));
		csdrEntitySets.add(getEntitySet(CSDR_CONTAINER, ES_CAPABILITY_SET_NAME));
		csdrEntitySets.add(getEntitySet(CSDR_CONTAINER, ES_CLIENT_SESSION_DATA_NAME));
		csdrEntitySets.add(getEntitySet(CSDR_CONTAINER, ES_FE_APP_DATA_NAME));
		csdrEntitySets.add(getEntitySet(CSDR_CONTAINER, ES_IFC_NAME));
		csdrEntitySets.add(getEntitySet(CSDR_CONTAINER, ES_IMS_PRIVATE_IDENTITY_NAME));
		csdrEntitySets.add(getEntitySet(CSDR_CONTAINER, ES_IMS_PUBLIC_IDENTITY_NAME));
		csdrEntitySets.add(getEntitySet(CSDR_CONTAINER, ES_MERGED_DATA_NAME));
		csdrEntitySets.add(getEntitySet(CSDR_CONTAINER, ES_SCSCF_NAME));
		csdrEntitySets.add(getEntitySet(CSDR_CONTAINER, ES_PREF_SCSCF_SET_NAME));
		csdrEntitySets.add(getEntitySet(CSDR_CONTAINER, ES_REGISTERED_IDENTITY_NAME));
		csdrEntitySets.add(getEntitySet(CSDR_CONTAINER, ES_SERVICE_PROFILE_NAME));
		csdrEntitySets.add(getEntitySet(CSDR_CONTAINER, ES_SUBSCRIPTION_PROFILE_NAME));
		csdrEntitySets.add(getEntitySet(CSDR_CONTAINER, ES_TRIGGER_POINT_NAME));
		csdrEntitySets.add(getEntitySet(CSDR_CONTAINER, ES_VISITED_NETWORK_NAME));
		csdrEntitySets.add(getEntitySet(CSDR_CONTAINER, ES_XCAP_DOCUMENT_NAME));
		entityContainer.setName(CSDR_CONTAINER_NAME);
		entityContainer.setEntitySets(csdrEntitySets);

		return entityContainer;
	}

	// K. Get Entity Container Information
	@Override
	public CsdlEntityContainerInfo getEntityContainerInfo(FullQualifiedName entityContainerName) throws ODataException {

		CsdlEntityContainerInfo entityContainerInfo = new CsdlEntityContainerInfo();

		if (entityContainerName == null || entityContainerName.equals(CSDR_CONTAINER))
			entityContainerInfo.setContainerName(CSDR_CONTAINER);
		return entityContainerInfo;
	}

}
