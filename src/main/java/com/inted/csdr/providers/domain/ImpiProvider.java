package com.inted.csdr.providers.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import org.apache.olingo.commons.api.data.ComplexValue;

import org.apache.olingo.commons.api.data.Entity;
import org.apache.olingo.commons.api.data.EntityCollection;
import org.apache.olingo.commons.api.data.Property;
import org.apache.olingo.commons.api.data.ValueType;
import org.apache.olingo.commons.api.edm.EdmEntitySet;
import org.apache.olingo.commons.api.edm.EdmEntityType;
import org.apache.olingo.commons.api.http.HttpStatusCode;
import org.apache.olingo.server.api.ODataApplicationException;
import org.apache.olingo.server.api.uri.UriParameter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.inted.csdr.domain.Impi;
import com.inted.csdr.domain.Schemes;
import com.inted.csdr.domain.embeddables.AuthenticationData;
import com.inted.csdr.domain.embeddables.AuthenticationScheme;
import com.inted.csdr.providers.CsdrEdmProvider;
import com.inted.csdr.util.ODataUtil;
import com.inted.csdr.util.PersistenceUtil;

public class ImpiProvider {

	final Logger LOG = LoggerFactory.getLogger(ImpiProvider.class);
	EntityManagerFactory emf;
	EntityManager em;

	public ImpiProvider() {
		emf = Persistence.createEntityManagerFactory(PersistenceUtil.getEmfMongo());
		em = emf.createEntityManager();
	}

	public void destroy() {
		em.close();
		emf.close();
	}

	public List<Impi> getPImpis() {

		TypedQuery<Impi> query = em.createQuery("SELECT i FROM Impi i", Impi.class);
		List<Impi> list = query.getResultList();

		return list;
	}

	public Entity getEImpi(Impi impi, int sn) {

		Entity et = new Entity();

		et.addProperty(new Property(CsdrEdmProvider.ET_IMS_PRIVATE_IDENTITY_NAME, "ID", ValueType.PRIMITIVE, sn));

		et.addProperty(
				new Property(CsdrEdmProvider.ET_IMS_PRIVATE_IDENTITY_NAME, "Sid", ValueType.PRIMITIVE, impi.getId()));

		et.addProperty(
				new Property(CsdrEdmProvider.ET_IMS_PRIVATE_IDENTITY_NAME, "Uri", ValueType.PRIMITIVE, impi.getUri()));

		ComplexValue ad = getAuthData(impi.getAuthData());
		et.addProperty(new Property(null, "AuthenticationData", ValueType.COMPLEX, ad));

		ComplexValue as = getAuthScheme(impi.getAuthScheme());
		et.addProperty(new Property(null, "AuthenticationScheme", ValueType.COMPLEX, as));

		et.setType(CsdrEdmProvider.ET_IMS_PRIVATE_IDENTITY.getFullQualifiedNameAsString());
		return et;
	}

	public EntityCollection getEImpis() {

		List<Impi> lsp = getPImpis();
		EntityCollection collection = new EntityCollection();

		for (int i = 0; i < lsp.size(); i++) {
			Entity et = getEImpi(lsp.get(i), i + 1);
			collection.getEntities().add(et);
		}
		return collection;
	}

	public Entity findEImpi(EdmEntitySet edmEntitySet, List<UriParameter> keyParams) throws ODataApplicationException {

		EntityCollection entitySet = getEImpis();
		EdmEntityType edmEntityType = edmEntitySet.getEntityType();

		/* generic approach to find the requested entity */
		Entity requestedEntity = ODataUtil.findEntity(edmEntityType, entitySet, keyParams);

		if (requestedEntity == null) {
			throw new ODataApplicationException("Entity for requested key doesn't exist",
					HttpStatusCode.NOT_FOUND.getStatusCode(), Locale.ENGLISH);
		}
		return requestedEntity;
	}

	// Auth Data
	public ComplexValue getAuthData(AuthenticationData ad) {

		ComplexValue cv = new ComplexValue();

		cv.getValue().add(new Property(CsdrEdmProvider.CT_AUTH_DATA_NAME, "SecretKey", ValueType.PRIMITIVE,
				new String(ad.getSecretKey())));

		cv.getValue().add(
				new Property(CsdrEdmProvider.CT_AUTH_DATA_NAME, "Op", ValueType.PRIMITIVE, new String(ad.getOp())));

		cv.getValue().add(
				new Property(CsdrEdmProvider.CT_AUTH_DATA_NAME, "Amf", ValueType.PRIMITIVE, new String(ad.getAmf())));

		cv.getValue().add(new Property(CsdrEdmProvider.CT_AUTH_DATA_NAME, "Sqn", ValueType.PRIMITIVE, ad.getSqn()));

		cv.getValue().add(
				new Property(CsdrEdmProvider.CT_AUTH_DATA_NAME, "IpAddress", ValueType.PRIMITIVE, ad.getIpAddress()));

		cv.getValue().add(new Property(CsdrEdmProvider.CT_AUTH_DATA_NAME, "LineIdentifier", ValueType.PRIMITIVE,
				ad.getLineIdentifier()));

		return cv;
	}

	// Auth Scheme
	public ComplexValue getAuthScheme(AuthenticationScheme as) {

		ComplexValue cv = new ComplexValue();

		cv.getValue().add(new Property(CsdrEdmProvider.CT_AUTH_SCHEME_NAME, "DefaultAuthScheme", ValueType.ENUM,
				as.getDefaultScheme()));

		List<Integer> schemes = as.getSupportedSchemes();

		if (schemes.contains(255)) {
			List<Integer> list = new ArrayList<Integer>();
			list.add(Schemes.AKAv1);
			list.add(Schemes.AKAv2);
			list.add(Schemes.Auth_Scheme_MD5);
			list.add(Schemes.Digest);
			list.add(Schemes.Early);
			list.add(Schemes.HTTP_Digest_MD5);
			list.add(Schemes.NASS_Bundled);
			list.add(Schemes.SIP_Digest);
			schemes = list;
		}

		cv.getValue().add(new Property(CsdrEdmProvider.CT_AUTH_SCHEME_NAME, "SupportedAuthSchemes",
				ValueType.COLLECTION_ENUM, schemes));

		return cv;
	}

	/**
	 * Generate Authentication Scheme for IMPI From IMPI.java at FHoSS
	 */
	public int generateAuthScheme(boolean akav1, boolean akav2, boolean md5, boolean digest, boolean sip_digest,
			boolean http_digest, boolean early, boolean nass_bundle, boolean all) {

		if (all) {
			return 255;
		} else {
			int result = 0;

			if (akav1) {
				result |= 1;
			}
			if (akav2) {
				result |= 2;
			}
			if (md5) {
				result |= 4;
			}
			if (digest) {
				result |= 8;
			}
			if (sip_digest) {
				result |= 16;
			}
			if (http_digest) {
				result |= 32;
			}
			if (early) {
				result |= 64;
			}
			if (nass_bundle) {
				result |= 128;
			}

			return result;
		}
	}
}
