package com.inted.csdr.providers.domain;

import java.util.List;
import java.util.Locale;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import org.apache.olingo.commons.api.data.Entity;
import org.apache.olingo.commons.api.data.EntityCollection;
import org.apache.olingo.commons.api.data.Property;
import org.apache.olingo.commons.api.data.ValueType;
import org.apache.olingo.commons.api.edm.EdmEntitySet;
import org.apache.olingo.commons.api.edm.EdmEntityType;
import org.apache.olingo.commons.api.http.HttpStatusCode;
import org.apache.olingo.server.api.ODataApplicationException;
import org.apache.olingo.server.api.uri.UriParameter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.inted.csdr.domain.Scscf;
import com.inted.csdr.providers.CsdrEdmProvider;
import com.inted.csdr.util.ODataUtil;
import com.inted.csdr.util.PersistenceUtil;

public class ScscfProvider {

	final Logger LOG = LoggerFactory.getLogger(ScscfProvider.class);
	EntityManagerFactory emf;
	EntityManager em;

	public ScscfProvider() {
		emf = Persistence.createEntityManagerFactory(PersistenceUtil.getEmfMongo());
		em = emf.createEntityManager();
	}

	public void destroy() {
		em.close();
		emf.close();
	}

	public void add(Scscf scscf){
		try{
			em.persist(scscf);
		}catch(Exception ex){
			LOG.error("Could not add Scscf!");
		}
		
	}

	public List<Scscf> getPScscfs() {

		TypedQuery<Scscf> query = em.createQuery("SELECT s FROM Scscf s", Scscf.class);
		List<Scscf> list = query.getResultList();

		return list;
	}

	public Entity getEScscf(Scscf s, int sn) {

		Entity et = new Entity();

		et.addProperty(new Property(CsdrEdmProvider.ET_SCSCF_NAME, "ID", ValueType.PRIMITIVE, sn));

		et.addProperty(new Property(CsdrEdmProvider.ET_SCSCF_NAME, "Sid", ValueType.PRIMITIVE, s.getId()));

		et.addProperty(new Property(CsdrEdmProvider.ET_SCSCF_NAME, "Name", ValueType.PRIMITIVE, s.getName()));

		et.addProperty(new Property(CsdrEdmProvider.ET_SCSCF_NAME, "SipUri", ValueType.PRIMITIVE, s.getSipUri()));

		et.setType(CsdrEdmProvider.ET_SCSCF.getFullQualifiedNameAsString());
		return et;
	}

	public EntityCollection getEScscfs() {

		List<Scscf> lsp = getPScscfs();
		EntityCollection collection = new EntityCollection();

		for (int i = 0; i < lsp.size(); i++) {
			Entity et = getEScscf(lsp.get(i), i + 1);
			collection.getEntities().add(et);
		}
		return collection;
	}

	public Entity findEScscf(EdmEntitySet edmEntitySet, List<UriParameter> keyParams) throws ODataApplicationException {

		EntityCollection entitySet = getEScscfs();
		EdmEntityType edmEntityType = edmEntitySet.getEntityType();

		/* generic approach to find the requested entity */
		Entity requestedEntity = ODataUtil.findEntity(edmEntityType, entitySet, keyParams);

		if (requestedEntity == null) {
			throw new ODataApplicationException("Entity for requested key doesn't exist",
					HttpStatusCode.NOT_FOUND.getStatusCode(), Locale.ENGLISH);
		}
		return requestedEntity;
	}
}
