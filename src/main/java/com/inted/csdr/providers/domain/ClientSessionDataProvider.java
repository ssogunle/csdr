package com.inted.csdr.providers.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.apache.olingo.commons.api.data.Entity;
import org.apache.olingo.commons.api.data.EntityCollection;
import org.apache.olingo.commons.api.data.Property;
import org.apache.olingo.commons.api.data.ValueType;
import org.apache.olingo.commons.api.edm.EdmEntitySet;
import org.apache.olingo.commons.api.edm.EdmEntityType;
import org.apache.olingo.commons.api.http.HttpStatusCode;
import org.apache.olingo.server.api.ODataApplicationException;
import org.apache.olingo.server.api.uri.UriParameter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.inted.csdr.domain.ClientSessionData;
import com.inted.csdr.domain.RedisKeys;
import com.inted.csdr.providers.CsdrEdmProvider;
import com.inted.csdr.util.ODataUtil;
import com.inted.csdr.util.PersistenceUtil;

public class ClientSessionDataProvider {
	final Logger LOG = LoggerFactory.getLogger(ClientSessionDataProvider.class);
	private final String superKeyName = "ImpuSet";
	private EntityManagerFactory emf;
	private EntityManager em;

	public ClientSessionDataProvider() {
		emf = Persistence.createEntityManagerFactory(PersistenceUtil.getEmfRedis());
		em = emf.createEntityManager();
	}

	public void destroy() {
		em.close();
		emf.close();
	}

	public Entity add(Entity et) {
		String subscriberUri = et.getProperty("SubscriberUri") == null ? null
				: String.valueOf(et.getProperty("SubscriberUri").getValue());

		String lastKnownSessionId = et.getProperty("LastKnownSessionId") == null ? null
				: String.valueOf(et.getProperty("LastKnownSessionId").getValue());

		String lastKnownState = et.getProperty("LastKnownState") == null ? null
				: String.valueOf(et.getProperty("LastKnownState").getValue());

		String userAgent = et.getProperty("UserAgent") == null ? null
				: String.valueOf(et.getProperty("UserAgent").getValue());

		String mediaType = et.getProperty("MediaType") == null ? null
				: String.valueOf(et.getProperty("MediaType").getValue());

		String mediaPort = et.getProperty("MediaPort") == null ? null
				: String.valueOf(et.getProperty("MediaPort").getValue());

		String mediaProtocol = et.getProperty("MediaProtocol") == null ? null
				: String.valueOf(et.getProperty("MediaProtocol").getValue());

		ClientSessionData csd = new ClientSessionData();
		csd.setId(subscriberUri);
		csd.setLastKnownSessionId(lastKnownSessionId);
		csd.setLastKnownState(lastKnownState);
		csd.setMediaPort(mediaPort);
		csd.setMediaProtocol(mediaProtocol);
		csd.setMediaType(mediaType);
		csd.setUserAgent(userAgent);
		add(csd);

		RedisKeyProvider rkw = new RedisKeyProvider();
		RedisKeys rk = rkw.findPRedisKeys(superKeyName);

		if (rk == null) {
			rk = new RedisKeys();
			rk.setId(superKeyName);
		}

		if (!rk.getKeySet().contains(subscriberUri)) {
			rk.getKeySet().add(subscriberUri);
			rkw.update(rk);
		}

		rkw.destroy();

		return et;
	}

	// JPA Models
	public void add(ClientSessionData x) {

		try {
			em.merge(x);
		} catch (Exception ex) {
			LOG.error("Could not merge Client Session Data: " + ex);
		}
	}

	public ClientSessionData findPClientSessionData(String key) {
		ClientSessionData csd = (ClientSessionData) em.find(ClientSessionData.class, key);
		return csd;
	}

	// EDM
	public Entity getEClientSessionData(ClientSessionData csd, int sn) {

		if (csd == null)
			return null;

		Entity et = new Entity();

		et.addProperty(new Property(CsdrEdmProvider.ET_CLIENT_SESSION_DATA_NAME, "ID", ValueType.PRIMITIVE, sn));
		et.addProperty(new Property(CsdrEdmProvider.ET_CLIENT_SESSION_DATA_NAME, "SubscriberUri", ValueType.PRIMITIVE,
				csd.getId()));
		et.addProperty(new Property(CsdrEdmProvider.ET_CLIENT_SESSION_DATA_NAME, "LastKnownSessionId",
				ValueType.PRIMITIVE, csd.getLastKnownSessionId()));
		et.addProperty(new Property(CsdrEdmProvider.ET_CLIENT_SESSION_DATA_NAME, "LastKnownState", ValueType.PRIMITIVE,
				csd.getLastKnownState()));
		et.addProperty(new Property(CsdrEdmProvider.ET_CLIENT_SESSION_DATA_NAME, "UserAgent", ValueType.PRIMITIVE,
				csd.getUserAgent()));
		et.addProperty(new Property(CsdrEdmProvider.ET_CLIENT_SESSION_DATA_NAME, "MediaType", ValueType.PRIMITIVE,
				csd.getMediaType()));
		et.addProperty(new Property(CsdrEdmProvider.ET_CLIENT_SESSION_DATA_NAME, "MediaPort", ValueType.PRIMITIVE,
				csd.getMediaPort()));
		et.addProperty(new Property(CsdrEdmProvider.ET_CLIENT_SESSION_DATA_NAME, "MediaProtocol", ValueType.PRIMITIVE,
				csd.getMediaProtocol()));

		return et;
	}

	public List<ClientSessionData> getPClientSessionDataSet() {
		List<ClientSessionData> list = new ArrayList<ClientSessionData>();

		RedisKeyProvider rkw = new RedisKeyProvider();
		RedisKeys rks = rkw.findPRedisKeys(superKeyName);

		if (rks == null || rks.getKeySet() == null || rks.getKeySet().size() == 0)
			return list;

		List<String> impus = rks.getKeySet();
		ClientSessionDataProvider csdw = new ClientSessionDataProvider();

		for (String impu : impus) {
			ClientSessionData csd = csdw.findPClientSessionData(impu);
			if (csd != null) {
				list.add(csd);
			}
		}
		
		rkw.destroy();
		csdw.destroy();
		return list;
	}

	public EntityCollection getEClientSessionDataSet() {

		EntityCollection collection = new EntityCollection();
		RedisKeyProvider rkw = new RedisKeyProvider();
		RedisKeys rks = rkw.findPRedisKeys(superKeyName);

		if (rks == null || rks.getKeySet() == null || rks.getKeySet().size() == 0)
			return collection;

		List<String> impus = rks.getKeySet();
		ClientSessionDataProvider csdw = new ClientSessionDataProvider();

		int i = 0;
		for (String impu : impus) {
			ClientSessionData csd = csdw.findPClientSessionData(impu);
			if (csd != null) {
				collection.getEntities().add(getEClientSessionData(csd, i + 1));
				i++;
			}
		}
		rkw.destroy();
		csdw.destroy();
		return collection;
	}

	public Entity findEClientSessionData(EdmEntitySet edmEntitySet, List<UriParameter> keyParams)
			throws ODataApplicationException {

		EntityCollection entitySet = getEClientSessionDataSet();
		EdmEntityType edmEntityType = edmEntitySet.getEntityType();

		Entity requestedEntity = ODataUtil.findEntity(edmEntityType, entitySet, keyParams);

		if (requestedEntity == null) {

			throw new ODataApplicationException("Entity for requested key doesn't exist",
					HttpStatusCode.NOT_FOUND.getStatusCode(), Locale.ENGLISH);
		}

		return requestedEntity;
	}
}