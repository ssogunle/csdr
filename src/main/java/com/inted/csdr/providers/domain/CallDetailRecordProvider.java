package com.inted.csdr.providers.domain;

import java.util.List;
import java.util.Locale;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import org.apache.olingo.commons.api.data.Entity;
import org.apache.olingo.commons.api.data.EntityCollection;
import org.apache.olingo.commons.api.data.Property;
import org.apache.olingo.commons.api.data.ValueType;
import org.apache.olingo.commons.api.edm.EdmEntitySet;
import org.apache.olingo.commons.api.edm.EdmEntityType;
import org.apache.olingo.commons.api.http.HttpStatusCode;
import org.apache.olingo.server.api.ODataApplicationException;
import org.apache.olingo.server.api.uri.UriParameter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.inted.csdr.domain.CallDetailRecord;
import com.inted.csdr.providers.CsdrEdmProvider;
import com.inted.csdr.util.ODataUtil;
import com.inted.csdr.util.PersistenceUtil;

public class CallDetailRecordProvider {
	final Logger LOG = LoggerFactory.getLogger(CallDetailRecordProvider.class);

	EntityManagerFactory emf;
	EntityManager em;

	public CallDetailRecordProvider() {
		emf = Persistence.createEntityManagerFactory(PersistenceUtil.getEmfMysql());
		em = emf.createEntityManager();
	}

	public void destroy() {
		em.close();
		emf.close();
	}

	// JPA Models
	public void add(CallDetailRecord x) {

		try {
			em.persist(x);
		} catch (Exception ex) {
			LOG.error("Could not add Call Detail Record: " + ex);
		}
	}

	public CallDetailRecord findPCallDetailRecord(int id) {
		CallDetailRecord x = em.find(CallDetailRecord.class, id);
		return x;
	}

	public Entity add(Entity et) {

		String sipMethod = et.getProperty("SipMethod") == null ? null
				: String.valueOf(et.getProperty("SipMethod").getValue());

		String duration = et.getProperty("Duration") == null ? null :  String.valueOf(et.getProperty("Duration").getValue());

		String from = et.getProperty("From") == null ? null :  String.valueOf(et.getProperty("From").getValue());

		String to = et.getProperty("To") == null ? null :  String.valueOf(et.getProperty("To").getValue());

		String callId = et.getProperty("CallId") == null ? null :  String.valueOf(et.getProperty("CallId").getValue());

		String cSeq = et.getProperty("CSeq") == null ? null :  String.valueOf(et.getProperty("CSeq").getValue());

		String userAgent = et.getProperty("UserAgent") == null ? null
				:  String.valueOf(et.getProperty("UserAgent").getValue());

		String contact = et.getProperty("Contact") == null ? null :  String.valueOf(et.getProperty("Contact").getValue());

		boolean containsSdp = et.getProperty("ContainsSdp") == null ? false
				: Boolean.parseBoolean( String.valueOf(et.getProperty("ContainsSdp").getValue()));

		int sessionExpires = et.getProperty("SessionExpires") == null ? -1
				: Integer.parseInt( String.valueOf(et.getProperty("SessionExpires").getValue()));

		CallDetailRecord cdr = new CallDetailRecord();
		cdr.setSipMethod(sipMethod);
		cdr.setCallDuration(duration);
		cdr.setFromAddress(from);
		cdr.setToAddress(to);
		cdr.setCallId(callId);
		cdr.setcSeq(cSeq);
		cdr.setContainsSdp(containsSdp);
		cdr.setContact(contact);
		cdr.setSessionExpires(sessionExpires);
		cdr.setUserAgent(userAgent);

		add(cdr);

		return et;
	}

	public List<CallDetailRecord> getPCallDetailRecords() {
		TypedQuery<CallDetailRecord> query = em.createQuery("SELECT x FROM CallDetailRecord x", CallDetailRecord.class);
		List<CallDetailRecord> list = query.getResultList();
		return list;
	}

	public Entity getECallDetailRecord(CallDetailRecord cdr) {

		Entity et = new Entity();
		
		et.addProperty(
				new Property(CsdrEdmProvider.ET_CALL_DETAIL_RECORD_NAME, "ID", ValueType.PRIMITIVE, cdr.getId()));
		et.addProperty(new Property(CsdrEdmProvider.ET_CALL_DETAIL_RECORD_NAME, "SipMethod", ValueType.PRIMITIVE,
				cdr.getSipMethod()));
		et.addProperty(new Property(CsdrEdmProvider.ET_CALL_DETAIL_RECORD_NAME, "CallId", ValueType.PRIMITIVE,
				cdr.getCallId()));
		et.addProperty(
				new Property(CsdrEdmProvider.ET_CALL_DETAIL_RECORD_NAME, "CSeq", ValueType.PRIMITIVE, cdr.getcSeq()));
		et.addProperty(new Property(CsdrEdmProvider.ET_CALL_DETAIL_RECORD_NAME, "From", ValueType.PRIMITIVE,
				cdr.getFromAddress()));
		et.addProperty(new Property(CsdrEdmProvider.ET_CALL_DETAIL_RECORD_NAME, "To", ValueType.PRIMITIVE,
				cdr.getToAddress()));
		et.addProperty(new Property(CsdrEdmProvider.ET_CALL_DETAIL_RECORD_NAME, "Duration", ValueType.PRIMITIVE,
				cdr.getCallDuration()));
		et.addProperty(new Property(CsdrEdmProvider.ET_CALL_DETAIL_RECORD_NAME, "UserAgent", ValueType.PRIMITIVE,
				cdr.getUserAgent()));
		et.addProperty(new Property(CsdrEdmProvider.ET_CALL_DETAIL_RECORD_NAME, "SessionExpires", ValueType.PRIMITIVE,
				cdr.getSessionExpires()));
		et.addProperty(new Property(CsdrEdmProvider.ET_CALL_DETAIL_RECORD_NAME, "Contact", ValueType.PRIMITIVE,
				cdr.getContact()));
		et.addProperty(new Property(CsdrEdmProvider.ET_CALL_DETAIL_RECORD_NAME, "ContainsSdp", ValueType.PRIMITIVE,
				cdr.getContainsSdp()));

		return et;
	}

	public EntityCollection getECallDetailRecords() {

		List<CallDetailRecord> lcdr = getPCallDetailRecords();
		EntityCollection collection = new EntityCollection();

		for (CallDetailRecord cdr : lcdr) {
			Entity et = getECallDetailRecord(cdr);
			collection.getEntities().add(et);
		}
		return collection;
	}

	public Entity findECallDetailRecord(EdmEntitySet edmEntitySet, List<UriParameter> keyParams)
			throws ODataApplicationException {

		EntityCollection entitySet = getECallDetailRecords();
		EdmEntityType edmEntityType = edmEntitySet.getEntityType();

		Entity requestedEntity = ODataUtil.findEntity(edmEntityType, entitySet, keyParams);

		if (requestedEntity == null) {

			throw new ODataApplicationException("Entity for requested key doesn't exist",
					HttpStatusCode.NOT_FOUND.getStatusCode(), Locale.ENGLISH);
		}

		return requestedEntity;
	}
}
