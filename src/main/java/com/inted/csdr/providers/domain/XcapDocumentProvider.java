package com.inted.csdr.providers.domain;

import java.util.List;
import java.util.Locale;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import org.apache.olingo.commons.api.data.Entity;
import org.apache.olingo.commons.api.data.EntityCollection;
import org.apache.olingo.commons.api.data.Property;
import org.apache.olingo.commons.api.data.ValueType;
import org.apache.olingo.commons.api.edm.EdmEntitySet;
import org.apache.olingo.commons.api.edm.EdmEntityType;
import org.apache.olingo.commons.api.http.HttpStatusCode;
import org.apache.olingo.server.api.ODataApplicationException;
import org.apache.olingo.server.api.uri.UriParameter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.inted.csdr.domain.XcapDocument;
import com.inted.csdr.providers.CsdrEdmProvider;
import com.inted.csdr.util.ODataUtil;
import com.inted.csdr.util.PersistenceUtil;

public class XcapDocumentProvider {

	final Logger LOG = LoggerFactory.getLogger(XcapDocumentProvider.class);
	EntityManagerFactory emf;
	EntityManager em;

	public XcapDocumentProvider() {
		emf = Persistence.createEntityManagerFactory(PersistenceUtil.getEmfMongo());
	    em = emf.createEntityManager();

	}

	public void destroy() {
		em.close();
		emf.close();
	}

	public void add(Object obj) {
		try {
			em.persist(obj);
		} catch (Exception ex) {
			LOG.error("Cound to add XCAP Document: " + ex);
		}
	}

	public Entity addXcapDocument(Entity et) {
		XcapDocument xd = createDocument(et);
		try {
			em.persist(xd);
		} catch (Exception ex) {
			LOG.error("Could not add XCAP Document");
		}
		em.close();
		emf.close();
		return et;
	}

	public Entity getXcapDocument(EdmEntitySet edmEntitySet, List<UriParameter> keyParams)
			throws ODataApplicationException {
		EntityCollection entitySet = getEXcapDocuments();
		EdmEntityType edmEntityType = edmEntitySet.getEntityType();

		/* generic approach to find the requested entity */
		Entity requestedEntity = ODataUtil.findEntity(edmEntityType, entitySet, keyParams);

		if (requestedEntity == null) {
			// this variable is null if our data doesn't contain an entity for
			// the requested key
			// Throw suitable exception
			throw new ODataApplicationException("Entity for requested key doesn't exist",
					HttpStatusCode.NOT_FOUND.getStatusCode(), Locale.ENGLISH);
		}

		return requestedEntity;
	}

	public Entity getEXcapDocument(XcapDocument doc, int sn) {
		Entity et = new Entity();

		et.addProperty(new Property(CsdrEdmProvider.ET_XCAP_DOCUMENT_NAME, "ID", ValueType.PRIMITIVE, sn));
		et.addProperty(new Property(CsdrEdmProvider.ET_XCAP_DOCUMENT_NAME, "AppUsage", ValueType.PRIMITIVE,
				doc.getAppUsage()));
		et.addProperty(new Property(CsdrEdmProvider.ET_XCAP_DOCUMENT_NAME, "DocumentName", ValueType.PRIMITIVE,
				doc.getName()));
		et.addProperty(new Property(CsdrEdmProvider.ET_XCAP_DOCUMENT_NAME, "Xui", ValueType.PRIMITIVE, doc.getXui()));
		et.addProperty(new Property(CsdrEdmProvider.ET_XCAP_DOCUMENT_NAME, "Data", ValueType.PRIMITIVE, doc.getData()));
		et.addProperty(new Property(CsdrEdmProvider.ET_XCAP_DOCUMENT_NAME, "eTag", ValueType.PRIMITIVE, doc.geteTag()));
		et.addProperty(
				new Property(CsdrEdmProvider.ET_XCAP_DOCUMENT_NAME, "isUpdated", ValueType.PRIMITIVE, doc.getName()));

		return et;
	}

	public EntityCollection getEXcapDocuments() {

		List<XcapDocument> lxd = getPXcapDocuments();
		/*
		 * Convert JPA Entity to OData EntityCollection
		 */
		EntityCollection entitySet = new EntityCollection();

		for (int i = 0; i < lxd.size(); i++) {

			Entity et = getEXcapDocument(lxd.get(i), i + 1);

			entitySet.getEntities().add(et);

		}
		return entitySet;

	}

	public List<XcapDocument> getPXcapDocuments() {
		TypedQuery<XcapDocument> query = em.createQuery("SELECT xd FROM XcapDocument xd", XcapDocument.class);

		List<XcapDocument> list = query.getResultList();

		em.close();
		emf.close();

		return list;
	}

	private XcapDocument createDocument(Entity et) {
		XcapDocument doc = new XcapDocument();

		String appUsage = et.getProperties().contains("AppUsage") ? et.getProperty("AppUsage").getValue().toString()
				: "";
		String name = et.getProperties().contains("DocumentName") ? et.getProperty("DocumentName").getValue().toString()
				: "";
		String xui = et.getProperties().contains("Xui") ? et.getProperty("Xui").getValue().toString() : "";
		String data = et.getProperties().contains("Data") ? et.getProperty("Data").getValue().toString() : "";
		String etag = et.getProperties().contains("eTag") ? et.getProperty("eTag").getValue().toString() : "";
		Boolean isUpdated = et.getProperties().contains("IsUpdated") ? (Boolean) et.getProperty("IsUpdated").getValue()
				: false;

		doc.setAppUsage(appUsage);
		doc.setData(data);
		doc.setXui(xui);
		doc.seteTag(etag);
		doc.setIsUpdated(isUpdated);
		doc.setName(name);

		return doc;
	}
}
