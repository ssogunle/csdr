package com.inted.csdr.providers.domain;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import org.apache.olingo.commons.api.data.Entity;
import org.apache.olingo.commons.api.data.EntityCollection;
import org.apache.olingo.commons.api.data.Property;
import org.apache.olingo.commons.api.data.ValueType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.inted.csdr.domain.Capability;
import com.inted.csdr.domain.CapabilitySet;
import com.inted.csdr.providers.CsdrEdmProvider;
import com.inted.csdr.util.PersistenceUtil;

public class CapabilitySetProvider {

		final Logger LOG = LoggerFactory.getLogger(CapabilitySetProvider.class);
		
		EntityManagerFactory emf;
		EntityManager em;
		
		public void start(){
			emf = Persistence
				.createEntityManagerFactory(PersistenceUtil.getEmfMongo());
			em = emf.createEntityManager();
		}
		
		public void destroy() {
			em.close();
			emf.close();
		}
		
		public void add(String name, String capIds[]){
			
			CapabilitySet cs = new CapabilitySet();
			cs.setName(name);
			
			CapabilityProvider cw = new CapabilityProvider();
			
			for(int i=0; i<capIds.length; i++){
				Capability c = cw.findCapability(capIds[i]);
				
				if(c!=null)
					cs.getCapabilityList().add(c);
			}
			
			this.start();
			try {
				em.persist(cs);
			} catch (Exception ex) {
				LOG.error("Could not add Capability Set: " + ex);
			}
			this.destroy();
		}
		
		public CapabilitySet findCapabilitySet(String Id){
			
			this.start();
			CapabilitySet cs = em.find(CapabilitySet.class, Id);
			this.destroy();
			
			return cs;
		}
		
		public List<CapabilitySet> getPCapabilitySets() {

			TypedQuery<CapabilitySet> query = em.createQuery(
					"SELECT cs FROM CapabilitySet cs", CapabilitySet.class);
			List<CapabilitySet> list = query.getResultList();
			return list;
		}
		
		public Entity getECapabilitySet(CapabilitySet c, int sn) {
			Entity et = new Entity();
			et.addProperty(new Property(CsdrEdmProvider.ET_CAPABILITY_SET_NAME, "ID",
					ValueType.PRIMITIVE, sn));

			et.addProperty(new Property(CsdrEdmProvider.ET_CAPABILITY_SET_NAME, "Sid",
					ValueType.PRIMITIVE, c.getId()));

			et.addProperty(new Property(CsdrEdmProvider.ET_CAPABILITY_SET_NAME, "Name",
					ValueType.PRIMITIVE, c.getName()));
			
			et.setType(CsdrEdmProvider.ET_CAPABILITY_SET.getFullQualifiedNameAsString());
			return et;
		}
		
		public EntityCollection getECapabilities() {

			List<CapabilitySet> lcs = getPCapabilitySets();
			// Convert JPA Entity Set to OData EntityCollection
			EntityCollection collection = new EntityCollection();

			for (int i = 0; i < lcs.size(); i++) {
				Entity et = getECapabilitySet(lcs.get(i), i + 1);
				collection.getEntities().add(et);
			}
			return collection;
		}
		
		
}
