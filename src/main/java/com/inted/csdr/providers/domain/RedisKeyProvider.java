package com.inted.csdr.providers.domain;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.inted.csdr.domain.RedisKeys;
import com.inted.csdr.util.PersistenceUtil;

public class RedisKeyProvider {

	final Logger LOG = LoggerFactory.getLogger(RedisKeyProvider.class);

	EntityManagerFactory emf;
	EntityManager em;

	public RedisKeyProvider() {
		emf = Persistence.createEntityManagerFactory(PersistenceUtil.getEmfRedis());
		em = emf.createEntityManager();
	}

	public void destroy() {
		em.close();
		emf.close();
	}

	// JPA Models
	public void add(RedisKeys x) {

		try {
			em.persist(x);
		} catch (Exception ex) {
			LOG.error("Could not add Redis Keys: " + ex);
		}
	}

	public RedisKeys update(RedisKeys x) {
		RedisKeys rks = null;
		try {
			rks = em.merge(x);
		} catch (Exception ex) {
			LOG.error("Could not update Redis Keys: " + ex);
		}
		return rks;
	}

	public RedisKeys findPRedisKeys(String superKey) {
		RedisKeys rks = (RedisKeys) em.find(RedisKeys.class, superKey);
		return rks;
	}
}
