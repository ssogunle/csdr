package com.inted.csdr.providers.domain;

import java.util.List;
import java.util.Locale;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import org.apache.olingo.commons.api.data.Entity;
import org.apache.olingo.commons.api.data.EntityCollection;
import org.apache.olingo.commons.api.data.Property;
import org.apache.olingo.commons.api.data.ValueType;
import org.apache.olingo.commons.api.edm.EdmEntitySet;
import org.apache.olingo.commons.api.edm.EdmEntityType;
import org.apache.olingo.commons.api.http.HttpStatusCode;
import org.apache.olingo.server.api.ODataApplicationException;
import org.apache.olingo.server.api.uri.UriParameter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.inted.csdr.domain.FeData;
import com.inted.csdr.providers.CsdrEdmProvider;
import com.inted.csdr.util.ODataUtil;
import com.inted.csdr.util.PersistenceUtil;

public class FeDataProvider {
	final Logger LOG = LoggerFactory.getLogger(FeDataProvider.class);
	EntityManagerFactory emf;
	EntityManager em; 
	
	public FeDataProvider(){
		emf = Persistence
			.createEntityManagerFactory(PersistenceUtil.getEmfMysql());
		em = emf.createEntityManager();
	}
	
	public void destroy() {
		em.close();
		emf.close();
	}
	
	public void add(FeData feData){
		try {
			em.persist(feData);
		} catch (Exception ex) {
			LOG.error("Could not add FE Application Data");
		}
		//this.destroy();
	}
	
	public Entity addFeData(Entity et) {
		//this.start();
		FeData f = createData(et);

		try {
			em.persist(f);
		} catch (Exception ex) {
			LOG.error("Could not add FE Application Data");
		}
		//this.destroy();

		return et;
	}

	public Entity getFeApplication(EdmEntitySet edmEntitySet, List<UriParameter> keyParams)
			throws ODataApplicationException{
		
		EntityCollection entitySet= getFeApplicationSet();
		EdmEntityType edmEntityType = edmEntitySet.getEntityType();

		/* generic approach to find the requested entity */
		Entity requestedEntity = ODataUtil.findEntity(edmEntityType, entitySet, keyParams);

		if (requestedEntity == null) {
			// this variable is null if our data doesn't contain an entity for
			// the requested key
			// Throw suitable exception
			throw new ODataApplicationException("Entity for requested key doesn't exist",
					HttpStatusCode.NOT_FOUND.getStatusCode(), Locale.ENGLISH);
		}

		return requestedEntity;
	}
	
	public Entity getFeData(FeData feData, int sn){
		
		Entity et = new Entity();
		et.addProperty(new Property(CsdrEdmProvider.ET_FE_APP_DATA_NAME, "Id", ValueType.PRIMITIVE,sn));
		et.addProperty(new Property(CsdrEdmProvider.ET_FE_APP_DATA_NAME, "Name", ValueType.PRIMITIVE, feData.getName()));
		et.addProperty(new Property(CsdrEdmProvider.ET_FE_APP_DATA_NAME, "IpAddress", ValueType.PRIMITIVE, feData.getIpAddress()));
		et.addProperty(new Property(CsdrEdmProvider.ET_FE_APP_DATA_NAME, "OdataNamespace", ValueType.PRIMITIVE, feData.getOdataNamespace()));
		et.addProperty(new Property(CsdrEdmProvider.ET_FE_APP_DATA_NAME, "ApplicationViewIdentifier", ValueType.PRIMITIVE, feData.getAppViewId()));
	//	et.addProperty(new Property(CsdrEdmProvider.ET_FE_APP_DATA_NAME, "ims_subscription", ValueType.ENTITY, lfad.get(i).getImsSubscription()));
		
		return et;
	}
	public EntityCollection getFeApplicationSet(){
		
		List<FeData> lfad = getPFeDataSet();
		
		/*
		 * Convert JPA Entity to OData EntityCollection
		 */
		EntityCollection entitySet = new EntityCollection();

		for (int i = 0; i < lfad.size(); i++) {

			entitySet.getEntities().add(getFeData(lfad.get(i), i+1));
		}
		
		return entitySet;
	}
	public List<FeData> getPFeDataSet() {
	//	this.start();
		TypedQuery<FeData> query = em.createQuery("SELECT f FROM FeData f",
				FeData.class);

		List<FeData> list = query.getResultList();

	//	this.destroy();

		return list;
	}

	private FeData createData(Entity et) {

		FeData f = new FeData();
		String name = et.getProperties().contains("Name") ? et.getProperty("Name").getValue().toString() : "";
		String ipAddress = et.getProperties().contains("IpAddress") ? et.getProperty("IpAddress").getValue().toString() : "";
		String odataN = et.getProperties().contains("OdataNamespace")
				? et.getProperty("OdataNamespace").getValue().toString() : "";
		String appV = et.getProperties().contains("ApplicationViewIdentifier")
				? et.getProperty("ApplicationViewIdentifier").getValue().toString() : "";
	//	Object o = et.getProperties().contains("ims_subscription") ? et.getProperty("ims_subscription").getValue() : "";
	//	ImsSubscription imsu = (ImsSubscription) o;

		f.setAppViewId(appV);
		// f.setImsSubscription(imsu);
		f.setName(name);
		f.setName(ipAddress);
		f.setOdataNamespace(odataN);
		return f;
	}
}
