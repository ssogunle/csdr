package com.inted.csdr.providers.domain;


import java.util.List;
import java.util.Locale;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;


import org.apache.olingo.commons.api.data.Entity;
import org.apache.olingo.commons.api.data.EntityCollection;
import org.apache.olingo.commons.api.data.Property;
import org.apache.olingo.commons.api.data.ValueType;
import org.apache.olingo.commons.api.edm.EdmEntitySet;
import org.apache.olingo.commons.api.edm.EdmEntityType;

import org.apache.olingo.commons.api.http.HttpStatusCode;
import org.apache.olingo.server.api.ODataApplicationException;
import org.apache.olingo.server.api.uri.UriParameter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.inted.csdr.domain.Ifc;
import com.inted.csdr.providers.CsdrEdmProvider;
import com.inted.csdr.util.ODataUtil;
import com.inted.csdr.util.PersistenceUtil;

public class IfcProvider {

	final Logger LOG = LoggerFactory.getLogger(IfcProvider.class);

	EntityManagerFactory emf;

	EntityManager em ;
	
	public IfcProvider(){
		emf = Persistence
			.createEntityManagerFactory(PersistenceUtil.getEmfMongo());
		em = emf.createEntityManager();
	}
	
	public void destroy() {
		em.close();
		emf.close();
	}

	public Ifc findIfc(String ifcId){
		Ifc ifc = em.find(Ifc.class, ifcId);
		return ifc;
	}
	
	public Entity findEIfc(EdmEntitySet edmEntitySet, List<UriParameter> keyParams)
			throws ODataApplicationException {

		EntityCollection entitySet = getIfcSet(null);
		EdmEntityType edmEntityType = edmEntitySet.getEntityType();

		Entity requestedEntity = ODataUtil.findEntity(edmEntityType, entitySet,
				keyParams);

		if (requestedEntity == null) {
			
			throw new ODataApplicationException(
					"Entity for requested key doesn't exist",
					HttpStatusCode.NOT_FOUND.getStatusCode(), Locale.ENGLISH);
		}
		return requestedEntity;
	}

	public Entity getEIfc(Ifc ifc, int sn) {

		Entity et = new Entity();

		et.addProperty(new Property(CsdrEdmProvider.ET_IFC_NAME, "ID",
				ValueType.PRIMITIVE, sn));
		
		et.addProperty(new Property(CsdrEdmProvider.ET_IFC_NAME, "Sid",
				ValueType.PRIMITIVE, ifc.getId()));
		
		et.addProperty(new Property(CsdrEdmProvider.ET_IFC_NAME, "Name",
				ValueType.PRIMITIVE, ifc.getName()));
		
		if(ifc.getPriority()!=null)
		et.addProperty(new Property(CsdrEdmProvider.ET_IFC_NAME,
				"Priority", ValueType.PRIMITIVE, ifc
						.getPriority()));
		
		if(ifc.getProfilePartIndicator()!=null)
		et.addProperty(new Property(CsdrEdmProvider.ET_IFC_NAME, "ProfilePartIndicator",
				ValueType.ENUM, ifc.getProfilePartIndicator().ordinal()));

		et.setType(CsdrEdmProvider.ET_IFC.getFullQualifiedNameAsString());
		return et;
	}

	public EntityCollection getIfcSet(List<Ifc> lifc) {

		if (lifc == null)
			lifc = getPIfcSet();
	
		EntityCollection entitySet = new EntityCollection();

		for (int i = 0; i < lifc.size(); i++) {
			Entity et = getEIfc(lifc.get(i), i + 1);
			entitySet.getEntities().add(et);
		}
		return entitySet;
	}

	public List<Ifc> getPIfcSet() {
		
		TypedQuery<Ifc> sQuery = em.createQuery(
				"SELECT i FROM Ifc i", Ifc.class);
		List<Ifc> list = sQuery.getResultList();

		return list;
	}
}