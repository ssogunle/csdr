package com.inted.csdr.providers.domain;

import java.util.List;
import java.util.Locale;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import org.apache.olingo.commons.api.data.Entity;
import org.apache.olingo.commons.api.data.EntityCollection;
import org.apache.olingo.commons.api.data.Property;
import org.apache.olingo.commons.api.data.ValueType;
import org.apache.olingo.commons.api.edm.EdmEntitySet;
import org.apache.olingo.commons.api.edm.EdmEntityType;
import org.apache.olingo.commons.api.http.HttpStatusCode;
import org.apache.olingo.server.api.ODataApplicationException;
import org.apache.olingo.server.api.uri.UriParameter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.inted.csdr.domain.VisitedNetwork;
import com.inted.csdr.providers.CsdrEdmProvider;
import com.inted.csdr.util.ODataUtil;
import com.inted.csdr.util.PersistenceUtil;

public class VisitedNetworkProvider {

	final Logger LOG = LoggerFactory.getLogger(VisitedNetworkProvider.class);

	EntityManagerFactory emf;
	EntityManager em;
	
	public VisitedNetworkProvider(){
		emf = Persistence
			.createEntityManagerFactory(PersistenceUtil.getEmfMysql());
		em = emf.createEntityManager();
	}
	
	public void destroy() {
		em.close();
		emf.close();
	}

	public void add(Object obj){
		try{
			em.persist(obj);
		}catch(Exception ex){
			LOG.error("Cound to add Visited Network: "+ex);
		}
	}
	public Entity addVisitedNetwork(Entity et) {
		VisitedNetwork vn = createVisitedNetwork(et);
	
		try {
			em.persist(vn);
		} catch (Exception ex) {
			LOG.error("Could not create Visited Network: " + ex);
		}
	

		return et;
	}
	
	public Entity getVisitedNetwork(EdmEntitySet edmEntitySet,
			List<UriParameter> keyParams) throws ODataApplicationException {

		EntityCollection entitySet = getVisitedNetworks();
		EdmEntityType edmEntityType = edmEntitySet.getEntityType();

		/* generic approach to find the requested entity */
		Entity requestedEntity = ODataUtil.findEntity(edmEntityType, entitySet,
				keyParams);

		if (requestedEntity == null) {
			// this variable is null if our data doesn't contain an entity for
			// the requested key
			// Throw suitable exception
			throw new ODataApplicationException(
					"Entity for requested key doesn't exist",
					HttpStatusCode.NOT_FOUND.getStatusCode(), Locale.ENGLISH);
		}

		return requestedEntity;
	}
	
	public Entity getEVisitedNetwork(VisitedNetwork vn, int sn){
		
		Entity et = new Entity();
		
		et.addProperty(new Property(CsdrEdmProvider.ET_VISITED_NETWORK_NAME, "ID",
				ValueType.PRIMITIVE, sn));
	
		//et.addProperty(new Property(CsdrEdmProvider.ET_VISITED_NETWORK_NAME, "Sid",
		//		ValueType.PRIMITIVE, vn.getId()));
	
		et.addProperty(new Property(
				CsdrEdmProvider.ET_VISITED_NETWORK_NAME, "Name",
				ValueType.PRIMITIVE, vn.getName()));
		
		et.setType(CsdrEdmProvider.ET_VISITED_NETWORK.getFullQualifiedNameAsString());
		return et;
		
	}
	public EntityCollection getVisitedNetworks() {
		
		
		List<VisitedNetwork> lvn= getPVisitedNetworks();
		
		EntityCollection collection = new EntityCollection();

		for (int i = 0; i < lvn.size(); i++) {
			Entity et = getEVisitedNetwork(lvn.get(i), i + 1);
			collection.getEntities().add(et);
		}
		return collection;
		
	}
	
	
	public List<VisitedNetwork> getPVisitedNetworks() {

		TypedQuery<VisitedNetwork> query = em.createQuery(
				"SELECT x FROM VisitedNetwork x", VisitedNetwork.class);

		List<VisitedNetwork> list = query.getResultList();
		
		return list;
	}

	private VisitedNetwork createVisitedNetwork(Entity et) {
		VisitedNetwork vn = new VisitedNetwork();

		String name = et.getProperties().contains("name") ? et
				.getProperty("name").getValue().toString() : "";

		vn.setName(name);

		return vn;
	}
	
	
}
