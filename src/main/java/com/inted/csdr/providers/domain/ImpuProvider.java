package com.inted.csdr.providers.domain;

import java.util.List;
import java.util.Locale;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import org.apache.olingo.commons.api.data.Entity;
import org.apache.olingo.commons.api.data.EntityCollection;
import org.apache.olingo.commons.api.data.Property;
import org.apache.olingo.commons.api.data.ValueType;
import org.apache.olingo.commons.api.edm.EdmEntitySet;
import org.apache.olingo.commons.api.edm.EdmEntityType;
import org.apache.olingo.commons.api.http.HttpStatusCode;
import org.apache.olingo.server.api.ODataApplicationException;
import org.apache.olingo.server.api.uri.UriParameter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.inted.csdr.domain.Impu;
import com.inted.csdr.providers.CsdrEdmProvider;
import com.inted.csdr.util.ODataUtil;
import com.inted.csdr.util.PersistenceUtil;

public class ImpuProvider {

	final Logger LOG = LoggerFactory.getLogger(ImpuProvider.class);

	EntityManagerFactory emf;
	EntityManager em;

	public ImpuProvider() {
		emf = Persistence.createEntityManagerFactory(PersistenceUtil.getEmfMongo());
		em = emf.createEntityManager();
	}

	public void destroy() {
		em.close();
		emf.close();
	}

	public Impu findPImpu(String spId) {
		Impu impu = em.find(Impu.class, spId);
		return impu;
	}

	public List<Impu> getPImpus() {

		TypedQuery<Impu> sQuery = em.createQuery("SELECT impu FROM Impu impu", Impu.class);
		List<Impu> list = sQuery.getResultList();

		return list;

	}

	public Entity getEImpu(Impu impu, int sn) {

		Entity et = new Entity();
		
		et.addProperty(new Property(CsdrEdmProvider.ET_IMS_PUBLIC_IDENTITY_NAME, "ID",
				ValueType.PRIMITIVE, sn));
	
		et.addProperty(new Property(CsdrEdmProvider.ET_IMS_PUBLIC_IDENTITY_NAME, "Sid",
				ValueType.PRIMITIVE, impu.getId()));
		
		et.addProperty(
				new Property(CsdrEdmProvider.ET_IMS_PUBLIC_IDENTITY_NAME,
						"SipUri", ValueType.PRIMITIVE,impu.getSipUri()));
		
		if(impu.getIdentityType()!=null)
			et.addProperty(
				new Property(CsdrEdmProvider.ET_IMS_PUBLIC_IDENTITY_NAME,
						"IdentityType", ValueType.ENUM,impu.getIdentityType().ordinal()));

		if(impu.getImsUserState()!=null)
			et.addProperty(new Property(CsdrEdmProvider.ET_IMS_PUBLIC_IDENTITY_NAME, "ImsUserState",
				ValueType.ENUM, impu.getImsUserState().ordinal()));
		
		et.addProperty(
				new Property(CsdrEdmProvider.ET_IMS_PUBLIC_IDENTITY_NAME,
						"BarringIndication", ValueType.PRIMITIVE,impu.getBarringIndication()));
		
		et.addProperty(
				new Property(CsdrEdmProvider.ET_IMS_PUBLIC_IDENTITY_NAME,
						"CanRegister", ValueType.PRIMITIVE,impu.getCanRegister()));
		
		et.addProperty(
				new Property(CsdrEdmProvider.ET_IMS_PUBLIC_IDENTITY_NAME,
						"DisplayName", ValueType.PRIMITIVE,impu.getDisplayName()));
		
		
		if(impu.getPsiActivation()!=null)
			et.addProperty(
				new Property(CsdrEdmProvider.ET_IMS_PUBLIC_IDENTITY_NAME,
						"PsiActivation", ValueType.ENUM,impu.getPsiActivation().ordinal()));

		et.setType(CsdrEdmProvider.ET_IMS_PUBLIC_IDENTITY.getFullQualifiedNameAsString());
		return et;
	}
	
	public EntityCollection getEImpus(){
		
		List<Impu> l= getPImpus();
		EntityCollection collection = new EntityCollection();

		for (int i = 0; i < l.size(); i++) {
			Entity et = getEImpu(l.get(i), i + 1);
			collection.getEntities().add(et);
		}
		return collection;
		
	}
	
	public Entity findEImpu(EdmEntitySet edmEntitySet,
			List<UriParameter> keyParams) throws ODataApplicationException {
		
		EntityCollection entitySet = getEImpus();
		EdmEntityType edmEntityType = edmEntitySet.getEntityType();
		
		Entity requestedEntity = ODataUtil.findEntity(edmEntityType, entitySet,
				keyParams);

		if (requestedEntity == null) {
			throw new ODataApplicationException(
					"Entity for requested key doesn't exist",
					HttpStatusCode.NOT_FOUND.getStatusCode(), Locale.ENGLISH);
		}
		return requestedEntity;
	}
}
