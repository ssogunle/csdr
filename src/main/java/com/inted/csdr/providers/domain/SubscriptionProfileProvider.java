package com.inted.csdr.providers.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import org.apache.olingo.commons.api.data.ComplexValue;
import org.apache.olingo.commons.api.data.Entity;
import org.apache.olingo.commons.api.data.EntityCollection;
import org.apache.olingo.commons.api.data.Property;
import org.apache.olingo.commons.api.data.ValueType;
import org.apache.olingo.commons.api.edm.EdmEntitySet;
import org.apache.olingo.commons.api.edm.EdmEntityType;
import org.apache.olingo.commons.api.http.HttpStatusCode;
import org.apache.olingo.server.api.ODataApplicationException;
import org.apache.olingo.server.api.uri.UriParameter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.inted.csdr.domain.Impi;
import com.inted.csdr.domain.Impu;
import com.inted.csdr.domain.RegisteredIdentity;
import com.inted.csdr.domain.Scscf;
import com.inted.csdr.domain.ServiceProfile;
import com.inted.csdr.domain.SubscriptionProfile;
import com.inted.csdr.domain.embeddables.AuthenticationData;
import com.inted.csdr.domain.embeddables.AuthenticationScheme;
import com.inted.csdr.domain.embeddables.ChargingInfo;
import com.inted.csdr.domain.models.enumerated.IdentityType;
import com.inted.csdr.domain.models.enumerated.ImsUserState;
import com.inted.csdr.domain.models.enumerated.PsiActivation;
import com.inted.csdr.providers.CsdrEdmProvider;
import com.inted.csdr.util.ODataUtil;
import com.inted.csdr.util.PersistenceUtil;

public class SubscriptionProfileProvider {

	final Logger LOG = LoggerFactory.getLogger(SubscriptionProfileProvider.class);

	EntityManagerFactory emf;
	EntityManager em;

	public SubscriptionProfileProvider() {
		emf = Persistence.createEntityManagerFactory(PersistenceUtil.getEmfMongo());
		em = emf.createEntityManager();
	}

	public void destroy() {
		em.close();
		emf.close();
	}

	public void add(SubscriptionProfile sp) {

		try {
			em.persist(sp);
		} catch (Exception e) {
			LOG.error("Could not add Subscription Profile");
			e.printStackTrace();
		}
	}

	public SubscriptionProfile findPSubscriptionProfile(String spId) {
		SubscriptionProfile sp = em.find(SubscriptionProfile.class, spId);
		return sp;
	}

	public List<SubscriptionProfile> getPSubscriptionProfiles() {

		TypedQuery<SubscriptionProfile> sQuery = em.createQuery("SELECT sp FROM SubscriptionProfile sp",
				SubscriptionProfile.class);

		List<SubscriptionProfile> list = sQuery.getResultList();

		return list;
	}

	public Entity getESubscriptionProfile(SubscriptionProfile sp, int sn) {

		Entity et = new Entity();

		et.addProperty(new Property(CsdrEdmProvider.ET_SUBSCRIPTION_PROFILE_NAME, "ID", ValueType.PRIMITIVE, sn));

		et.addProperty(
				new Property(CsdrEdmProvider.ET_SUBSCRIPTION_PROFILE_NAME, "Sid", ValueType.PRIMITIVE, sp.getId()));

		et.addProperty(
				new Property(CsdrEdmProvider.ET_SUBSCRIPTION_PROFILE_NAME, "Name", ValueType.PRIMITIVE, sp.getName()));

		et.addProperty(new Property(CsdrEdmProvider.ET_SUBSCRIPTION_PROFILE_NAME, "Impi", ValueType.PRIMITIVE,
				sp.getImpi().getUri()));

		et.addProperty(new Property(CsdrEdmProvider.ET_SUBSCRIPTION_PROFILE_NAME, "ScscfName", ValueType.PRIMITIVE,
				sp.getScscfName()));

		ComplexValue ci = getChargingInfo(sp.getChargingInfo());
		et.addProperty(new Property(null, "ChargingInfo", ValueType.COMPLEX, ci));

		if (sp.getDsaiValue() != null)
			et.addProperty(new Property(CsdrEdmProvider.ET_SUBSCRIPTION_PROFILE_NAME, "Dsai", ValueType.ENUM,
					sp.getDsaiValue().ordinal()));

		et.setType(CsdrEdmProvider.ET_SUBSCRIPTION_PROFILE.getFullQualifiedNameAsString());

		// TBC
		return et;
	}

	public EntityCollection getESubscriptionProfiles() {

		List<SubscriptionProfile> lsp = getPSubscriptionProfiles();
		EntityCollection collection = new EntityCollection();

		for (int i = 0; i < lsp.size(); i++) {
			Entity et = getESubscriptionProfile(lsp.get(i), i + 1);
			collection.getEntities().add(et);
		}
		return collection;

	}

	public Entity findESubscriptionProfile(EdmEntitySet edmEntitySet, List<UriParameter> keyParams)
			throws ODataApplicationException {

		EntityCollection entitySet = getESubscriptionProfiles();
		EdmEntityType edmEntityType = edmEntitySet.getEntityType();

		/* generic approach to find the requested entity */
		Entity requestedEntity = ODataUtil.findEntity(edmEntityType, entitySet, keyParams);

		if (requestedEntity == null) {

			throw new ODataApplicationException("Entity for requested key doesn't exist",
					HttpStatusCode.NOT_FOUND.getStatusCode(), Locale.ENGLISH);
		}
		return requestedEntity;
	}

	// COMPLEX TYPES
	// Charging Info
	public ComplexValue getChargingInfo(ChargingInfo ci) {

		ComplexValue cv = new ComplexValue();
		cv.getValue().add(new Property(CsdrEdmProvider.CT_CHARGING_INFORMATION_NAME, "PrimaryCcf", ValueType.PRIMITIVE,
				ci.getPrimaryCcf()));
		cv.getValue().add(new Property(CsdrEdmProvider.CT_CHARGING_INFORMATION_NAME, "SecondaryCcf",
				ValueType.PRIMITIVE, ci.getSecondaryCcf()));
		cv.getValue().add(new Property(CsdrEdmProvider.CT_CHARGING_INFORMATION_NAME, "PrimaryEcf", ValueType.PRIMITIVE,
				ci.getPrimaryEcf()));
		cv.getValue().add(new Property(CsdrEdmProvider.CT_CHARGING_INFORMATION_NAME, "SecondaryEcf",
				ValueType.PRIMITIVE, ci.getSecondaryEcf()));

		return cv;
	}

}
