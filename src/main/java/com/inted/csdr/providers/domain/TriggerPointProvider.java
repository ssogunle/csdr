package com.inted.csdr.providers.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import javax.persistence.Persistence;

import javax.persistence.TypedQuery;

import org.apache.olingo.commons.api.data.ComplexValue;
import org.apache.olingo.commons.api.data.Entity;
import org.apache.olingo.commons.api.data.EntityCollection;
import org.apache.olingo.commons.api.data.Property;
import org.apache.olingo.commons.api.data.ValueType;
import org.apache.olingo.commons.api.edm.EdmEntitySet;
import org.apache.olingo.commons.api.edm.EdmEntityType;
import org.apache.olingo.commons.api.http.HttpStatusCode;
import org.apache.olingo.server.api.ODataApplicationException;
import org.apache.olingo.server.api.uri.UriParameter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.inted.csdr.domain.TriggerPoint;
import com.inted.csdr.domain.embeddables.Spt;
import com.inted.csdr.providers.CsdrEdmProvider;
import com.inted.csdr.util.ODataUtil;
import com.inted.csdr.util.PersistenceUtil;

public class TriggerPointProvider {

	final Logger LOG = LoggerFactory.getLogger(TriggerPointProvider.class);
	EntityManagerFactory emf;

	EntityManager em;

	public TriggerPointProvider() {
		emf = Persistence.createEntityManagerFactory(PersistenceUtil.getEmfMongo());
		em = emf.createEntityManager();
	}

	public void destroy() {
		em.close();
		emf.close();
	}

	public void add(Object obj) {

		try {
			em.persist(obj);
		} catch (Exception ex) {
			LOG.error("Could not add Trigger Point");
		}

	}

	public TriggerPoint findPTriggerPoint(String id) {
		TriggerPoint tp = em.find(TriggerPoint.class, id);
		return tp;
	}

	public Entity findETriggerPoint(EdmEntitySet edmEntitySet, List<UriParameter> keyParams)
			throws ODataApplicationException {

		EntityCollection entitySet = getETriggerPoints();
		EdmEntityType edmEntityType = edmEntitySet.getEntityType();

		/* generic approach to find the requested entity */
		Entity requestedEntity = ODataUtil.findEntity(edmEntityType, entitySet, keyParams);

		if (requestedEntity == null) {
			throw new ODataApplicationException("Entity for requested key doesn't exist",
					HttpStatusCode.NOT_FOUND.getStatusCode(), Locale.ENGLISH);
		}

		return requestedEntity;
	}

	public Entity getETriggerPoint(TriggerPoint tp, int sn) {

		Entity et = new Entity();

		et.addProperty(new Property(CsdrEdmProvider.ET_TRIGGER_POINT_NAME, "ID", ValueType.PRIMITIVE, sn));

		et.addProperty(new Property(CsdrEdmProvider.ET_TRIGGER_POINT_NAME, "Sid", ValueType.PRIMITIVE, tp.getId()));

		et.addProperty(new Property(CsdrEdmProvider.ET_TRIGGER_POINT_NAME, "Name", ValueType.PRIMITIVE, tp.getName()));

		et.addProperty(new Property(CsdrEdmProvider.ET_TRIGGER_POINT_NAME, "ConditionTypeCnf", ValueType.PRIMITIVE,
				tp.getConditionTypeCnf()));

		List<ComplexValue> spts = getCSpts(tp.getSptList());
		et.addProperty(new Property(null, "ServicePointTriggers",
				ValueType.COLLECTION_COMPLEX, spts));

		et.setType(CsdrEdmProvider.ET_TRIGGER_POINT.getFullQualifiedNameAsString());

		return et;
	}

	public EntityCollection getETriggerPoints() {
		List<TriggerPoint> ltp = getPTriggerPoints();
		EntityCollection entitySet = new EntityCollection();

		for (int i = 0; i < ltp.size(); i++) {
			Entity et = getETriggerPoint(ltp.get(i), i + 1);
			entitySet.getEntities().add(et);
		}
		return entitySet;
	}

	public List<TriggerPoint> getPTriggerPoints() {
		TypedQuery<TriggerPoint> query = em.createQuery("SELECT t FROM TriggerPoint t", TriggerPoint.class);
		List<TriggerPoint> list = query.getResultList();
		return list;
	}

	public List<ComplexValue> getCSpts(List<Spt> sptList) {
		List<ComplexValue> list = new ArrayList<ComplexValue>();

			for (Spt spt : sptList) {
		
				ComplexValue cv = new ComplexValue();

				cv.getValue().add(new Property(CsdrEdmProvider.CT_SPT_NAME, "GroupId",
						ValueType.PRIMITIVE,spt.getGrpId()));
				
				cv.getValue().add(new Property(CsdrEdmProvider.CT_SPT_NAME, "ConditionNegated",
						ValueType.PRIMITIVE,spt.getConditionNegated()));
		
				cv.getValue().add(new Property(CsdrEdmProvider.CT_SPT_NAME, "Type",
						ValueType.ENUM,spt.getType().getValue()));
				
				cv.getValue().add(new Property(CsdrEdmProvider.CT_SPT_NAME, "Data",
						ValueType.PRIMITIVE,spt.getData()));

				list.add(cv);
			}
			
		
		return list;
	}

}
