package com.inted.csdr.providers.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.olingo.commons.api.data.ComplexValue;
import org.apache.olingo.commons.api.data.Entity;
import org.apache.olingo.commons.api.data.EntityCollection;
import org.apache.olingo.commons.api.data.Property;
import org.apache.olingo.commons.api.data.ValueType;
import org.apache.olingo.commons.api.edm.EdmEntitySet;
import org.apache.olingo.commons.api.edm.EdmEntityType;
import org.apache.olingo.commons.api.http.HttpStatusCode;
import org.apache.olingo.server.api.ODataApplicationException;
import org.apache.olingo.server.api.uri.UriParameter;

import com.inted.csdr.domain.Ifc;
import com.inted.csdr.domain.Impu;
import com.inted.csdr.domain.SubscriptionProfile;
import com.inted.csdr.domain.embeddables.AuthenticationData;
import com.inted.csdr.providers.CsdrEdmProvider;
import com.inted.csdr.util.ODataUtil;

public class MergedDataProvider {

	public Entity getEMergedData(List<Ifc> ifcs, List<Impu> impus, int sn) {

		Entity et = new Entity();

		et.addProperty(new Property(CsdrEdmProvider.ET_MERGED_DATA_NAME, "ID", ValueType.PRIMITIVE, sn));

		List<ComplexValue> ifcList = getTrimmedIfcs(ifcs);
		et.addProperty(new Property(null, "Ifcs", ValueType.COLLECTION_COMPLEX, ifcList));

		List<ComplexValue> impuList = getTrimmedImpus(impus);
		et.addProperty(new Property(null, "Impus", ValueType.COLLECTION_COMPLEX, impuList));

		ifcs.get(0).getTriggerPoint().getSptList();

		et.setType(CsdrEdmProvider.ET_MERGED_DATA.getFullQualifiedNameAsString());
		return et;

	}

	public EntityCollection getEMergedDataSet() {

		List<SubscriptionProfile> sps = new SubscriptionProfileProvider().getPSubscriptionProfiles();
		EntityCollection collection = new EntityCollection();

		for (int i = 0; i < sps.size(); i++) {
			Entity et = getEMergedData(sps.get(i).getServiceProfile().getIfcList(), sps.get(i).getImpuList(), i + 1);
			collection.getEntities().add(et);
		}
		return collection;

	}

	public Entity findEMergedData(EdmEntitySet edmEntitySet, List<UriParameter> keyParams)
			throws ODataApplicationException {

		EntityCollection entitySet = getEMergedDataSet();
		EdmEntityType edmEntityType = edmEntitySet.getEntityType();

		/* generic approach to find the requested entity */
		Entity requestedEntity = ODataUtil.findEntity(edmEntityType, entitySet, keyParams);

		if (requestedEntity == null) {
			throw new ODataApplicationException("Entity for requested key doesn't exist",
					HttpStatusCode.NOT_FOUND.getStatusCode(), Locale.ENGLISH);
		}
		return requestedEntity;
	}

	public List<ComplexValue> getTrimmedIfcs(List<Ifc> ifcs) {

		List<ComplexValue> list = new ArrayList<ComplexValue>();

		for (Ifc ifc : ifcs) {
			ComplexValue cv = new ComplexValue();

			cv.getValue().add(new Property(CsdrEdmProvider.CT_TRIMMED_IFC_NAME, "Priority", ValueType.PRIMITIVE,
					ifc.getPriority()));

			cv.getValue().add(new Property(CsdrEdmProvider.CT_TRIMMED_IFC_NAME, "TriggerPointCnf", ValueType.PRIMITIVE,
					ifc.getTriggerPoint().getConditionTypeCnf()));
			
			List<ComplexValue> spts = new TriggerPointProvider().getCSpts(ifc.getTriggerPoint().getSptList());
			cv.getValue().add(new Property(null, "ServicePointTriggers",
					ValueType.COLLECTION_COMPLEX, spts));
			
			cv.getValue().add(new Property(CsdrEdmProvider.CT_TRIMMED_IFC_NAME, "ApplicationServerUri",
					ValueType.PRIMITIVE, ifc.getAppServer().getServerName()));

			list.add(cv);
		}

		return list;

	}

	public List<ComplexValue> getTrimmedImpus(List<Impu> impus) {

		List<ComplexValue> list = new ArrayList<ComplexValue>();

		for (Impu impu : impus) {
			ComplexValue cv = new ComplexValue();

			cv.getValue().add(new Property(CsdrEdmProvider.CT_TRIMMED_IMPU_NAME, "SipUri", ValueType.PRIMITIVE,
					impu.getSipUri()));
			cv.getValue().add(new Property(CsdrEdmProvider.CT_TRIMMED_IMPU_NAME, "Barred", ValueType.PRIMITIVE,
					impu.getBarringIndication()));

			list.add(cv);
		}

		return list;

	}
}
