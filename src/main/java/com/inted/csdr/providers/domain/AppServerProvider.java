package com.inted.csdr.providers.domain;

import java.util.List;
import java.util.Locale;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import org.apache.olingo.commons.api.data.ComplexValue;
import org.apache.olingo.commons.api.data.Entity;
import org.apache.olingo.commons.api.data.EntityCollection;
import org.apache.olingo.commons.api.data.Property;
import org.apache.olingo.commons.api.data.ValueType;
import org.apache.olingo.commons.api.edm.EdmEntitySet;
import org.apache.olingo.commons.api.edm.EdmEntityType;
import org.apache.olingo.commons.api.http.HttpStatusCode;
import org.apache.olingo.server.api.ODataApplicationException;
import org.apache.olingo.server.api.uri.UriParameter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.inted.csdr.domain.AppServer;
import com.inted.csdr.domain.embeddables.ShPurPermission;
import com.inted.csdr.domain.embeddables.ShUdrPermission;
import com.inted.csdr.providers.CsdrEdmProvider;
import com.inted.csdr.util.ODataUtil;
import com.inted.csdr.util.PersistenceUtil;

public class AppServerProvider {

	final Logger LOG = LoggerFactory.getLogger(AppServerProvider.class);

	EntityManagerFactory emf;
	EntityManager em;

	public AppServerProvider() {
		emf = Persistence.createEntityManagerFactory(PersistenceUtil.getEmfMongo());
		em = emf.createEntityManager();
	}

	public void destroy() {
		em.close();
		emf.close();
	}

	// JPA Models
	public void add(AppServer as) {

		try {
			em.persist(as);
		} catch (Exception ex) {
			LOG.error("Could not add Application Server: " + ex);
		}
	}

	public AppServer findPAppServer(String id) {
		AppServer as = em.find(AppServer.class, id);
		return as;
	}

	
	public List<AppServer> getPAppServers() {
		TypedQuery<AppServer> query = em.createQuery("SELECT as FROM AppServer as", AppServer.class);
		List<AppServer> list = query.getResultList();
		return list;
	}

	// OData Models
	
	/*public Entity add(Entity et) {

		try {
			em.persist(as);
		} catch (Exception ex) {
			LOG.error("Could not add Application Server: " + ex);
		}
	}*/
	public Entity getEAppServer(AppServer as, int sn) {
		
		Entity et = new Entity();
		
		et.addProperty(new Property(CsdrEdmProvider.ET_APP_SERVER_NAME, "ID", ValueType.PRIMITIVE, sn));

		et.addProperty(new Property(CsdrEdmProvider.ET_APP_SERVER_NAME, "Sid", ValueType.PRIMITIVE, as.getId()));

		et.addProperty(new Property(CsdrEdmProvider.ET_APP_SERVER_NAME, "Name", ValueType.PRIMITIVE, as.getName()));

		et.addProperty(
				new Property(CsdrEdmProvider.ET_APP_SERVER_NAME, "SipUri", ValueType.PRIMITIVE, as.getServerName()));

		et.addProperty(new Property(CsdrEdmProvider.ET_APP_SERVER_NAME, "DiameterUri", ValueType.PRIMITIVE,
				as.getDiameterUri()));

		et.addProperty(new Property(CsdrEdmProvider.ET_APP_SERVER_NAME, "DefaultHandling", ValueType.PRIMITIVE,
				as.getDefaultHandling()));

		et.addProperty(new Property(CsdrEdmProvider.ET_APP_SERVER_NAME, "ServiceInfo", ValueType.PRIMITIVE,
				as.getServiceInfo()));

		ComplexValue shUdrPermissionsCv = getShUdrPermissions(as.getShUdrPermission());
		et.addProperty(new Property(null, "ShUdrPermissions", ValueType.COMPLEX, shUdrPermissionsCv));

		ComplexValue shPurPermissionsCv = getShPurPermissions(as.getShPurPermission());
		et.addProperty(new Property(null, "ShPurPermissions", ValueType.COMPLEX, shPurPermissionsCv));

		et.addProperty(new Property(CsdrEdmProvider.ET_APP_SERVER_NAME, "IncludeRegisterRequest", ValueType.PRIMITIVE,
				as.getIncludeRegisterRequest()));

		et.addProperty(new Property(CsdrEdmProvider.ET_APP_SERVER_NAME, "IncludeRegisterResponse", ValueType.PRIMITIVE,
				as.getIncludeRegisterResponse()));

		et.setType(CsdrEdmProvider.ET_APP_SERVER.getFullQualifiedNameAsString());
		
		return et;
	}

	public EntityCollection getEAppServers() {

		List<AppServer> las = getPAppServers();
		EntityCollection collection = new EntityCollection();

		for (int i = 0; i < las.size(); i++) {
			Entity et = getEAppServer(las.get(i), i + 1);
			collection.getEntities().add(et);
		}
		return collection;
	}

	public Entity findEAppServer(EdmEntitySet edmEntitySet, List<UriParameter> keyParams)
			throws ODataApplicationException {

		EntityCollection entitySet = getEAppServers();
		EdmEntityType edmEntityType = edmEntitySet.getEntityType();

		Entity requestedEntity = ODataUtil.findEntity(edmEntityType, entitySet, keyParams);

		if (requestedEntity == null) {

			throw new ODataApplicationException("Entity for requested key doesn't exist",
					HttpStatusCode.NOT_FOUND.getStatusCode(), Locale.ENGLISH);
		}

		return requestedEntity;
	}

	public ComplexValue getShUdrPermissions(ShUdrPermission val) {

		ComplexValue cv = new ComplexValue();
		cv.getValue().add(new Property(CsdrEdmProvider.CT_SH_UDR_PERMISSIONS_NAME, "AliasesRepoData",
				ValueType.PRIMITIVE, val.getAliasesRepositoryData()));
		cv.getValue().add(new Property(CsdrEdmProvider.CT_SH_UDR_PERMISSIONS_NAME, "ChargingInfo", ValueType.PRIMITIVE,
				val.getChargingInfo()));
		cv.getValue().add(
				new Property(CsdrEdmProvider.CT_SH_UDR_PERMISSIONS_NAME, "Dsai", ValueType.PRIMITIVE, val.getDsai()));
		cv.getValue().add(
				new Property(CsdrEdmProvider.CT_SH_UDR_PERMISSIONS_NAME, "Ifc", ValueType.PRIMITIVE, val.getIfc()));
		cv.getValue().add(new Property(CsdrEdmProvider.CT_SH_UDR_PERMISSIONS_NAME, "ImpuIdentifier",
				ValueType.PRIMITIVE, val.getImpuIdentifier()));
		cv.getValue().add(new Property(CsdrEdmProvider.CT_SH_UDR_PERMISSIONS_NAME, "ImsUserState", ValueType.PRIMITIVE,
				val.getImsUserState()));
		cv.getValue().add(new Property(CsdrEdmProvider.CT_SH_UDR_PERMISSIONS_NAME, "IsAllowed", ValueType.PRIMITIVE,
				val.getIsAllowed()));
		cv.getValue().add(new Property(CsdrEdmProvider.CT_SH_UDR_PERMISSIONS_NAME, "PsiActivation", ValueType.PRIMITIVE,
				val.getPsiActivation()));
		return cv;
	}

	public ComplexValue getShPurPermissions(ShPurPermission val) {

		ComplexValue cv = new ComplexValue();

		cv.getValue().add(
				new Property(CsdrEdmProvider.CT_SH_PUR_PERMISSIONS_NAME, "Dsai", ValueType.PRIMITIVE, val.getDsai()));
		cv.getValue().add(new Property(CsdrEdmProvider.CT_SH_PUR_PERMISSIONS_NAME, "IsAllowed", ValueType.PRIMITIVE,
				val.getIsAllowed()));
		cv.getValue().add(new Property(CsdrEdmProvider.CT_SH_PUR_PERMISSIONS_NAME, "PsiActivation", ValueType.PRIMITIVE,
				val.getPsiActivation()));
		return cv;
	}

}
