package com.inted.csdr.providers.domain;

import java.util.List;
import java.util.Locale;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import org.apache.olingo.commons.api.data.Entity;
import org.apache.olingo.commons.api.data.EntityCollection;
import org.apache.olingo.commons.api.data.Property;
import org.apache.olingo.commons.api.data.ValueType;
import org.apache.olingo.commons.api.edm.EdmEntitySet;
import org.apache.olingo.commons.api.edm.EdmEntityType;
import org.apache.olingo.commons.api.http.HttpStatusCode;
import org.apache.olingo.server.api.ODataApplicationException;
import org.apache.olingo.server.api.uri.UriParameter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.inted.csdr.domain.ServiceProfile;
import com.inted.csdr.providers.CsdrEdmProvider;
import com.inted.csdr.util.ODataUtil;
import com.inted.csdr.util.PersistenceUtil;

public class ServiceProfileProvider {

	final Logger LOG = LoggerFactory.getLogger(ServiceProfileProvider.class);

	EntityManagerFactory emf;
	EntityManager em;

	public ServiceProfileProvider() {
		emf = Persistence.createEntityManagerFactory(PersistenceUtil.getEmfMongo());
		em = emf.createEntityManager();
	}

	public void destroy() {
		em.close();
		emf.close();
	}

	public void add(ServiceProfile sp) {
		try {
			em.persist(sp);
		} catch (Exception e) {
				LOG.error("Could not add Service Profile");
		}
	}

	public ServiceProfile findPServiceProfile(String spId) {
		ServiceProfile sp = em.find(ServiceProfile.class, spId);
		return sp;
	}

	public Entity getEServiceProfile(ServiceProfile sp, int sn) {

		Entity et = new Entity();

		et.addProperty(new Property(CsdrEdmProvider.ET_SERVICE_PROFILE_NAME, "ID", ValueType.PRIMITIVE, sn));

		et.addProperty(new Property(CsdrEdmProvider.ET_SERVICE_PROFILE_NAME, "Sid", ValueType.PRIMITIVE, sp.getId()));

		et.addProperty(
				new Property(CsdrEdmProvider.ET_SERVICE_PROFILE_NAME, "Name", ValueType.PRIMITIVE, sp.getName()));

		et.setType(CsdrEdmProvider.ET_SERVICE_PROFILE.getFullQualifiedNameAsString());
		return et;
	}

	public List<ServiceProfile> getPServiceProfiles() {

		TypedQuery<ServiceProfile> sQuery = em.createQuery("SELECT sp FROM ServiceProfile sp", ServiceProfile.class);
		List<ServiceProfile> list = sQuery.getResultList();

		return list;
	}

	public EntityCollection getEServiceProfiles() {

		List<ServiceProfile> lsp = getPServiceProfiles();

		EntityCollection collection = new EntityCollection();

		for (int i = 0; i < lsp.size(); i++) {
			Entity et = getEServiceProfile(lsp.get(i), i + 1);
			collection.getEntities().add(et);
		}
		return collection;
	}

	public Entity findEServiceProfile(EdmEntitySet edmEntitySet, List<UriParameter> keyParams)
			throws ODataApplicationException {

		EntityCollection entitySet = getEServiceProfiles();
		EdmEntityType edmEntityType = edmEntitySet.getEntityType();

		/* generic approach to find the requested entity */
		Entity requestedEntity = ODataUtil.findEntity(edmEntityType, entitySet, keyParams);

		if (requestedEntity == null) {

			throw new ODataApplicationException("Entity for requested key doesn't exist",
					HttpStatusCode.NOT_FOUND.getStatusCode(), Locale.ENGLISH);
		}

		return requestedEntity;
	}

}
