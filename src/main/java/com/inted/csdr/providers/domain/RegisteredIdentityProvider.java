package com.inted.csdr.providers.domain;

import java.util.List;
import java.util.Locale;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import org.apache.olingo.commons.api.data.Entity;
import org.apache.olingo.commons.api.data.EntityCollection;
import org.apache.olingo.commons.api.data.Property;
import org.apache.olingo.commons.api.data.ValueType;
import org.apache.olingo.commons.api.edm.EdmEntitySet;
import org.apache.olingo.commons.api.edm.EdmEntityType;
import org.apache.olingo.commons.api.http.HttpStatusCode;
import org.apache.olingo.server.api.ODataApplicationException;
import org.apache.olingo.server.api.uri.UriParameter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.inted.csdr.domain.RegisteredIdentity;
import com.inted.csdr.providers.CsdrEdmProvider;
import com.inted.csdr.util.ODataUtil;
import com.inted.csdr.util.PersistenceUtil;

public class RegisteredIdentityProvider {

	final Logger LOG = LoggerFactory.getLogger(RegisteredIdentityProvider.class);
	
	EntityManagerFactory emf;
	EntityManager em;

	public  RegisteredIdentityProvider() {
		emf = Persistence.createEntityManagerFactory(PersistenceUtil.getEmfMysql());
		em = emf.createEntityManager();
	}

	public void destroy() {
		em.close();
		emf.close();
	}

	public void add(Object obj){
		
		try{
			em.persist(obj);
		}
		catch(Exception ex){
			LOG.error("Could not add Subscriber Identity Info: "+ex);
		}
	}
	public List<RegisteredIdentity> getPRegisteredIdentities() {

		TypedQuery<RegisteredIdentity> query = em.createQuery("SELECT x FROM RegisteredIdentity x", RegisteredIdentity.class);
		List<RegisteredIdentity> list = query.getResultList();
		return list;
	}

	public Entity getERegisteredIdentity(RegisteredIdentity r, int sn) {

		Entity et = new Entity();

		et.addProperty(new Property(CsdrEdmProvider.ET_REGISTERED_IDENTITY_NAME, "ID", ValueType.PRIMITIVE, sn));

		et.addProperty(
				new Property(CsdrEdmProvider.ET_REGISTERED_IDENTITY_NAME, "Impi", ValueType.PRIMITIVE, r.getImpi()));
		
		et.addProperty(
				new Property(CsdrEdmProvider.ET_REGISTERED_IDENTITY_NAME, "Impu", ValueType.PRIMITIVE,r.getImpu()));
		
		
		et.addProperty(
				new Property(CsdrEdmProvider.ET_REGISTERED_IDENTITY_NAME, "AuthKey", ValueType.PRIMITIVE,new String(r.getAuthKey())));
		
		et.addProperty(
				new Property(CsdrEdmProvider.ET_REGISTERED_IDENTITY_NAME, "AuthOp", ValueType.PRIMITIVE, new String(r.getAuthOp())));
		
		et.addProperty(
				new Property(CsdrEdmProvider.ET_REGISTERED_IDENTITY_NAME, "AuthAmf", ValueType.PRIMITIVE, new String(r.getAuthAmf())));
		

		et.addProperty(
				new Property(CsdrEdmProvider.ET_REGISTERED_IDENTITY_NAME, "AuthSqn", ValueType.PRIMITIVE,r.getAuthSqn()));
		et.addProperty(
				new Property(CsdrEdmProvider.ET_REGISTERED_IDENTITY_NAME, "AuthScheme", ValueType.PRIMITIVE,r.getAuthScheme()));
		et.addProperty(
				new Property(CsdrEdmProvider.ET_REGISTERED_IDENTITY_NAME, "AuthLineId", ValueType.PRIMITIVE,r.getAuthLineId()));
		et.addProperty(
				new Property(CsdrEdmProvider.ET_REGISTERED_IDENTITY_NAME, "AuthIpAddress", ValueType.PRIMITIVE,r.getAuthIpAddress()));
	
		et.addProperty(
				new Property(CsdrEdmProvider.ET_REGISTERED_IDENTITY_NAME, "Barred", ValueType.PRIMITIVE,r.getBarring()));
		et.addProperty(
				new Property(CsdrEdmProvider.ET_REGISTERED_IDENTITY_NAME, "CanRegister", ValueType.PRIMITIVE, r.getCanRegister()));
		et.addProperty(
				new Property(CsdrEdmProvider.ET_REGISTERED_IDENTITY_NAME, "VisitedNetwork", ValueType.PRIMITIVE,r.getVisitedNetwork()));
		
		et.addProperty(
				new Property(CsdrEdmProvider.ET_REGISTERED_IDENTITY_NAME, "ScscfId", ValueType.PRIMITIVE,r.getScscfName()));
		
		if(r.getImpuType()!=null){
		et.addProperty(
				new Property(CsdrEdmProvider.ET_REGISTERED_IDENTITY_NAME, "ImpuType", ValueType.PRIMITIVE,r.getImpuType().ordinal()));
		}
		
		if(r.getPsiActivation()!=null){
		et.addProperty(
				new Property(CsdrEdmProvider.ET_REGISTERED_IDENTITY_NAME, "PsiActivated", ValueType.PRIMITIVE,r.getPsiActivation().ordinal()));
		}
		
		if(r.getUserState()!=null){
		et.addProperty(
				new Property(CsdrEdmProvider.ET_REGISTERED_IDENTITY_NAME, "UserState", ValueType.PRIMITIVE,r.getUserState().ordinal()));
		}
		
		et.setType(CsdrEdmProvider.ET_REGISTERED_IDENTITY.getFullQualifiedNameAsString());
		return et;
	}

	public EntityCollection getERegisteredIdentities() {

		List<RegisteredIdentity> lsp = getPRegisteredIdentities();
		EntityCollection collection = new EntityCollection();

		for (int i = 0; i < lsp.size(); i++) {
			Entity et = getERegisteredIdentity(lsp.get(i), i + 1);
			collection.getEntities().add(et);
		}
		return collection;
	}
	
	public Entity findERegisteredIdentity(EdmEntitySet edmEntitySet,
			List<UriParameter> keyParams) throws ODataApplicationException {
		
		EntityCollection entitySet = getERegisteredIdentities();
		EdmEntityType edmEntityType = edmEntitySet.getEntityType();
		
		/* generic approach to find the requested entity */
		Entity requestedEntity = ODataUtil.findEntity(edmEntityType, entitySet,
				keyParams);
		
		if (requestedEntity == null) {
			throw new ODataApplicationException(
					"Entity for requested key doesn't exist",
					HttpStatusCode.NOT_FOUND.getStatusCode(), Locale.ENGLISH);
		}
		return requestedEntity;
	}
}
