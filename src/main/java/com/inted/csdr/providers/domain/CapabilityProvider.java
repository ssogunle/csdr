package com.inted.csdr.providers.domain;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import org.apache.olingo.commons.api.data.Entity;
import org.apache.olingo.commons.api.data.EntityCollection;
import org.apache.olingo.commons.api.data.Property;
import org.apache.olingo.commons.api.data.ValueType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.inted.csdr.domain.Capability;
import com.inted.csdr.providers.CsdrEdmProvider;
import com.inted.csdr.util.PersistenceUtil;

public class CapabilityProvider {

	final Logger LOG = LoggerFactory.getLogger(CapabilityProvider.class);
	
	EntityManagerFactory emf;
	EntityManager em;
	
	public void start(){
		emf = Persistence
			.createEntityManagerFactory(PersistenceUtil.getEmfMongo());
		em = emf.createEntityManager();
	}
	
	public void destroy() {
		em.close();
		emf.close();
	}
	
	public void add(String name){
		
		Capability c = new Capability();
		c.setName(name);
		
		this.start();
		try {
			em.persist(c);
		} catch (Exception ex) {
			LOG.error("Could not add Capability: " + ex);
		}
		this.destroy();
	}
	
	public Capability findCapability(String Id){
		
		this.start();
		Capability c = em.find(Capability.class, Id);
		this.destroy();
		
		return c;
	}
	
	public List<Capability> getPCapabilities() {

		TypedQuery<Capability> query = em.createQuery(
				"SELECT c FROM Capability c", Capability.class);
		List<Capability> list = query.getResultList();
		return list;
	}
	
	public Entity getECapability(Capability c, int sn) {
		Entity et = new Entity();
		et.addProperty(new Property(CsdrEdmProvider.ET_CAPABILITY_NAME, "ID",
				ValueType.PRIMITIVE, sn));

		et.addProperty(new Property(CsdrEdmProvider.ET_CAPABILITY_NAME, "Sid",
				ValueType.PRIMITIVE, c.getId()));

		et.addProperty(new Property(CsdrEdmProvider.ET_CAPABILITY_NAME, "Name",
				ValueType.PRIMITIVE, c.getName()));
		
		et.setType(CsdrEdmProvider.ET_CAPABILITY.getFullQualifiedNameAsString());
		return et;
	}
	
	public EntityCollection getECapabilities() {

		List<Capability> lc = getPCapabilities();
		// Convert JPA Entity Set to OData EntityCollection
		EntityCollection collection = new EntityCollection();

		for (int i = 0; i < lc.size(); i++) {
			Entity et = getECapability(lc.get(i), i + 1);
			collection.getEntities().add(et);
		}
		return collection;
	}
	
	
}
