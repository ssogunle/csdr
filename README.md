# Converged Subscriber Data Repository (CSDR)

## Synopsis
This is a unified data repository for [Rich Communication Services](https://www.gsma.com/futurenetworks/rcs/). It consolidates subscriber data into a single logical repository without adversely affecting existing network infrastructure, such as the [IP Multimedia Subsystem (IMS)](https://www.gsma.com/futurenetworks/rcs/). It adopts a polyglot persistence model for the underlying data store and exposes heterogeneous data through the [Open Data Protocol (OData)](https://www.odata.org/).

With the introduction of polyglot persistence, multiple data stores can be used within the CSDR and disparate network data sources can access heterogeneous data sets using OData as a standard communications protocol. 
As the CSDR persistence model becomes more complex due to the inclusion of more storage technologies, polyglot persistence ensures a consistent conceptual view of these data sets through OData.  CSDR is able to achieve a unified view by mapping requests from front-end (FE) applications that utilize different communications protocols to an OData request. 

Examples of such communication protocols include: [Session Initiation Protocol (SIP)](https://en.wikipedia.org/wiki/Session_Initiation_Protocol), [Diameter Protocol](https://en.wikipedia.org/wiki/Diameter_(protocol)), and [XML Configuration Access Protocol (XCAP)](https://en.wikipedia.org/wiki/XML_Configuration_Access_Protocol), among others. 

Notably, CSDR adopts [Mysql](https://www.mysql.com/), [Redis](https://redis.io/) and [MongoDB](https://www.mongodb.com/) at its backend and was deployed on the production-grade [Wildfly Application Server](https://wildfly.org/). It adopts the  [PrimeFaces](https://www.primefaces.org/) Java Server Faces framework for its FE implementation.

Furthermore, the CSDR was integrated into a popular open-source implementation of the core part of an IMS network known as the  [Open IMS Core](http://openimscore.sourceforge.net/). Four (4) FE applications were developed to interact with the CSDR, namely:

1. Home Subscriber Server (HSS) FE. Located [here](https://bitbucket.org/ssogunle/hss-fe/).
2. XCAP FE. Located [here](https://bitbucket.org/ssogunle/xcap-fe/).
3. SIP Telephony FE. Located [here](https://bitbucket.org/ssogunle/telephony-fe/).
4. SIP Messaging FE. Located [here](https://bitbucket.org/ssogunle/messaging-fe/).

**NOTE**: Each FE is deployed separately and therefore, not part of the CSDR implentation. Additionally, both *SIP Telephony and Messaging FE* utilized [Restcomm SIP Servlets](https://github.com/RestComm/sip-servlets) as deployment application servers.
	

# Project Dependencies
- CSDR's dependencies are described in the [Project Object Model](https://bitbucket.org/ssogunle/csdr/src/master/pom.xml).

# Persistence File
- CSDR's persistence file can be found [here](https://bitbucket.org/ssogunle/csdr/src/master/src/main/resources/META-INF/persistence.xml).

# General Code Structure (CSDR)
- Front-end code can be found in the [webapp](https://bitbucket.org/ssogunle/csdr/src/master/src/main/webapp/) sub-directory.
- Back-end code can be found in the [csdr](https://bitbucket.org/ssogunle/csdr/src/master/src/main/java/com/inted/csdr/) sub-directory. 

# UPDATE: Accessing Application from Web Browser 
CSDR is a Data Service for highly-specialized networking equipments such as the FE applications described above. Those equipments have to query the CSDR for specific (enitity) data sets. To find out what data entities the CSDR offers to its FE applicationns, simply enter the following in your browser:
```bash
localhost:<port-number>/csdr/svc
```

To retrieve Entity records that the CSDR has stored in its repository, please enter the following:
```bash
localhost:<port-number>/csdr/svc/<entity-name>
```
**NOTE**: An example of an `<entity-name>` could be `IfcSet`.


To access the home page for CRUD operations, simply enter the following:
```bash
localhost:<port-number>/csdr
```

# How to Deploy (Option 1)

### Install (Linux)
- [Docker](https://www.docker.com/)

### Download Image
- You will require a [Docker Hub](https://hub.docker.com/) account. Please register and login if you do not already have an account. 
- Thereafter, you can download the docker image as follows:
 
```bash
$  docker pull ssogunle/csdr
```

### Run 
- After downloading the image to your PC, you will need to run the container in 'detached' mode as follows:

```bash
$  docker run -d -p 8080:8080 -p 27017:27017 -p 6379:6379 -p 3306:3306 ssogunle/csdr
```

#### Docker Run (Desctiption)
- [-d]: Run container in detached mode

** Important **:

- [ -p 8080:8080]: Binds container's Wildfly's local port number to port 8080 on your system. 
- This could be modified if you already have an application that utilizes port 8080. 
- Simply change the `first` port number in the argument to your preferred number. 
- You can verify that this works correctly on your web browser at: ***localhost:<port-number>/csdr/***
 
		
** Optional **:

- [ -p 27017:27017]: Binds MongoDB's port number 

- [ -p 6379:6379]: Binds Redis' port number

- [ -p 3306:3306]: Binds MySQL's port number

#### Verify Container Activity
- To verify that the container is running, you can issue the following command:

```bash
$  docker ps
```

- *docker ps* shows the list of actively running docker container alongside their aliases.

#### Accessing Database Contents
- Once the *csdr app* is running within the docker container, you can issue the following command to log into the container:

```bash
$  docker exec -it <container-name> bash
```

- You can open multiple terminals if you wish to examine contents of MySQL, Redis and MongoDB databases.


# How To Deploy (Option 2)

### Download and Install 
- [Java 1.8](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
- [Apache Maven](https://maven.apache.org/) 
- [Wildfly AS 10](https://download.jboss.org/wildfly/10.0.0.Final/wildfly-10.0.0.Final.zip)
- [MongoDB](https://www.mongodb.com/)
- [Redis](https://redis.io/download)
- [MySQL](https://dev.mysql.com/downloads/)
 
### Start all components:
- Ensure that MySQL, Redis and MongoDB servers are actively running on their respective ports.
- Ensure that the WildFly AS is running. 
- WildFly AS install (or home) directory should be at: /opt/jboss/wildfly/ (on Linux)
- In case you have chosen another directory, please navigate to the home directory for the WildFly AS accordingly.
- Thereafter, navigate to the bin/ directory and execute the following (on Linux):

```bash
$  bash standalone.sh
```

- **NOTE**: Windows OS will have a *standalone.bat*

### Build Artifact
- Clone this Git repository. 
- Execute the following comand in the root folder for this repository:

```bash
$  mvn clean install
```


### Deploy Artifact on Wildfly
- Once artifact build is successful, you can copy the *csdr.war* file from the newly generated target/ sub-directory.
- Copy this artifact to the standalone/deployments sub-directory of the WildFly home directory. For example,  COPY csdr/target/csdr.war to /opt/jboss/wildfly/standalone/deployments/

### Launch and Test CSDR Service
- Visit localhost:<wildfly-port-number>/csdr to have a feel of the web service.


Have fun!


